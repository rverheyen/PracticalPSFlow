import torch
import matplotlib as mpl
from matplotlib import pyplot as plt 
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.lines import Line2D

import pandas as pd 
import numpy as np
import yaml

from ppflows.gluino.gluino_models import GluinoModel, MixtureGluinoModel
from ppflows.gluino.gluino_auxiliary import massless_to_massive, GluinoEncoder
from ppflows.permuters import IteratedPermutation

def plot_hist(data, data_baseline, ax, ax_ratio, label, colour, linewidth, x_min, x_max, n_bins, norm):
    bins = np.linspace(x_min, x_max, n_bins)
    bin_width = (x_max - x_min)/n_bins

    # Compute errors
    counts, edges = np.histogram(data, bins=bins)
    errors = np.sqrt(counts)

    # Normalize to cross section
    counts = norm*counts/data.shape[0]/bin_width
    errors = norm*errors/data.shape[0]/bin_width

    centers = (edges[1:] + edges[:-1])/2

    fill_x = np.array([e for edge in bins for e in [edge, edge]][1:-1])
    fill_delta_y = np.array([y for error in errors for y in [error, error]])
    fill_central_y = np.array([c for count in counts for c in [count, count]])

    # Regular plot
    ax.hist(centers, bins=bins, histtype='step', label=label, color=colour, linewidth=linewidth, weights=counts)
    ax.fill_between(fill_x, fill_central_y - fill_delta_y, fill_central_y + fill_delta_y, facecolor=colour, alpha=0.3)
    
    # Ratio plot
    counts_baseline, _ = np.histogram(data_baseline, bins=bins)
    counts_baseline = norm*counts_baseline/data_baseline.shape[0]/bin_width
    ax_ratio.hist(centers, bins=bins, histtype='step', label=label, color=colour, linewidth=linewidth, weights=counts/counts_baseline)

    # Errors in ratio
    ratio_counts = counts/counts_baseline
    ratio_counts[np.isinf(ratio_counts)] = 1
    ratio_errors = errors/counts_baseline
    ratio_errors[np.isinf(ratio_errors)] = 1
    
    fill_central_y = np.array([c for count in ratio_counts for c in [count, count]])
    fill_delta_y = np.array([y for error in ratio_errors for y in [error, error]])
    ax_ratio.fill_between(fill_x, fill_central_y - fill_delta_y, fill_central_y + fill_delta_y, facecolor=colour, alpha=0.3)

def perp(x, y):
    z = np.zeros((x.shape[0], 3))
    z[:,0] = x[:,2]*y[:,3] - x[:,3]*y[:,2]
    z[:,1] = x[:,3]*y[:,1] - x[:,1]*y[:,3]
    z[:,2] = x[:,1]*y[:,2] - x[:,2]*y[:,1]

    # Normalize
    norm = np.expand_dims(np.sqrt(z[:,0]**2 + z[:,1]**2 + z[:,2]**2), axis=1)
    return z/norm

def dpsi(p):
    perp_1 = perp(p[:,0], p[:,1])
    perp_2 = perp(p[:,0] + p[:,1], p[:,2])

    dpsi = np.arccos(perp_1[:,0]*perp_2[:,0] + perp_1[:,1]*perp_2[:,1] + perp_1[:,2]*perp_2[:,2])

    return dpsi


textwidth = 9
textheight = textwidth/2.4
fontsize = 10.95

colors = ["#e41a1c", "#377eb8", "#4daf4a", "#984ea3", "#ff7f00", "#ffff33", "#a65628", "#f781bf"]
labels = ["Uniform dequantization", "Flow dequantization", "Uniform argmax", "Flow argmax", "Mixture"]

mpl.rcParams['text.usetex'] = True
mpl.rcParams['font.family'] = 'serif'

mpl.rcParams['xtick.direction'] = 'in'
mpl.rcParams['xtick.major.size'] = 4.0
mpl.rcParams['xtick.minor.size'] = 2.0
mpl.rcParams['xtick.major.width'] = 0.5
mpl.rcParams['xtick.minor.width'] = 0.5

mpl.rcParams['ytick.direction'] = 'in'
mpl.rcParams['ytick.major.size'] = 4.0
mpl.rcParams['ytick.minor.size'] = 2.0
mpl.rcParams['ytick.major.width'] = 0.5
mpl.rcParams['ytick.minor.width'] = 0.5

mpl.rcParams['lines.linewidth'] = 1.2
mpl.rcParams['lines.markersize'] = 2
mpl.rcParams["legend.labelspacing"] = 0.1
mpl.rcParams["legend.frameon"] = False
mpl.rcParams["legend.handletextpad"] = 0.5
mpl.rcParams["xtick.minor.visible"] = True
mpl.rcParams["ytick.minor.visible"] = True
#mpl.rcParams["legend.columnspacing"] = 0
mpl.rcParams.update({'font.size': fontsize})

n_bins = 25
n_train = 1000000
n_samples = 1000

data = pd.read_csv("../data_4_gluino.csv", header=None).to_numpy()[:n_train,:]

mass = np.sqrt(data[0,0]**2 - data[0,1]**2 - data[0,2]**2 - data[0,3]**2)

data_continuous = data[:,:16]
data_continuous = torch.tensor(data_continuous.reshape(data_continuous.shape[0], 4, 4))
data_discrete = torch.tensor(data[:,16:], dtype=torch.long)

permute_classes_continuous  = np.array([[0],[1],[2],[3]])   
permute_classes_discrete    = np.array([[2,7],[3,8],[4,9],[5,10]])

with open("hyperparams.yaml") as file:
    hyperparams = yaml.safe_load(file)

samples_continuous_model = []
samples_discrete_model = []

hyperparams["discrete_mode"] = "uniform_dequantization"
uniform_dequantization_model = GluinoModel(hyperparams)
uniform_dequantization_model.load_state_dict(torch.load("results/model_uniform_dequantization.pt", map_location=torch.device('cpu')))
uniform_dequantization_model.eval()
with torch.no_grad():
    samples_continuous, samples_discrete = uniform_dequantization_model.sample(n_samples, batch_size=2048)
    samples_continuous = massless_to_massive(samples_continuous, mass)

    samples_continuous_model.append(samples_continuous)
    samples_discrete_model.append(samples_discrete)

hyperparams["discrete_mode"] = "flow_dequantization"
flow_dequantization_model = GluinoModel(hyperparams)
flow_dequantization_model.load_state_dict(torch.load("results/model_flow_dequantization.pt", map_location=torch.device('cpu')))
flow_dequantization_model.eval()
with torch.no_grad():
    samples_continuous, samples_discrete = flow_dequantization_model.sample(n_samples, batch_size=2048)
    samples_continuous = massless_to_massive(samples_continuous, mass)

    samples_continuous_model.append(samples_continuous)
    samples_discrete_model.append(samples_discrete)

hyperparams["discrete_mode"] = "uniform_argmax"
uniform_argmax_model = GluinoModel(hyperparams)
uniform_argmax_model.load_state_dict(torch.load("results/model_uniform_argmax.pt", map_location=torch.device('cpu')))
uniform_argmax_model.eval()
with torch.no_grad():
    samples_continuous, samples_discrete = uniform_argmax_model.sample(n_samples, batch_size=2048)
    samples_continuous = massless_to_massive(samples_continuous, mass)

    samples_continuous_model.append(samples_continuous)
    samples_discrete_model.append(samples_discrete)

hyperparams["discrete_mode"] = "flow_argmax"
flow_argmax_model = GluinoModel(hyperparams)
flow_argmax_model.load_state_dict(torch.load("results/model_flow_argmax.pt", map_location=torch.device('cpu')))
flow_argmax_model.eval()
with torch.no_grad():
    samples_continuous, samples_discrete = flow_argmax_model.sample(n_samples, batch_size=2048)
    samples_continuous = massless_to_massive(samples_continuous, mass)

    samples_continuous_model.append(samples_continuous)
    samples_discrete_model.append(samples_discrete)
'''
mixture_model = MixtureGluinoModel(hyperparams)
mixture_model.load_state_dict(torch.load("results/model_mixture.pt", map_location=torch.device('cpu')))
mixture_model.eval()
with torch.no_grad():
    samples_continuous, samples_discrete = mixture_model.sample(n_samples, batch_size=2048)
    samples_continuous = massless_to_massive(samples_continuous, mass)

    samples_continuous_model.append(samples_continuous)
    samples_discrete_model.append(samples_discrete)
'''

# ------------------------------ Discrete distributions -----------------------------

fig, axs = plt.subplots(2, 2, sharex='col', gridspec_kw={'hspace': 0, 'height_ratios': [5,2]})
fig.set_size_inches(textwidth, textheight)

# ---------------- Helicity ----------------
hel_configs = torch.tensor([[1,1,1,1,1,1], 
                            [1,1,1,1,1,-1],
                            [1,1,1,1,-1,-1],
                            [1,1,1,-1,-1,-1],
                            [1,1,-1,-1,-1,-1]])
hel_bin_centers = (np.linspace(0, hel_configs.shape[0], hel_configs.shape[0]+1)[1:] + np.linspace(0, hel_configs.shape[0], hel_configs.shape[0]+1)[:-1])/2

hel_fracs_data = torch.zeros(hel_configs.shape[0])
for i, hel in enumerate(hel_configs):
    hel_fracs_data[i] = torch.sum(torch.all(data_discrete[:,:6] == hel, axis=1))/data_discrete.shape[0]

axs[0,0].hist(hel_bin_centers, bins=np.linspace(0, hel_configs.shape[0], hel_configs.shape[0]+1), histtype='step', linewidth=1.5, label='Train', color='black', weights=hel_fracs_data.numpy())
axs[1,0].hist(hel_bin_centers, bins=np.linspace(0, hel_configs.shape[0], hel_configs.shape[0]+1), histtype='step', linewidth=1.5, label='Train', color='black', weights=hel_fracs_data.numpy()/hel_fracs_data.numpy())

for j, (samples_continuous, samples_discrete) in enumerate(zip(samples_continuous_model, samples_discrete_model)):
    hel_fracs_model = torch.zeros(hel_configs.shape[0])
    for i, hel in enumerate(hel_configs):
        hel_fracs_model[i] = torch.sum(torch.all(samples_discrete[:,:6] == hel, axis=1))/samples_discrete.shape[0]

    axs[0,0].hist(hel_bin_centers, bins=np.linspace(0, hel_configs.shape[0], hel_configs.shape[0]+1), histtype='step', linewidth=1., label=labels[j], color=colors[j], weights=hel_fracs_model.numpy())
    axs[1,0].hist(hel_bin_centers, bins=np.linspace(0, hel_configs.shape[0], hel_configs.shape[0]+1), histtype='step', linewidth=1., label=labels[j], color=colors[j], weights=hel_fracs_model.numpy()/hel_fracs_data.numpy())

axs[0,0].set_xlim(0.01, hel_configs.shape[0]-0.01)
axs[0,0].set_ylim(0.001, 0.085)
axs[1,0].set_ylim(0.8, 1.2)

axs[1,0].set_xticks([0.5, 1.5, 2.5, 3.5, 4.5])
axs[1,0].set_xticklabels([r'$(+,+) \to (+,+,+,+)$', r'$(+,+) \to (+,+,+,-)$', r'$(+,+) \to (+,+,-,-)$', r'$(+,+) \to (+,-,-,-)$', r'$(+,+) \to (-,-,-,-)$'], rotation=90, fontsize=6.2)
axs[1,0].minorticks_off()

axs[0,0].set_title(r'Helicity')
axs[0,0].set_ylabel(r'$\sigma_{\mathrm{hel}} / \sigma_{\mathrm{tot}}$')
axs[1,0].set_ylabel(r'Ratio')


# ---------------- Color ----------------
col_configs = torch.tensor([[0,1,2,3,4],
                            [1,0,2,3,4],
                            [2,0,1,3,4],
                            [3,0,1,2,4],
                            [4,0,1,2,3]])
col_bin_centers = (np.linspace(0, col_configs.shape[0], col_configs.shape[0]+1)[1:] + np.linspace(0, col_configs.shape[0], col_configs.shape[0]+1)[:-1])/2

col_fracs_data = torch.zeros(col_configs.shape[0])
for i, col in enumerate(col_configs):
    col_fracs_data[i] = torch.sum(torch.all(data_discrete[:,6:] == col, axis=1))/data_discrete.shape[0]

axs[0,1].hist(col_bin_centers, bins=np.linspace(0, col_configs.shape[0], col_configs.shape[0]+1), histtype='step', label='MC truth', color='black', linewidth=1.5, weights=col_fracs_data.numpy())
axs[1,1].hist(col_bin_centers, bins=np.linspace(0, col_configs.shape[0], col_configs.shape[0]+1), histtype='step', label='MC truth', color='black', linewidth=1.5, weights=col_fracs_data.numpy()/col_fracs_data.numpy())

for j, (samples_continuous, samples_discrete) in enumerate(zip(samples_continuous_model, samples_discrete_model)):
    col_fracs_model = torch.zeros(col_configs.shape[0])
    for i, col in enumerate(col_configs):
        col_fracs_model[i] = torch.sum(torch.all(samples_discrete[:,6:] == col, axis=1))/samples_discrete.shape[0]

    axs[0,1].hist(col_bin_centers, bins=np.linspace(0, col_configs.shape[0], col_configs.shape[0]+1), histtype='step', label=labels[j], color=colors[j], weights=col_fracs_model.numpy())
    axs[1,1].hist(col_bin_centers, bins=np.linspace(0, col_configs.shape[0], col_configs.shape[0]+1), histtype='step', label=labels[j], color=colors[j], weights=col_fracs_model.numpy()/col_fracs_data.numpy())

axs[0,1].set_xlim(0.01, col_configs.shape[0]-0.01)
axs[0,1].set_ylim(0.001, 0.021)
axs[1,1].set_ylim(0.8, 1.2)

axs[1,1].set_xticks([0.5, 1.5, 2.5, 3.5, 4.5])
axs[1,1].set_xticklabels([r'$(1,2) \to (3,4,5,6)$', r'$(1,3) \to (2,4,5,6)$', r'$(1,4) \to (2,3,5,6)$', r'$(1,5) \to (2,3,4,6)$', r'$(1,6) \to (2,3,4,5)$'], rotation=90, fontsize=9)
axs[1,1].minorticks_off()

axs[0,1].set_title(r'Colour')
axs[0,1].set_ylabel(r'$\sigma_{\mathrm{col}} / \sigma_{\mathrm{tot}}$')
axs[1,1].set_ylabel(r'Ratio')

plt.axes(axs[0,0])
leg_handles, leg_labels = axs[0,0].get_legend_handles_labels()
new_handles = [Line2D([], [], c=h.get_edgecolor()) for h in leg_handles]
plt.legend(handles=new_handles, labels=leg_labels)

plt.savefig("plots_discrete.pdf", bbox_inches='tight')
plt.close()




# ------------------------------ Conditional helicity distributions -----------------------------
fig, axs = plt.subplots(2, 3, sharex='col', sharey='row', gridspec_kw={'wspace': 0, 'hspace': 0, 'height_ratios': [5,2]})
fig.set_size_inches(textwidth, textheight)

hel_configs = torch.tensor([[1, 1, -1, 1, -1, 1], # Opposite double flip
                            [1, 1, -1, 1, 1, -1], # Adjacent double flip
                            [1, 1, -1, 1, 1, 1]]) # Single flip (all work)

x_min = 0
x_max = np.pi
bin_width = (x_max - x_min)/n_bins
bin_centers = (np.linspace(x_min, x_max, n_bins)[1:] + np.linspace(x_min, x_max, n_bins)[:-1])/2

for i, hel in enumerate(hel_configs):
    # ---------------- Plot the data ----------------
    dpsi_data = dpsi(data_continuous[torch.all(data_discrete[:,:6] == hel, axis=1)])
    plot_hist(dpsi_data, dpsi_data, axs[0,i], axs[1,i], label='Train', colour='black', linewidth=1.5, x_min=x_min, x_max=x_max, n_bins=n_bins, norm=dpsi_data.shape[0]/data_continuous.shape[0])
    
    # ---------------- Plot the models ----------------
    for j, (samples_continuous, samples_discrete) in enumerate(zip(samples_continuous_model, samples_discrete_model)):
        mask = torch.all(samples_discrete[:,:6] == hel, dim=1)
        samples_continuous_masked = samples_continuous[mask]

        dpsi_samples = dpsi(samples_continuous_masked)
        
        plot_hist(dpsi_samples, dpsi_data, axs[0,i], axs[1,i], label=labels[j], colour=colors[j], linewidth=1., x_min=x_min, x_max=x_max, n_bins=n_bins, norm=dpsi_samples.shape[0]/samples_continuous.shape[0])

    axs[0,i].set_ylim(0.00001, 0.0149)
    axs[1,i].set_xlim(0.0001, np.pi)
    axs[1,i].set_ylim(0.8, 1.2)

    axs[0,i].set_xticks([np.pi/4, np.pi/2, 3*np.pi/4, np.pi])
    axs[0,i].set_xticklabels([r'$\pi/4$', r'$\pi/2$', r'$3\pi/4$', r'$\pi$'])

    axs[1,i].set_xlabel(r'$\Delta \psi_{12}$')

axs[0,0].set_title(r'$(+,+) \to (-,+,-,+)$')
axs[0,1].set_title(r'$(+,+) \to (-,+,+,-)$')
axs[0,2].set_title(r'$(+,+) \to (-,+,+,+)$')

axs[0,0].set_ylabel(r'$\frac{1}{\sigma_{\mathrm{tot}}} \frac{d\sigma}{d\Delta \psi_{12}}$')
axs[1,0].set_ylabel(r'Ratio')

plt.axes(axs[0,0])
leg_handles, leg_labels = axs[0,0].get_legend_handles_labels()
new_handles = [Line2D([], [], c=h.get_edgecolor()) for h in leg_handles]
plt.legend(handles=new_handles, labels=leg_labels, loc='upper right')

plt.savefig("plots_helicity_conditional.pdf", bbox_inches='tight')
plt.close()




# ------------------------------ Conditional color distributions -----------------------------
fig, axs = plt.subplots(2, 3, sharex='col', sharey='row', gridspec_kw={'wspace': 0, 'hspace': 0, 'height_ratios': [5,2]})
fig.set_size_inches(textwidth, textheight)

col_configs = torch.tensor([[1, 3, 0, 2, 4], # Opposite double flip
                            [2, 0, 1, 3, 4], # Adjacent double flip
                            [1, 0, 2, 3, 4]]) # Single flip (all work)

x_min = 601
x_max = 950
bin_width = (x_max - x_min)/n_bins
bin_centers = (np.linspace(x_min, x_max, n_bins)[1:] + np.linspace(x_min, x_max, n_bins)[:-1])/2

for i, col in enumerate(col_configs):
    # ---------------- Plot the data ----------------
    energy_data = data_continuous[torch.all(data_discrete[:,6:] == col, axis=1)].numpy()[:,0,0]
    plot_hist(energy_data, energy_data, axs[0,i], axs[1,i], label='Train', colour='black', linewidth=1.5, x_min=x_min, x_max=x_max, n_bins=n_bins, norm=energy_data.shape[0]/data_continuous.shape[0])
    
    # ---------------- Plot the models ----------------
    for j, (samples_continuous, samples_discrete) in enumerate(zip(samples_continuous_model, samples_discrete_model)):
        mask = torch.all(samples_discrete[:,6:] == col, dim=1)
        samples_continuous_masked = samples_continuous[mask]

        energy_samples = samples_continuous_masked[:,0,0].numpy()        
        plot_hist(energy_samples, energy_data, axs[0,i], axs[1,i], label=labels[j], colour=colors[j], linewidth=1, x_min=x_min, x_max=x_max, n_bins=n_bins, norm=energy_samples.shape[0]/samples_continuous.shape[0])
        
    axs[0,i].set_ylim(0.00001, 0.00009)
    axs[1,i].set_xlim(x_min+1, x_max-1)
    axs[1,i].set_ylim(0.8, 1.2)

    axs[1,i].set_xlabel(r'$E_{\tilde{g}_1}$ [GeV]')

axs[0,0].set_title(r'$(1,3) \to (5,2,4,6)$')
axs[0,1].set_title(r'$(1,4) \to (2,3,5,6)$')
axs[0,2].set_title(r'$(1,3) \to (2,4,5,6)$')

axs[0,0].set_ylabel(r'$\frac{1}{\sigma_{\mathrm{tot}}} \frac{d\sigma}{dE_{\tilde{g}_1}} \mathrm{ [GeV]}^{-1}$')
axs[1,0].set_ylabel(r'Ratio')

plt.axes(axs[0,0])
leg_handles, leg_labels = axs[0,0].get_legend_handles_labels()
new_handles = [Line2D([], [], c=h.get_edgecolor()) for h in leg_handles]
plt.legend(handles=new_handles, labels=leg_labels, loc='upper right')

plt.savefig("plots_color_conditional.pdf", bbox_inches='tight')