import torch
from torch import nn
import torch.optim as optim

from ppflows.rqs_flow.transforms import MaskedPiecewiseRationalQuadraticAutoregressiveTransform

from ppflows.argmax import ArgmaxUniform, ArgmaxBeta, ArgmaxFlow
from ppflows.dequantization import DequantizationUniform, DequantizationBeta, DequantizationFlow
from ppflows.utils import EarlyStopper

from matplotlib import pyplot as plt
import numpy as np

config = {
    "num_categories": 8,
    
    "batch_size": 512,
    "n_RQS_knots": 8,
    "n_made_layers": 2,
    "n_made_units": 10,
    
    "learning_rate": 2e-4,
    "discrete_mode": 'flow_argmax',

    "n_iterations": 300,
    "validation_interval": 25,
    "lr_decay": 0.5,
    "lr_decay_patience": 25,
    "early_stopping_patience": 50,
    "n_training": 100000, 
    "n_validation": 50000,
}

torch.manual_seed(0)
p_categorical = torch.rand(config["num_categories"])
p_categorical /= torch.sum(p_categorical)

class SimpleDiscreteModel(nn.Module):
    def __init__(self, config):
        super(SimpleDiscreteModel, self).__init__()

        self.num_categories_ = config["num_categories"]
        num_categories_per_dim = torch.ones(1)*self.num_categories_

        if config["discrete_mode"] == "uniform_argmax":
            self.discrete_layer_ = ArgmaxUniform(num_categories_per_dim, 0)
        
        if config["discrete_mode"] == "beta_argmax":
            self.discrete_layer_ = ArgmaxBeta(num_categories_per_dim, 0)

        if config["discrete_mode"] == "flow_argmax":
            self.discrete_layer_ = ArgmaxFlow(num_categories_per_dim, 0)
        
        if config["discrete_mode"] == "uniform_dequantization":
            self.discrete_layer_ = DequantizationUniform(num_categories_per_dim, 0)
        
        if config["discrete_mode"] == "beta_dequantization":
            self.discrete_layer_ = DequantizationBeta(num_categories_per_dim, 0)

        if config["discrete_mode"] == "flow_dequantization":
            self.discrete_layer_ = DequantizationFlow(num_categories_per_dim, 0)

        self.flow_dim_ = self.discrete_layer_.num_flow_dimensions()

        # A simple flow layer
        self.flow_transform_ = MaskedPiecewiseRationalQuadraticAutoregressiveTransform(
                features=self.flow_dim_, 
                hidden_features=config["n_made_units"],
                num_bins=config["n_RQS_knots"],
                num_blocks=config["n_made_layers"],
                tails="restricted",
            )

    # Assumes uniform base
    def log_prob(self, inputs):
        total_log_prob = torch.zeros(inputs.shape[0])

        # Discrete layer
        inputs_continuous, discrete_log_prob = self.discrete_layer_.forward(None, inputs)
        total_log_prob += discrete_log_prob

        # Pass through the flow transform
        inputs_continuous, transform_log_prob = self.flow_transform_(inputs_continuous)
        total_log_prob += transform_log_prob

        return total_log_prob

    def sample(self, num_samples):
        total_log_prob = torch.zeros(num_samples)

        sample = torch.rand(num_samples, self.discrete_layer_.num_flow_dimensions())

        # Flow transform
        sample, transform_log_prob = self.flow_transform_.inverse(sample)
        total_log_prob += transform_log_prob

        # Apply discrete layer
        if self.discrete_layer_ != None:
            _, sample_discrete, discrete_log_prob = self.discrete_layer_.inverse(sample)
            total_log_prob += discrete_log_prob

        return sample_discrete, total_log_prob


categorical = torch.distributions.categorical.Categorical(probs = p_categorical)

# Make training data
training_data           = categorical.sample((config["n_training"],)).unsqueeze(dim=1)
train_loader            = torch.utils.data.DataLoader(dataset=training_data, batch_size=config["batch_size"], shuffle=True)

validation_data         = categorical.sample((config["n_validation"],)).unsqueeze(dim=1)
val_loader              = torch.utils.data.DataLoader(dataset=validation_data, batch_size=config["batch_size"], shuffle=True)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

model = SimpleDiscreteModel(config)
model.to(device)

optimizer = optim.Adam(model.parameters(), lr=config["learning_rate"])

# Scheduling with reduction on plateau
scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=config["lr_decay"], patience=config["lr_decay_patience"], verbose=True)

# Early stopping
early_stopper = EarlyStopper(patience=config["early_stopping_patience"])

batch_generator = iter(train_loader)
best_validation_loss = float("inf")
validation_counter = 0
test_loss = 0

with torch.no_grad():
    model_sample_pretraining, log_prob_sample_pretraining = model.sample(config["n_training"])

for iteration in range(config["n_iterations"]):
    # Catch dataloader exceptions when hitting end of dataset
    try:
        train_batch = next(batch_generator)
    except StopIteration:
        # Restart the generator
        batch_generator = iter(train_loader)
        train_batch = next(batch_generator)

    # SGD
    optimizer.zero_grad()
    loss = -model.log_prob(train_batch).mean()    
    loss.backward()
    optimizer.step()
    print(loss.item())

# Compute validation loss
validation_loss = 0
for val_batch in val_loader:
    validation_loss -= torch.sum(model.log_prob(val_batch))

print("Validation loss", validation_loss/config["n_validation"])
print("Theoretical loss", -torch.sum(p_categorical*torch.log(p_categorical)))

with torch.no_grad():
    data_dequantized, log_probs_dequantized = model.discrete_layer_.forward(None, training_data)
    # data_transformed, log_probs_transformed = model.flow_transform_.forward(data_dequantized)
    model_sample, log_prob_sample = model.sample(config["n_training"])

# MC integral for the individual likelihoods
n_points = 100000
monte_carlo_probs = torch.zeros(config["num_categories"])
with torch.no_grad():
    for i in range(config["num_categories"]):
        eval_points = torch.ones(n_points).long().unsqueeze(dim=1)*i
        eval_log_probs = model.log_prob(eval_points)

        average_prob = torch.exp(eval_log_probs).mean()
        monte_carlo_probs[i] = average_prob

n_bins = config["num_categories"]*8
# plt.hist((torch.arange(config["num_categories"])+0.5).numpy()/config["num_categories"], bins=np.linspace(0,1,num=config["num_categories"]+1), weights=p_categorical.numpy(), histtype='step', color='red')
plt.hist(training_data[:,0].numpy()/config["num_categories"], bins=np.linspace(0,1,num=config["num_categories"]+1), weights=config["num_categories"]*np.ones(training_data.shape[0])/training_data.shape[0],  histtype='step', label='data')
plt.hist(data_dequantized[:,0].numpy(), bins=n_bins, weights=n_bins*np.ones(data_dequantized.shape[0])/data_dequantized.shape[0], histtype='step', label='dequantized')
# plt.hist(data_transformed[:,0].numpy(), bins=n_bins, weights=n_bins*np.ones(data_transformed.shape[0])/data_transformed.shape[0], histtype='step', label='transformed')
plt.hist(model_sample[:,0].numpy()/config["num_categories"], bins=np.linspace(0,1,num=config["num_categories"]+1), weights=config["num_categories"]*np.ones(model_sample.shape[0])/model_sample.shape[0],  histtype='step', label='model')
plt.hist(model_sample_pretraining[:,0].numpy()/config["num_categories"], bins=np.linspace(0,1,num=config["num_categories"]+1), weights=config["num_categories"]*np.ones(model_sample_pretraining.shape[0])/model_sample_pretraining.shape[0],  histtype='step', label='model pretraining')
# plt.hist((torch.arange(config["num_categories"]) + 0.5 ).numpy()/config["num_categories"], bins=np.linspace(0,1,num=config["num_categories"]+1), weights=config["num_categories"]*monte_carlo_probs.numpy(), histtype='step', label='MC probs')


print("Original probabilities", p_categorical)
print("Modelled probabilities", monte_carlo_probs)
print("Sum of modelled probabilities", torch.sum(monte_carlo_probs))


plt.legend()
plt.savefig("plot.pdf")