#!/usr/bin/python3

import torch
import torch.optim as optim
import wandb
import os
import argparse
import yaml
import numpy as np

from ppflows.four_top.four_top_auxiliary import get_inference_dataloader
from ppflows.four_top.four_top_model import FourTopModelOrig, FourTopModelFac

from sklearn.metrics import roc_curve, roc_auc_score

from matplotlib import pyplot as plt

with open("hyperparams.yaml") as file:
    hyperparams = yaml.safe_load(file)
    
hyperparams["file_dir"] = os.getcwd()

parser = argparse.ArgumentParser(description='Training for four top model')
parser.add_argument('--model', required=True, choices=['original', 'factorized'], help='type of model')
parser.add_argument('--perm',  required=True, choices=['ordered', 'stochastic'], help='permutation strategy (for factorized model)')

args = parser.parse_args()
hyperparams["model"] = args.model
hyperparams["permutation"] = args.perm

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

if hyperparams["model"] == "original":
    model_background    = FourTopModelOrig(hyperparams)
    model_signal        = FourTopModelOrig(hyperparams)

    model_background.load_state_dict(torch.load("results/model_original_background.pt", map_location=torch.device(device)))
    model_signal.load_state_dict(torch.load("results/model_original_signal.pt", map_location=torch.device(device)))

elif hyperparams["model"] == "factorized":

    hyperparams["data"] = "background"
    model_background    = FourTopModelFac(hyperparams)
    hyperparams["data"] = "signal"
    model_signal        = FourTopModelFac(hyperparams)

    if hyperparams["permutation"] == "ordered":
        model_background.load_state_dict(torch.load("results/model_factorized_background_ordered.pt", map_location=torch.device(device)))
        model_signal.load_state_dict(torch.load("results/model_factorized_signal_ordered.pt", map_location=torch.device(device)))

    elif hyperparams["permutation"] == "stochastic":
        model_background.load_state_dict(torch.load("results/model_factorized_background_stochastic.pt", map_location=torch.device(device)))    
        model_signal.load_state_dict(torch.load("results/model_factorized_signal_stochastic.pt", map_location=torch.device(device)))
    
model_background.to(device)
model_signal.to(device)

loader = get_inference_dataloader(hyperparams)

real_labels = []
log_prob_ratios = [] # signal over background
count = 0
with torch.no_grad():
    # Original model
    if hyperparams["model"] == "original":
        for batch, label in loader:
            log_prob_signal = model_signal.log_prob(batch).numpy()
            log_prob_background = model_background.log_prob(batch).numpy()

            log_prob_ratio = log_prob_signal - log_prob_background
            real_labels.append(label.numpy())
            log_prob_ratios.append(log_prob_ratio)

            if count == 2:
                break

    # Factorized model
    elif hyperparams["model"] == "factorized":
        for batch_cont, batch_disc, label in loader:
            count += 1

            log_prob_signal = model_signal.log_prob(batch_cont, batch_disc).numpy()
            log_prob_background = model_background.log_prob(batch_cont, batch_disc).numpy()

            log_prob_ratio = log_prob_signal - log_prob_background
            real_labels.append(label.numpy())
            log_prob_ratios.append(log_prob_ratio)

real_labels = np.concatenate(real_labels)
log_prob_ratios = np.concatenate(log_prob_ratios)

# Do some gymnastics to avoid issues due to infinities/overflows
scores_1 = np.exp(log_prob_ratios) / (1 + np.exp(log_prob_ratios))
scores_2 = 1 / (1 + np.exp(-log_prob_ratios))

scores_1[np.isnan(scores_1)] = scores_2[np.isnan(scores_1)]
scores_2[np.isnan(scores_2)] = scores_1[np.isnan(scores_2)]

scores = (scores_1 + scores_2)/2

# AUC scores 
print("AUC score:", roc_auc_score(real_labels, scores))

# ROC curves
fpr_score, tpr_score, _ = roc_curve(real_labels, scores)

plt.plot(fpr_score, tpr_score, label="Score 2")
plt.legend()
plt.savefig('ROC.pdf')