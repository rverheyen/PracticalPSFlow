import numpy as np
import torch
import torch.optim as optim
import wandb
import os
import argparse
import yaml
from ray import tune

from ppflows.darkmachines.darkmachines_model import DarkMachinesModel
from ppflows.permuters import IteratedPermutation

from ppflows.utils import EarlyStopper

from ray.tune.integration.wandb import wandb_mixin

def compute_loss_over_dataloader(model, dataloader, hyperparams):
    loss = 0
    data_size = 0
    with torch.no_grad():
        for batch_cont, batch_disc in dataloader:
            loss_now = -model.log_prob(batch_cont, batch_disc).mean()

            loss = loss*data_size + loss_now.item()*batch_cont.shape[0]
            data_size += batch_cont.shape[0]
            loss /= data_size

    return loss

@wandb_mixin
def train(hyperparams, checkpoint_dir=None):
    torch.autograd.set_detect_anomaly(True)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    model = DarkMachinesModel(hyperparams)
    model.to(device)

    train_loader, val_loader, test_loaders, secret_loader = model.dark_machines_data_.convert_channel_to_dataloaders(train=True, validation=True)

    optimizer = optim.Adam(model.parameters(), lr=hyperparams["learning_rate"])

    # Scheduling with reduction on plateau
    scheduler  = optim.lr_scheduler.ReduceLROnPlateau(optimizer,  factor=hyperparams["lr_decay"], patience=hyperparams["lr_decay_patience"])
    
    # Early stopping
    early_stopper = EarlyStopper(patience=hyperparams["early_stopping_patience"])

    batch_generator = iter(train_loader)
    best_validation_loss = float("inf")
    validation_counter = 0
    test_loss = 0
    while True:
        # Catch dataloader exceptions when hitting end of dataset
        try:
            train_batch_cont, train_batch_disc = next(batch_generator)
        except StopIteration:
            # Restart the generator
            batch_generator = iter(train_loader)
            train_batch_cont, train_batch_disc = next(batch_generator)

        # SGD
        optimizer.zero_grad()
        loss = -model.log_prob(train_batch_cont, train_batch_disc).mean()    
        loss.backward()
        optimizer.step()
        print(loss.item())
        # wandb.log({"batch_loss": loss})

        # ---------------- Validation -----------------
        validation_counter += 1
        if validation_counter == hyperparams["validation_interval"]:
            validation_counter = 0

            # Compute validation loss
            validation_loss = compute_loss_over_dataloader(model, val_loader, hyperparams)
            # wandb.log({"validation_loss": validation_loss})

            print("val", validation_loss)
            # Compute test loss and save best model
            if validation_loss < best_validation_loss:
                best_validation_loss = validation_loss
                torch.save(model.state_dict(), "results/chan" + args.channel + "/model.pt")
                exit()
                # torch.save(model, os.path.join(wandb.run.dir, "model.pt"))
                
            # Update lr
            scheduler.step(validation_loss)
            # wandb.log({"learning_rate": optimizer_dense.param_groups[0]['lr']})

            # Check early stopping
            # wandb.log({"bad_iterations": early_stopper.counter_})
            if early_stopper(validation_loss):
                break



parser = argparse.ArgumentParser(description='Train darkmachines model')
parser.add_argument("channel", metavar="chan", type=str)
args = parser.parse_args()

with open("results/chan" + args.channel + "/hyperparams.yaml") as file:
    hyperparams = yaml.safe_load(file)

train(hyperparams)