#!/usr/bin/python3

import torch
import torch.optim as optim
import wandb
import os
import argparse
import numpy as np

from ppflows.four_top.four_top_auxiliary import get_inference_dataloaders_orig
from ppflows.four_top.four_top_model import FourTopModelOrig
from ppflows.permuters import IteratedPermutation

config = {
    "max_num_jets_orig": 4,
    "max_num_leps_orig": 2,
    "train_fraction": 0.9,
    "data": "background",

    "batch_size": 512,
    "n_RQS_knots": 16,
    "n_made_layers": 3,
    "n_made_units_per_dim": 5,
    "n_flow_layers": 8,
    "learning_rate": 1e-3,

    "validation_interval": 50,
    "lr_decay": 0.5,
    "lr_decay_patience": 25,
    "early_stopping_patience": 50,
}

config["file_dir"] = os.getcwd() + "/.."

model_background = torch.load("model_background.pt", map_location=torch.device('cpu'))
model_signal = torch.load("model_signal.pt", map_location=torch.device('cpu'))

inference_loader = get_inference_dataloaders_orig(config)

log_probs_background = []
log_probs_signal = []
with torch.no_grad():
    for batch in inference_loader:
        log_probs_background.append(model_background.log_prob(batch).numpy())
        log_probs_signal.append(model_signal.log_prob(batch).numpy())

log_probs_background = np.concatenate(log_probs_background)
log_probs_signal = np.concatenate(log_probs_signal)
np.savetxt("log_probs_background.csv", log_probs_background, fmt='%1.6f')
np.savetxt("log_probs_signal.csv", log_probs_signal, fmt='%1.6f')