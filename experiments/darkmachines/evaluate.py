from ppflows.darkmachines.darkmachines_model import DarkMachinesModel
import os
import torch
import argparse
import yaml
import numpy as np

parser = argparse.ArgumentParser(description='Train darkmachines model')
parser.add_argument("channel", metavar="chan", type=str)
args = parser.parse_args()

with open("results/chan" + args.channel + "/hyperparams.yaml") as file:
    hyperparams = yaml.safe_load(file)

model = DarkMachinesModel(hyperparams)
model.load_state_dict(torch.load("results/chan" + args.channel + "/model.pt"))
model.eval()

_, _, test_loaders, secret_loader = model.dark_machines_data_.convert_channel_to_dataloaders(test=True, secret=True)

with torch.no_grad():
    for loader, file_name in test_loaders:
        log_probs_list = []
        for train_batch_cont, train_batch_disc in loader:
            log_probs = model.log_prob(train_batch_cont, train_batch_disc)
            log_probs_list.append(log_probs)

        log_probs = torch.cat(log_probs_list)

        # Construct file name
        file_name_out = "results/chan" + args.channel + "/" + file_name.split(".")[0] + "_log_probs.csv"
        np.savetxt(file_name_out, log_probs.numpy())