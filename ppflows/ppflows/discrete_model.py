'''
NOT FUNTIONAL
'''
"""
A simple discrete model with tabulated probabilities.
Relies on a problem-specific 'encoder' that encodes discrete data into an index.
Has the option to include a stochastic permuter.
"""

import torch
from torch import nn

class DiscreteModel(nn.Module):
    def __init__(self, encoder, permuter):
        super(DiscreteModel, self).__init__()

        self.encoder = encoder
        self.permuter = permuter 

        # Register weights
        self.register_buffer("_weights", torch.zeros(encoder.discrete_size(), dtype=torch.float32, requires_grad=False))

        # Register normalization
        self.register_buffer("_normalization", torch.zeros(1))

        # Register maximum weight
        self.register_buffer("_maximum", torch.zeros(1))

    # Training is very simple, only requiring saturation of stochastic permutations
    def train(self, discrete_data, batch_size = 1000, epochs = 100):
        print("Training discrete model.")
        print("Epochs = ",epochs)
        print("Batch size = ",batch_size)

        # Reset the weights
        self._weights = torch.zeros(encoder.discrete_size(), dtype=torch.float32, requires_grad=False)

        n_batches = m.ceil(discrete_data.shape[0]/batch_size)

        for epoch in range(epochs):
            print("Epoch ", epoch)

            permutation = torch.randperm(discrete_data.shape[0])
            for batch in range(n_batches):
                # Set up the batch
                batch_begin = batch*batch_size
                batch_end   = min( (batch+1)*batch_size, discrete_data.shape[0]-1 )
                batch_indices = permutation[batch_begin:batch_end]
                batch_data = discrete_data[batch_indices]

                _,permuted_batch_data = self.permuter.permute(discrete_features=batch_data)

                # Now encode the batch
                encoded_batch = self.encoder.encode_index(permuted_batch_data)

                # Increment weights
                self._weights.index_add_(0, encoded_batch.long(), torch.ones(encoded_batch.shape[0]))

        # At the end, compute the normalization and save
        self._normalization = torch.sum(self._weights)

    def log_prob_from_index(self, indices):
        weights_data = self._weights[indices.long()]

        return weights_data/self._normalization        

    def log_prob(self, discrete_data):
        # Encode to indices
        encoded_data = self.encoder.encode_index(discrete_data)

        return log_prob_from_index(encoded_data)

    def sample(self, n):
        # Roulette wheel selection of probabilities
        sample = torch.tensor(n, dtype=torch.int)
        accepted = torch.zeros(n, dtype=torch.bool)


'''
class DiscreteModelGluinio(nn.Module):
    def __init__(self):
        super(DiscreteModelGluinio, self).__init__()

        # Setting some parameters
        self.helicity_size = 6
        self.colour_size = 5

        self.discrete_permute_classes = np.array([[2,7],[3,8],[4,9],[5,10]])

        # Set up an encoder and a permuter
        self.encoder = DiscreteEncoderGluino(helicity_size=self.helicity_size, 
            colour_size=self.colour_size)
        self.permuter = StochasticPermuter(discrete_permute_classes=self.discrete_permute_classes)

        # Register weights
        self.register_buffer("_weights", torch.zeros(2**self.helicity_size * m.factorial(self.colour_size), dtype=torch.float32, requires_grad=False))

        # Register normalization
        self.register_buffer("_normalization", torch.zeros(1))

        # Register maximum weight
        self.register_buffer("_maximum", torch.zeros(1))

    def train(self, discrete_data, batch_size = 1000, epochs = 1):
        #print("Training discrete model.")
        #print("Epochs = ",epochs)
        #print("Batch size = ",batch_size)

        n_batches = m.ceil(discrete_data.shape[0]/batch_size)

        for epoch in range(epochs):
            #print("Epoch ", epoch)

            permutation = torch.randperm(discrete_data.shape[0])
            for batch in range(n_batches):
                # Set up the batch
                batch_begin = batch*batch_size
                batch_end   = min( (batch+1)*batch_size, discrete_data.shape[0]-1 )
                batch_indices = permutation[batch_begin:batch_end]
                batch_data = discrete_data[batch_indices]

                _,permuted_batch_data = self.permuter.permute(discrete_features=batch_data)

                # Now encode the batch
                encoded_batch = self.encoder.encode_index(permuted_batch_data)

                # Increment weights
                self._weights.index_add_(0, encoded_batch.long(), torch.ones(encoded_batch.shape[0]))

        # At the end, compute the normalization and save
        self._normalization = torch.sum(self._weights)


    def log_prob_from_index(self, indices):
        # Get weights
        weights_data = self._weights[indices.long()]

        return weights_data/self._normalization        

    def log_prob(self, discrete_data):
        # Encode to indices
        encoded_data = self.encoder.encode_index(discrete_data)

        return log_prob_from_index(encoded_data)

    def sample(self, n):
        return 0
'''