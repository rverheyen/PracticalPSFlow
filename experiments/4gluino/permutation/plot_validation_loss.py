import matplotlib as mpl
from matplotlib import pyplot as plt 
import pandas as pd 
import numpy as np

textwidth = 4.5
textheight = textwidth/1.2
fontsize = 10.95

mpl.rcParams['text.usetex'] = True
mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['xtick.direction'] = 'in'
mpl.rcParams['ytick.direction'] = 'in'
mpl.rcParams['lines.linewidth'] = 1.2
mpl.rcParams['lines.markersize'] = 2
mpl.rcParams["legend.labelspacing"] = 0.1
mpl.rcParams["legend.frameon"] = False
mpl.rcParams["legend.handletextpad"] = 0.5
#mpl.rcParams["legend.columnspacing"] = 0
mpl.rcParams.update({'font.size': fontsize})

fig = plt.gcf()
fig.set_size_inches(textwidth, textheight)

df_without_perm = pd.read_csv("results/loss_without_perm.csv").to_numpy()
df_with_perm = pd.read_csv("results/loss_with_perm.csv").to_numpy()

plt.plot(df_without_perm[:,0], df_without_perm[:,1], color="#e41a1c", label='Without permutation')
plt.plot(df_with_perm[:,0], df_with_perm[:,1], color="#377eb8", label='With permutation')
plt.legend()

plt.xlabel('Iteration')
plt.ylabel('Validation negative log-likelihood')

plt.savefig("results/validation_loss.pdf")
