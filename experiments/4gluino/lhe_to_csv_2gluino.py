import numpy as np
import gzip
import shutil
import math as m

from dataclasses import dataclass, field
from xml.etree import ElementTree

from skhep.math import LorentzVector

from numpy.linalg import inv
from scipy.optimize import newton

import argparse

@dataclass
class Particle:
    pdgid: int
    px: float
    py: float
    pz: float
    energy: float
    mass: float
    spin: float
    status: int
    vtau: float
    parent: int
    col: int
    acol: int

    def p4(self):
        return LorentzVector(self.px, self.py, self.pz, self.energy)

    def pVec(self):
        return m.sqrt(self.px**2 + self.py**2 + self.pz**2)

    def cosTheta(self):
        return self.pz/self.pVec()

    def phi(self):
        return m.atan2(self.px, self.py)


@dataclass
class Event:
    particles: list = field(default_factory=list)
    weights: list = field(default_factory=list)
    scale: float = -1
    xsec: float = -1

    def add_particle(self, particle):
        self.particles.append(particle)


class LHEReader():
    def __init__(self, file_path):
        self.file_path = file_path
        self.iterator = ElementTree.iterparse(self.file_path)
        self.current = None
        self.current_weights = None

    def unpack_from_iterator(self):
        # Read the lines for this event
        lines = self.current[1].text.strip().split("\n")
        
        # Create a new event
        event = Event()
        event.scale = float(lines[0].strip().split()[3])
        event.weights = self.current_weights

        # Read header
        xsec = float(lines[0].split()[2])
        event.xsec = xsec
        event_header = lines[0].strip()
        num_part = int(event_header.split()[0].strip())

        # Iterate over particle lines and push back
        for ipart in range(1, num_part+1):
            part_data = lines[ipart].strip().split()
            p = Particle(pdgid = int(part_data[0]),
                        status = int(part_data[1]),
                        parent = int(part_data[2])-1,
                        col  = int(part_data[4]),
                        acol = int(part_data[5]), 
                        px = float(part_data[6]),
                        py = float(part_data[7]),
                        pz = float(part_data[8]),
                        energy = float(part_data[9]),
                        mass = float(part_data[10]),
                        vtau = float(part_data[11]),
                        spin = int(float(part_data[12])))
            event.add_particle(p)

        return event

    def __iter__(self):
        return self

    def __next__(self):
        # Clear XML iterator
        if(self.current):
            self.current[1].clear()

        # Find next event in XML
        element = next(self.iterator)
        while element[1].tag != "event":
            element = next(self.iterator)
        self.current = element
        
        return self.unpack_from_iterator()

parser = argparse.ArgumentParser(description='Converts an lhe file for gg>4 gluino to a csv.')
parser.add_argument("lhe_file", metavar='lhe_filename', help="lhe file name")
parser.add_argument("csv_file", metavar='csv_filename', help='csv file name')
args = parser.parse_args()

out_data = open(args.csv_file, "w")
reader = LHEReader(args.lhe_file)

for iev, event in enumerate(reader):
    assert(len(event.particles) == 4)
    
    out_line = ""

    # Output momenta
    for particle in event.particles:
        if particle.status == 1:
            out_line += "{0:.8g}".format(particle.energy) + "," + "{0:.8g}".format(particle.px) + "," + "{0:.8g}".format(particle.py) + "," + "{0:.8g}".format(particle.pz) + ","

    # Remove last comma, add endline and write
    out_line = out_line[:-1]
    out_line += "\n"
    out_data.write(out_line)
