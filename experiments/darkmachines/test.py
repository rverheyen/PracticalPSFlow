from ppflows.darkmachines.darkmachines_auxiliary import convert_channel_to_dataloaders
import os
import numpy as np
from matplotlib import pyplot as plt

config = {
    "channel": "1",
    "validation_fraction": 0.1,
    "num_objects": 6,

    "batch_size": 512
}

config["file_dir"] = os.getcwd()

file_path = os.getcwd() + "/training_files/chan1/background_chan1_7.79.csv"

training_loader, test_loaders, secret_loader, maxima, minima = convert_channel_to_dataloaders(config)

config = {
    "batch_size": 2048,
    "n_RQS_knots": 8,
    "n_made_layers": 2,
    "n_made_units_per_dim": 10,
    "n_flow_layers": 6,
    "learning_rate": 1e-3,
    "use_residual_blocks": True,
    "use_batch_norm": False,
    "dropout_probability": .0,

    "n_iterations": 100000,
    "validation_interval": 25,
    "lr_decay": 0.5,
    "lr_decay_patience": 25,
    "early_stopping_patience": 50,
}
