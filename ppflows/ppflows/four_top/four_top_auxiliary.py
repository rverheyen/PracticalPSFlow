import torch
import numpy as np
import tqdm 
import pandas as pd
import h5py

# SurFlows-style encoding
def convert_data_to_tensor_fac(config):
    max_num_objects = config["max_objects"]

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    hf = h5py.File(config["file_dir"] + "/4top_lep_400k.h5", 'r')
    data_continuous = torch.tensor(np.array(hf.get('X')))
    data_label = torch.tensor(hf.get('y'), dtype=torch.long)

    n_data = data_continuous.shape[0]

    data_converted_continuous = torch.zeros((n_data, 2 + 4*max_num_objects))
    data_converted_discrete   = torch.zeros((n_data, max_num_objects), dtype=torch.long)

    data_converted_continuous[:,0] = data_continuous[:,6]
    data_converted_continuous[:,1] = data_continuous[:,7]

    data_continuous_reshape = data_continuous[:,8:].reshape(n_data, 12, 4)

    # jet, b-jet, e-, e+, mu-, mu+
    # Set up a set of corresponding categories
    data_discrete = torch.ones(n_data, 12, dtype=torch.long)
    for i in range(4,8):
        data_discrete[:,i] = 2
    data_discrete[:,8]  = 3
    data_discrete[:,9]  = 4
    data_discrete[:,10] = 5
    data_discrete[:,11] = 6

    # Sort by pt
    sort_indices = data_continuous_reshape[:,:,1].sort(dim=1, descending=True)[1]
    data_continuous_reshape = data_continuous_reshape[torch.arange(n_data)[:,None], sort_indices]
    data_discrete = data_discrete[torch.arange(n_data)[:,None], sort_indices]

    for i in range(max_num_objects):
        # Store continuous data
        data_converted_continuous[:,2+4*i:6+4*i] = data_continuous_reshape[:,i]

        # Store discrete data
        data_converted_discrete[:,i] = data_discrete[:,i]

        # Fix empty slots
        empty_mask = data_continuous_reshape[:,i,0] == 0.

        # Continuous data
        data_converted_continuous[empty_mask,2+4*i:6+4*i] = torch.ones(torch.sum(empty_mask), 4)*float('nan')

        # Discrete data
        data_converted_discrete[empty_mask,i] = 0

    # Rescale continuous data to unit gaussian
    means = torch.mean(data_converted_continuous, dim=0)
    stds  = torch.std(data_converted_continuous, dim=0)
    data_converted_continuous = (data_converted_continuous - means)/stds

    return data_converted_continuous, data_converted_discrete, data_label

# DarkMachines-style encoding
# MET, METphi, num_j, num_b, num_g, num_ep, num_em, num_mp, num_mm
def convert_data_to_tensor_orig(config):
    max_num_objects = config["max_objects"]

    hf = h5py.File(config["file_dir"] + "/4top_lep_400k.h5", 'r')
    data_continuous = torch.tensor(np.array(hf.get('X')))
    data_label = torch.tensor(hf.get('y'), dtype=torch.long)
    
    n_data = data_continuous.shape[0]

    data_converted = torch.zeros((n_data, 8 + 5*max_num_objects))

    # Particle counts
    data_converted[:,:6] = data_continuous[:,:6] + torch.rand(n_data, 6) - 0.5

    # MET and METphi
    data_converted[:,6] = data_continuous[:,6]
    data_converted[:,7] = data_continuous[:,7]

    # Reshape to isolate objects
    data_continuous_reshape = data_continuous[:,8:].reshape(n_data, 12, 4)

    # jet, b-jet, e-, e+, mu-, mu+
    # Set up a set of corresponding categories
    data_discrete = torch.ones(n_data, 12)
    for i in range(4,8):
        data_discrete[:,i] = 2
    data_discrete[:,8]  = 3
    data_discrete[:,9]  = 4
    data_discrete[:,10] = 5
    data_discrete[:,11] = 6

    # Dequantize
    data_discrete += torch.rand(data_discrete.shape)

    # Sort by pt
    sort_indices = data_continuous_reshape[:,:,1].sort(dim=1, descending=True)[1]
    data_continuous_reshape = data_continuous_reshape[torch.arange(n_data)[:,None], sort_indices]
    data_discrete = data_discrete[torch.arange(n_data)[:,None], sort_indices]

    for i in range(max_num_objects):
        # Store continuous data
        data_converted[:,8+5*i:12+5*i] = data_continuous_reshape[:,i]

        # Store discrete data
        data_converted[:,12+5*i] = data_discrete[:,i]

        # Fix empty slots
        empty_mask = data_continuous_reshape[:,i,0] == 0.

        # Fill empty slots with numbers between -3 and -6
        data_converted[empty_mask, 8+5*i:13+5*i] = -3*torch.rand(data_converted[empty_mask].shape[0], 5) - 3

    maxima, _ = torch.max(data_converted, dim=0)
    minima, _ = torch.min(data_converted, dim=0)
    data_converted = (data_converted - minima)/(maxima - minima)
    data_converted = torch.nan_to_num(data_converted, -1)

    return data_converted, data_label

def get_training_dataloaders(config, pretrain=False):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    if config["model"] == "original":
        data, labels = convert_data_to_tensor_orig(config)
        if config["data"] == "background":
            label_mask = (labels == 0)
        elif config["data"] == "signal":
            label_mask = (labels == 1)

        # Only keep the data with the right label
        data = data[label_mask]

        # Move to device
        data = data.to(device)

        n_train = int(data.shape[0]*config["train_fraction"])
        training_loader     = torch.utils.data.DataLoader(dataset=data[:n_train], batch_size=config["batch_size"], shuffle=True)
        validation_loader   = torch.utils.data.DataLoader(dataset=data[n_train:], batch_size=config["batch_size"], shuffle=True)

        return training_loader, validation_loader

    elif config["model"] == "factorized":
        data_continuous, data_discrete, labels = convert_data_to_tensor_fac(config)
        if config["data"] == "background":
            label_mask = (labels == 0)
        elif config["data"] == "signal":
            label_mask = (labels == 1)

        # Only keep the data with the right label
        data_continuous = data_continuous[label_mask]
        data_discrete = data_discrete[label_mask]

        if pretrain:
            training_data   = torch.utils.data.TensorDataset(data_continuous, data_discrete)
            training_loader = torch.utils.data.DataLoader(dataset=training_data, batch_size=config["batch_size"])

            return training_loader

        else:
            n_train = int(data_continuous.shape[0]*config["train_fraction"])

            # Move to device
            data_continuous = data_continuous.to(device)
            data_discrete = data_discrete.to(device)

            training_data   = torch.utils.data.TensorDataset(data_continuous[:n_train], data_discrete[:n_train])
            validation_data = torch.utils.data.TensorDataset(data_continuous[n_train:], data_discrete[n_train:])

            training_loader = torch.utils.data.DataLoader(dataset=training_data, batch_size=config["batch_size"], shuffle=True)
            validation_loader   = torch.utils.data.DataLoader(dataset=validation_data, batch_size=config["batch_size"], shuffle=True)

            return training_loader, validation_loader

def get_inference_dataloader(config):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    if config["model"] == "original":
        data, labels = convert_data_to_tensor_orig(config)

        # Move to device
        data = data.to(device)
        labels = labels.to(device)

        combined_data = torch.utils.data.TensorDataset(data, labels)

    elif config["model"] == "factorized":
        data_continuous, data_discrete, labels = convert_data_to_tensor_fac(config)

        # Move to device
        data_continuous = data_continuous.to(device)
        data_discrete = data_discrete.to(device)
        labels = labels.to(device)
        
        combined_data = torch.utils.data.TensorDataset(data_continuous, data_discrete, labels)

    loader = torch.utils.data.DataLoader(dataset=combined_data, batch_size=config["batch_size"])

    return loader