import torch
from torch import nn
import torch.nn.functional as F
import numpy as np
import math

from ppflows.rqs_flow.transforms import MaskedPiecewiseRationalQuadraticAutoregressiveTransform
from ppflows.rqs_flow.transforms import RandomPermutation, CompositeTransform
from ppflows.permuters import StochasticPermutation, IteratedPermutation
from ppflows.distributions import UniformBox, UniformMixtureBox, StandardMixtureNormal

from ppflows.four_top.four_top_auxiliary import get_training_dataloaders

'''
Model for original dark machines encoding
'''
class FourTopModelOrig(nn.Module):
    def __init__(self, hyperparams):
        super(FourTopModelOrig, self).__init__()

        # Number of continuous dimensions
        self.flow_dim_ = 8 + 5*hyperparams["max_objects"]
        
        # ------------------------- Set up the flow model -------------------------
        self.base_dist_ = UniformBox(self.flow_dim_)

        # Set up the flow distribution
        flow_transforms = []
        for _ in range(hyperparams["n_flow_layers"]):
            flow_transforms.append(RandomPermutation(features=self.flow_dim_))
            flow_transforms.append(MaskedPiecewiseRationalQuadraticAutoregressiveTransform(
                features=self.flow_dim_, 
                hidden_features=hyperparams["n_made_units_per_dim"]*self.flow_dim_,
                num_bins=hyperparams["n_RQS_knots"],
                num_blocks=hyperparams["n_made_layers"],
                tails="restricted",
            ))
        self.composite_flow_transform_ = CompositeTransform(flow_transforms)

    # ----- Forward and backward passes -----
    def _forward(self, inputs):
        # Pass through the flow transform
        inputs_continuous, transform_log_prob = self.composite_flow_transform_(inputs)
    
        return inputs_continuous, transform_log_prob

    def log_prob(self, inputs_continuous):
        inputs_continuous, total_log_prob = self._forward(inputs_continuous)

        # Evaluate the base distribution
        total_log_prob += self.base_dist_.log_prob(inputs_continuous)

        return total_log_prob

'''
Model for factorized encoding
Does not do permutation, as the data is already sorted
'''
class FourTopModelFac(nn.Module):
    def __init__(self, hyperparams):
        super(FourTopModelFac, self).__init__()
    
        # Number of continuous dimensions
        self.flow_dim_ = 2 + 4*hyperparams["max_objects"]

        # Number of categories per dim
        num_categories_per_dim = torch.ones(hyperparams["max_objects"])*7
        self.register_buffer("num_categories_per_dim_", num_categories_per_dim, persistent=False)

        total_num_categories = int(torch.prod(self.num_categories_per_dim_).item())

        # Compute the factors required for the encoding of categorical labels 
        category_factors = torch.ones(self.num_categories_per_dim_.shape[0]).long()
        for i in range(1, self.num_categories_per_dim_.shape[0]):
            category_factors[i] = category_factors[i-1]*self.num_categories_per_dim_[i-1]
        self.register_buffer("category_factors_", category_factors, persistent=False)

        # Set up the permutation model
        permute_classes_continuous = []
        permute_classes_discrete = []
        for i in range(hyperparams["max_objects"]):
            permute_classes_continuous.append(np.array([2+4*i, 3+4*i, 4+4*i, 5+4*i]))
            permute_classes_discrete.append(np.array([i]))
        permute_classes_continuous = np.array(permute_classes_continuous)
        permute_classes_discrete = np.array(permute_classes_discrete)

        self.permuter_ = StochasticPermutation(permute_classes_continuous, permute_classes_discrete)

        self.permuter_ = None
        self.pretraining_permuter_ = None
        if "permutation" in hyperparams and hyperparams["permutation"] == "stochastic":
            self.permuter_ = StochasticPermutation(permute_classes_continuous, permute_classes_discrete)
            self.pretraining_permuter_ = IteratedPermutation(permute_classes_continuous, permute_classes_discrete)

        # ------------------------------------------------------------------------
        # ------------------------------- Pretraining ----------------------------
        # ------------------------------------------------------------------------
        train_loader = get_training_dataloaders(hyperparams, pretrain=True)
        
        # Find all categories in the training data
        category_dict = {}

        # If stochastic permutation, run over all permutations of the data
        if self.pretraining_permuter_ is not None:
            while True:
                # Training_loader
                for (_, pretrain_batch_disc) in train_loader:
                    # Permute
                    _, pretrain_batch_disc, _ = self.pretraining_permuter_.forward(None, pretrain_batch_disc)

                    # Encode to category
                    pretrain_batch_disc = torch.sum(self.category_factors_*pretrain_batch_disc, dim=-1).squeeze()

                    # Fill dictionary
                    for category in pretrain_batch_disc:
                        if category.item() in category_dict:
                            category_dict[category.item()] += 1
                        else:
                            category_dict[category.item()] = 1

                # Advance permuter
                if not self.pretraining_permuter_.next():
                    break

        # Otherwise, just run over the data
        else:
            # Training_loader
            for (_, pretrain_batch_disc) in train_loader:
                # Encode to category
                pretrain_batch_disc = torch.sum(self.category_factors_*pretrain_batch_disc, dim=-1).squeeze().long()

                # Fill dictionary
                for category in pretrain_batch_disc:
                    if category.item() in category_dict:
                        category_dict[category.item()] += 1
                    else:
                        category_dict[category.item()] = 1

        category_to_index = torch.ones(total_num_categories).long()*(-1)
        index_to_category = torch.zeros(len(category_dict)).long()
        log_probs_categorical = torch.zeros(len(category_dict))

        # Fill in the maps between index and category
        for index, category in enumerate(category_dict):
            category_to_index[category] = index
            index_to_category[index] = category
            log_probs_categorical[index] = category_dict[category]
        
        # Compute the categorical log probs
        log_probs_categorical /= torch.sum(log_probs_categorical)
        log_probs_categorical = torch.log(log_probs_categorical)

        self.register_buffer("category_to_index_", category_to_index, persistent=False)
        self.register_buffer("index_to_category_", index_to_category, persistent=False)
        
        # Set the embedding
        self.embedding_ = nn.Embedding(len(category_dict), hyperparams["embedding_size"])

        # Base distribution
        self.base_dist_ = StandardMixtureNormal(self.flow_dim_, len(category_dict))
        self.base_dist_.categorical_log_probs_ = log_probs_categorical

        # Set up the flow model
        flow_transforms = []
        for _ in range(hyperparams["n_flow_layers"]):
            flow_transforms.append(RandomPermutation(features=self.flow_dim_))
            flow_transforms.append(MaskedPiecewiseRationalQuadraticAutoregressiveTransform(
                features=self.flow_dim_, 
                hidden_features=hyperparams["n_made_units_per_dim"]*self.flow_dim_,
                context_features=hyperparams["embedding_size"],
                num_bins=hyperparams["n_RQS_knots"],
                num_blocks=hyperparams["n_made_layers"],
                tails="linear", 
                tail_bound = 3
            ))
        self.composite_flow_transform_ = CompositeTransform(flow_transforms)

        # Set up a transform that only contains the permutations of the flow
        # Used for propagating the masking operation in the inverse direction
        permute_transforms = []
        for transform in flow_transforms:
            if isinstance(transform, RandomPermutation):
                permute_transforms.append(transform)
        self.composite_permute_transform_ = CompositeTransform(permute_transforms)

    # ----- Forward and backward passes -----
    def _forward(self, inputs_continuous, inputs_discrete):
        # Permute
        if self.permuter_ is not None:
            inputs_continuous, inputs_discrete, _ = self.permuter_.forward(inputs_continuous, inputs_discrete)

        # Encode the categorical data into single category
        outputs_discrete = torch.sum(self.category_factors_*inputs_discrete, dim=-1).squeeze()
        # Convert category to index
        outputs_discrete = self.category_to_index_[outputs_discrete]

        # Identify any data points with category not in the training set
        unknown_category_mask = outputs_discrete == -1
        
        # For the purposes of passing through the flow, set the category indices of unknown categories to 0
        outputs_discrete[unknown_category_mask] = 0
        
        # Pass through the flow network
        outputs_continuous, transform_log_prob = self.composite_flow_transform_(inputs_continuous, context=self.embedding_(outputs_discrete.clone()))

        # Now correct for unknown categories
        outputs_discrete[unknown_category_mask] = -1
        transform_log_prob[unknown_category_mask] = -float("inf")
        
        return outputs_continuous, outputs_discrete, transform_log_prob

    def log_prob_conditional(self, inputs_continuous, inputs_discrete):
        inputs_continuous, inputs_discrete, total_log_prob = self._forward(inputs_continuous, inputs_discrete)

        # Evaluate the base distribution
        total_log_prob += self.base_dist_.log_prob_conditional(inputs_continuous, inputs_discrete)

        return total_log_prob

    def log_prob(self, inputs_continuous, inputs_discrete):
        inputs_continuous, inputs_discrete, total_log_prob = self._forward(inputs_continuous, inputs_discrete)

        # Evaluate the base distribution
        total_log_prob += self.base_dist_.log_prob_joint(inputs_continuous, inputs_discrete)

        return total_log_prob