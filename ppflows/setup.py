from setuptools import setup, find_packages

setup(
    name='ppflows', 
    version='1.0', 
    packages=find_packages(),
    install_requires=[
        "torch",
        "pandas",
        "wandb",
        "ray[tune]",
        "gdown",
    ],
)
