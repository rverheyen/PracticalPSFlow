#!/usr/bin/python3

import torch
import torch.optim as optim
import wandb
import math
import os
import argparse
import yaml
from ray import tune

from ppflows.gluino.gluino_auxiliary import build_dataloader
from ppflows.gluino.gluino_models import GluinoModel
from ppflows.utils import EarlyStopper

from ray.tune.integration.wandb import wandb_mixin

def compute_loss_over_dataloader(model, dataloader, config):
    loss = 0
    data_size = 0
    with torch.no_grad():
        for _, (batch_cont, _) in enumerate(dataloader):
            loss_now = -model.log_prob(batch_cont, permute=False).mean()

            loss = loss*data_size + loss_now.item()*batch_cont.shape[0]
            data_size += batch_cont.shape[0]
            loss /= data_size

    return loss

@wandb_mixin
def train(config, checkpoint_dir=None):
    train_loader, val_loader, test_loader = build_dataloader(config)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    model = GluinoModel(config)
    model.to(device)

    optimizer = optim.SGD(model.parameters(), lr=config["min_lr"])

    mult = (config["max_lr"] / config["min_lr"]) ** (1/config["n_iterations"])
    lr = config["min_lr"]
    optimizer.param_groups[0]['lr'] = lr

    beta = 0.98
    avg_loss = 0.
    best_loss = float("inf")
    batch_num = 0

    batch_generator = iter(train_loader)
    for iteration in range(config["n_iterations"]):
        batch_num += 1

        # Catch dataloader exceptions when hitting end of dataset
        try:
            train_batch_cont, _ = next(batch_generator)
        except StopIteration:
            # Restart the generator
            batch_generator = iter(train_loader)
            train_batch_cont, _ = next(batch_generator)

        # SGD
        optimizer.zero_grad()
        loss = -model.log_prob(train_batch_cont).mean()    

        avg_loss = beta * avg_loss + (1-beta) * loss.item()
        smoothed_loss = avg_loss / (1 - beta**batch_num)

        #Record the best loss
        if smoothed_loss < best_loss or batch_num==1:
            best_loss = smoothed_loss

        # Do SGD step
        loss.backward()
        optimizer.step()

        # Record
        wandb.log({"loss": smoothed_loss})
        wandb.log({"log_lr": math.log10(lr)})

        # Update LR
        lr *= mult
        optimizer.param_groups[0]['lr'] = lr

        
with open("hyperparams.yaml") as file:
    hyperparams = yaml.safe_load(file)

# Add data dir
hyperparams["data_dir"] = os.getcwd() + "/.."

hyperparams["min_lr"] = 1e-6
hyperparams["max_lr"] = 10
hyperparams["n_iterations"] = 50000
hyperparams["batch_size"] = tune.grid_search([2048, 4096, 8192, 16384])

# Add weights and biases info
hyperparams["wandb"] = {"project": "lr-search","api_key": "4494a6b931b14488cee12d47fb9d7696822c00d7"}

# analysis = tune.run(train, config=config, num_samples=1, resources_per_trial={"cpu": 1})
analysis = tune.run(train, config=hyperparams, resources_per_trial={"gpu": 0.25})