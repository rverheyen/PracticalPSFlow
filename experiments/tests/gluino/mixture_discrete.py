import torch

from ppflows.gluino.gluino_auxiliary import build_dataloader
from ppflows.gluino.gluino_models import MixtureGluinoModel
import os

config = {
    "batch_size": 100,
    "n_RQS_knots": 4,
    "n_made_layers": 2,
    "n_made_units_per_dim": 5,
    "n_flow_layers": 6,
    "embedding_net_depth": 2,
    "embedding_size": 8,

    "permutation": "stochastic", 
    "discrete_mode": "beta_mixture",

    "use_batch_norm": True,
    "use_residual_blocks": True,
    "dropout_probability": 0,

    "n_training": 100, 
    "n_validation": 1,
    "n_test": 1, 
}

config["file_dir"] = os.getcwd()

train_loader, val_loader, test_loader = build_dataloader(config)

# Set up model and optimizer
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = MixtureGluinoModel(config)
model.to(device)

# Train the prior
for train_batch_cont, train_batch_disc in train_loader:
    model.add_to_prior_counts(train_batch_disc)
model.compute_prior_categorical_log_probs()

batch_generator = iter(train_loader)
train_batch_cont, train_batch_disc = next(batch_generator)

forward_cont, forward_disc, forward_log_prob = model._forward(train_batch_cont, train_batch_disc, permute=False)
backward_cont, backward_disc, backward_log_prob = model._backward(forward_cont, forward_disc, permute=False)

print("Difference between data and transformed, continuous piece: ", torch.mean(torch.abs(train_batch_cont - backward_cont)).item())
print("Difference between data and transformed, discrete piece: ", torch.mean(torch.abs(train_batch_disc - backward_disc).float()).item())
print("Difference between forward and backward log probs: ", torch.mean(torch.abs(forward_log_prob + backward_log_prob)).item())

print(model.log_prob_conditional(train_batch_cont, train_batch_disc, permute=False))
print(model.log_prob(train_batch_cont, train_batch_disc, permute=False))

print(model.sample(10, permute=False))
print(model.sample_conditional(train_batch_disc, permute=False))