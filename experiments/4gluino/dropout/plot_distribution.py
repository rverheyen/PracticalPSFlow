import torch
import matplotlib as mpl
from matplotlib import pyplot as plt 
from matplotlib.lines import Line2D

import pandas as pd 
import numpy as np
import yaml

from ppflows.gluino.gluino_models import DropoutGluinoModel
from ppflows.gluino.gluino_auxiliary import massless_to_massive

def plot_hist(data, data_baseline, ax, ax_ratio, label, colour, linewidth, linestyle, x_min, x_max, n_bins, norm):
    bins = np.linspace(x_min, x_max, n_bins)
    bin_width = (x_max - x_min)/n_bins

    # Compute errors
    counts, edges = np.histogram(data, bins=bins)
    errors = np.sqrt(counts)

    # Normalize to cross section
    counts = norm*counts/data.shape[0]/bin_width
    errors = norm*errors/data.shape[0]/bin_width

    centers = (edges[1:] + edges[:-1])/2

    fill_x = np.array([e for edge in bins for e in [edge, edge]][1:-1])
    fill_delta_y = np.array([y for error in errors for y in [error, error]])
    fill_central_y = np.array([c for count in counts for c in [count, count]])

    # Regular plot
    ax.hist(centers, bins=bins, histtype='step', label=label, color=colour, linewidth=linewidth, weights=counts, linestyle=linestyle)
    ax.fill_between(fill_x, fill_central_y - fill_delta_y, fill_central_y + fill_delta_y, facecolor=colour, alpha=0.3)
    
    # Ratio plot
    counts_baseline, _ = np.histogram(data_baseline, bins=bins)
    counts_baseline = norm*counts_baseline/data_baseline.shape[0]/bin_width
    ax_ratio.hist(centers, bins=bins, histtype='step', label=label, color=colour, linewidth=linewidth, weights=counts/counts_baseline, linestyle=linestyle)

    # Errors in ratio
    ratio_counts = counts/counts_baseline
    ratio_counts[np.isinf(ratio_counts)] = 1
    ratio_errors = errors/counts_baseline
    ratio_errors[np.isinf(ratio_errors)] = 1
    
    fill_central_y = np.array([c for count in ratio_counts for c in [count, count]])
    fill_delta_y = np.array([y for error in ratio_errors for y in [error, error]])
    ax_ratio.fill_between(fill_x, fill_central_y - fill_delta_y, fill_central_y + fill_delta_y, facecolor=colour, alpha=0.3)

textwidth = 9
textheight = textwidth/2.4
fontsize = 10.95

colors = ["#e41a1c", "#377eb8", "#4daf4a", "#984ea3", "#ff7f00", "#ffff33", "#a65628", "#f781bf"]

mpl.rcParams['text.usetex'] = True
mpl.rcParams['font.family'] = 'serif'

mpl.rcParams['xtick.direction'] = 'in'
mpl.rcParams['xtick.major.size'] = 4.0
mpl.rcParams['xtick.minor.size'] = 2.0
mpl.rcParams['xtick.major.width'] = 0.5
mpl.rcParams['xtick.minor.width'] = 0.5

mpl.rcParams['ytick.direction'] = 'in'
mpl.rcParams['ytick.major.size'] = 4.0
mpl.rcParams['ytick.minor.size'] = 2.0
mpl.rcParams['ytick.major.width'] = 0.5
mpl.rcParams['ytick.minor.width'] = 0.5

mpl.rcParams['lines.linewidth'] = 1.2
mpl.rcParams['lines.markersize'] = 2
mpl.rcParams["legend.labelspacing"] = 0.1
mpl.rcParams["legend.frameon"] = False
mpl.rcParams["legend.handletextpad"] = 0.5
mpl.rcParams["xtick.minor.visible"] = True
mpl.rcParams["ytick.minor.visible"] = True
#mpl.rcParams["legend.columnspacing"] = 0
mpl.rcParams.update({'font.size': fontsize})

xsec_2_gluino = 4.573364
xsec_4_gluino = 0.00018326
xsec_sum = xsec_2_gluino + xsec_4_gluino
n_bins = 50

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

n_train = 1000000
n_samples = 10000000

data_2_gluino = pd.read_csv("../data_2_gluino.csv", header=None).to_numpy()
data_4_gluino = pd.read_csv("../data_4_gluino.csv", header=None).to_numpy()

mass = np.sqrt(data_4_gluino[0,0]**2 - data_4_gluino[0,1]**2 - data_4_gluino[0,2]**2 - data_4_gluino[0,3]**2)

with open("hyperparams.yaml") as file:
    hyperparams = yaml.safe_load(file)

model = DropoutGluinoModel(hyperparams)
model.load_state_dict(torch.load("results/model_ordered.pt", map_location=torch.device(device)))
model.eval()
model.to(device)

with torch.no_grad():
    samples_2_gluino = model.sample_conditional(torch.ones(n_samples, device=device).long(), batch_size=2048)
    samples_2_gluino[:,:2,:] = massless_to_massive(samples_2_gluino[:,:2,:], mass)
    samples_2_gluino = samples_2_gluino.cpu().numpy()

    samples_4_gluino = model.sample_conditional(torch.zeros(n_samples, device=device).long(), batch_size=2048)
    samples_4_gluino = massless_to_massive(samples_4_gluino, mass)
    samples_4_gluino = samples_4_gluino.cpu().numpy()

fig, axs = plt.subplots(3, 2, sharex='col', gridspec_kw={'hspace': 0, 'height_ratios': [5,1,1]})
gs = axs[1, 1].get_gridspec()
axs[1,1].remove()
axs[2,1].remove()
axtwo = fig.add_subplot(gs[1:, -1], sharex=axs[0,1])

fig.add_subplot
fig.set_size_inches(textwidth, textheight)

# ----------------------------- pT --------------------------------
x_min = 0.001
x_max = 1370
bin_width = (x_max - x_min)/n_bins

pT_data_2_gluino = np.sqrt(data_2_gluino[:,1]**2 + data_2_gluino[:,2]**2)
pT_data_4_gluino = np.sqrt(data_4_gluino[:,1]**2 + data_4_gluino[:,2]**2)

pT_model_2_gluino = np.sqrt(samples_2_gluino[:,0,1]**2 + samples_2_gluino[:,0,2]**2)
pT_model_4_gluino = np.sqrt(samples_4_gluino[:,0,1]**2 + samples_4_gluino[:,0,2]**2)

plot_hist(pT_data_4_gluino, pT_data_4_gluino, axs[0,0], axs[2,0], label='Train (4 gluino)', colour="black", linewidth=1.5, linestyle='solid', x_min=x_min, x_max=x_max, n_bins=n_bins, norm=xsec_4_gluino/(xsec_2_gluino + xsec_4_gluino))
plot_hist(pT_data_2_gluino, pT_data_2_gluino, axs[0,0], axs[1,0], label='Train (2 gluino)', colour="black", linewidth=1.5, linestyle='dashed', x_min=x_min, x_max=x_max, n_bins=n_bins, norm=xsec_2_gluino/(xsec_2_gluino + xsec_4_gluino))
plot_hist(pT_model_4_gluino, pT_data_4_gluino, axs[0,0], axs[2,0], label='Flow + dropout (4 gluino)', colour=colors[0], linewidth=1., linestyle='solid', x_min=x_min, x_max=x_max, n_bins=n_bins, norm=xsec_4_gluino/(xsec_2_gluino + xsec_4_gluino))
plot_hist(pT_model_2_gluino, pT_data_2_gluino, axs[0,0], axs[1,0], label='Flow + dropout (4 gluino)', colour=colors[0], linewidth=1., linestyle='dashed', x_min=x_min, x_max=x_max, n_bins=n_bins, norm=xsec_2_gluino/(xsec_2_gluino + xsec_4_gluino))

axs[0,0].set_xlim(xmin=x_min, xmax=x_max)
axs[0,0].set_yscale('log')
y_minor = mpl.ticker.LogLocator(base = 10.0, subs = np.arange(1.0, 10.0) * 0.1, numticks = 10)
axs[0,0].yaxis.set_minor_locator(y_minor)
axs[0,0].yaxis.set_minor_formatter(mpl.ticker.NullFormatter())


axs[1,0].set_ylim(0.8, 1.2)
axs[2,0].set_ylim(0.8, 1.19999)

axs[2,0].set_xlabel(r'$p_{\perp} $ [GeV]')
axs[0,0].set_ylabel(r'$\frac{1}{\sigma_{\mathrm{sum}}} \frac{d\sigma}{d p_{\perp}} \mathrm{[GeV]}^{-1}$')

# ----------------------------- mass --------------------------------
x_min = 1220
x_max = 1770
bin_width = (x_max - x_min)/n_bins

mgg_data = np.sqrt((data_4_gluino[:,0] + data_4_gluino[:,4])**2 - (data_4_gluino[:,1] + data_4_gluino[:,5])**2 - (data_4_gluino[:,2] + data_4_gluino[:,6])**2 - (data_4_gluino[:,3] + data_4_gluino[:,7])**2)
mgg_model = np.sqrt((samples_4_gluino[:,0,0] + samples_4_gluino[:,1,0])**2 - (samples_4_gluino[:,0,1] + samples_4_gluino[:,1,1])**2 - (samples_4_gluino[:,0,2] + samples_4_gluino[:,1,2])**2 - (samples_4_gluino[:,0,3] + samples_4_gluino[:,1,3])**2)

plot_hist(mgg_data, mgg_data, axs[0,1], axtwo, label='Train (4 gluino)', colour='black', linewidth=1.5, linestyle='solid', x_min=x_min, x_max=x_max, n_bins=n_bins, norm=xsec_4_gluino/(xsec_2_gluino + xsec_4_gluino))
plot_hist(mgg_model, mgg_data, axs[0,1], axtwo, label='Flow + dropout (4 gluino)', colour=colors[0], linewidth=1., linestyle='solid', x_min=x_min, x_max=x_max, n_bins=n_bins, norm=xsec_4_gluino/(xsec_2_gluino + xsec_4_gluino))

'''
mgg_data_bins, _, _  = axs[0,1].hist(mgg_data, histtype='step', bins=np.linspace(x_min, x_max, n_bins), label='MC truth (4 gluino)', color="black", linewidth=1.5, weights=xsec_4_gluino*np.ones(mgg_data.shape[0])/mgg_data.shape[0]/bin_width/xsec_sum)
mgg_model_bins, _, _ = axs[0,1].hist(mgg_model, histtype='step', bins=np.linspace(x_min, x_max, n_bins), label='Flow + dropout (4 gluino)', color=colors[0], weights=xsec_4_gluino*np.ones(mgg_model.shape[0])/mgg_model.shape[0]/bin_width/xsec_sum)

mgg_bin_centers = (np.linspace(x_min, x_max, n_bins)[1:] + np.linspace(x_min, x_max, n_bins)[:-1])/2

mgg_ratio = mgg_model_bins/mgg_data_bins
mgg_ratio[np.isinf(mgg_ratio)] = 1

axtwo.hist(mgg_bin_centers, bins=np.linspace(x_min, x_max, n_bins), histtype='step', color="black", linewidth=1.5, weights=np.ones(mgg_ratio.shape[0]))
axtwo.hist(mgg_bin_centers, bins=np.linspace(x_min, x_max, n_bins), histtype='step', color=colors[0], weights=mgg_ratio)
'''
axs[0,1].set_ylim(ymin=1e-10, ymax=1.49e-7)
axtwo.set_ylim(0.8, 1.2)
axtwo.set_xlim(xmin=x_min, xmax=x_max)

axtwo.set_xlabel(r'$m_{\tilde{g}\tilde{g}}$ [GeV]')
axs[0,1].set_ylabel(r'$\frac{1}{\sigma_{\mathrm{sum}}} \frac{d\sigma}{m_{\tilde{g}\tilde{g}}} \mathrm{[GeV]}^{-1}$')

plt.axes(axs[0,0])
handles, labels = axs[0,0].get_legend_handles_labels()
new_handles = [Line2D([], [], c=h.get_edgecolor(), linestyle=h.get_linestyle()) for h in handles]
plt.legend(handles=new_handles, labels=labels, loc=(0.27, 0.44))

plt.savefig("plots.pdf", bbox_inches='tight')
plt.close()

'''
# ----------------------------- costheta --------------------------------
x_min = -1
x_max = 1
bin_width = (x_max - x_min)/n_bins

cos_data_2_gluino = data_2_gluino[:,3]/np.sqrt(data_2_gluino[:,1]**2 + data_2_gluino[:,2]**2 + data_2_gluino[:,3]**2)
cos_data_4_gluino = data_4_gluino[:,3]/np.sqrt(data_4_gluino[:,1]**2 + data_4_gluino[:,2]**2 + data_4_gluino[:,3]**2)

cos_model_2_gluino = samples_2_gluino[:,0,3]/np.sqrt(samples_2_gluino[:,0,1]**2 + samples_2_gluino[:,0,2]**2 + samples_2_gluino[:,0,3]**2)
cos_model_4_gluino = samples_4_gluino[:,0,3]/np.sqrt(samples_4_gluino[:,0,1]**2 + samples_4_gluino[:,0,2]**2 + samples_4_gluino[:,0,3]**2)

ax = plt.gca()

plt.hist(cos_data_2_gluino, histtype='step', bins=np.linspace(x_min, x_max, n_bins), label='MC truth (2 gluino)', color=colors[0], weights=xsec_2_gluino*np.ones(cos_data_2_gluino.shape[0])/cos_data_2_gluino.shape[0]/bin_width/xsec_sum)
plt.hist(cos_data_4_gluino, histtype='step', bins=np.linspace(x_min, x_max, n_bins), label='MC truth (4 gluino)', color=colors[0], weights=xsec_4_gluino*np.ones(cos_data_4_gluino.shape[0])/cos_data_4_gluino.shape[0]/bin_width/xsec_sum, linestyle='dashed')
plt.hist(cos_model_2_gluino, histtype='step', bins=np.linspace(x_min, x_max, n_bins), label='model dropout (2 gluino)', color=colors[1], weights=xsec_2_gluino*np.ones(cos_model_2_gluino.shape[0])/cos_model_2_gluino.shape[0]/bin_width/xsec_sum)
plt.hist(cos_model_4_gluino, histtype='step', bins=np.linspace(x_min, x_max, n_bins), label='model dropout (4 gluino)', color=colors[1], weights=xsec_4_gluino*np.ones(cos_model_4_gluino.shape[0])/cos_model_4_gluino.shape[0]/bin_width/xsec_sum, linestyle='dashed')

plt.yscale('log')

handles, labels = ax.get_legend_handles_labels()
new_handles = [Line2D([], [], c=h.get_edgecolor()) for h in handles]
plt.legend(handles=new_handles, labels=labels)

plt.xlabel(r'$\cos \theta $')
plt.ylabel(r'$\frac{1}{\sigma_{\mathrm{sum}}} \frac{d\sigma}{d \cos \theta}$')

plt.savefig("plot_cos.pdf")
plt.close()
'''