import torch

from ppflows.gluino.gluino_auxiliary import build_dataloader
from ppflows.gluino.gluino_models import GluinoModel
import os

config = {
    "batch_size": 100,
    "n_RQS_knots": 4,
    "n_made_layers": 2,
    "n_made_units_per_dim": 5,
    "n_flow_layers": 6,
    "embedding_net_depth": 2,
    "embedding_size": 8,

    "permutation": "stochastic", 
    "discrete_mode": "uniform_dequantization",

    "use_batch_norm": True,
    "use_residual_blocks": True,
    "dropout_probability": 0,

    "n_training": 100, 
    "n_validation": 1,
    "n_test": 1, 
}

config["file_dir"] = os.getcwd()

train_loader, val_loader, test_loader = build_dataloader(config)

# Set up model and optimizer
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = GluinoModel(config)
model.to(device)

batch_generator = iter(train_loader)
train_batch_cont, train_batch_disc = next(batch_generator)

forward_cont, forward_log_prob = model._forward(train_batch_cont, train_batch_disc, permute=False)
backward_cont, backward_disc, backward_log_prob = model._backward(forward_cont, permute=False)

print("Difference between data and transformed, continuous piece: ", torch.mean(torch.abs(train_batch_cont - backward_cont)).item())
if backward_disc is not None:
    print("Difference between data and transformed, discrete piece: ", torch.mean(torch.abs(train_batch_disc - backward_disc).float()).item())
print("Difference between forward and backward log probs: ", torch.mean(torch.abs(forward_log_prob + backward_log_prob)).item())

print(model.log_prob(train_batch_cont, train_batch_disc))
print(model.sample(100))

