import torch
import matplotlib as mpl
from matplotlib import pyplot as plt 
from matplotlib.lines import Line2D
import yaml
from ppflows.gluino.gluino_models import GluinoModel

import pandas as pd 
import numpy as np

from ppflows.gluino.gluino_auxiliary import massless_to_massive

textwidth = 9
textheight = textwidth/2.4
fontsize = 10.95

colors = ["#e41a1c", "#377eb8", "#4daf4a", "#984ea3", "#ff7f00", "#ffff33", "#a65628", "#f781bf"]
labels = ["Flow (gluino 1)", "Flow (gluino 2)", "Flow (gluino 3)", "Flow (gluino 4)"]

mpl.rcParams['text.usetex'] = True
mpl.rcParams['font.family'] = 'serif'

mpl.rcParams['xtick.direction'] = 'in'
mpl.rcParams['xtick.major.size'] = 4.0
mpl.rcParams['xtick.minor.size'] = 2.0
mpl.rcParams['xtick.major.width'] = 0.5
mpl.rcParams['xtick.minor.width'] = 0.5

mpl.rcParams['ytick.direction'] = 'in'
mpl.rcParams['ytick.major.size'] = 4.0
mpl.rcParams['ytick.minor.size'] = 2.0
mpl.rcParams['ytick.major.width'] = 0.5
mpl.rcParams['ytick.minor.width'] = 0.5

mpl.rcParams['lines.linewidth'] = 1.2
mpl.rcParams['lines.markersize'] = 2
mpl.rcParams["legend.labelspacing"] = 0.1
mpl.rcParams["legend.frameon"] = False
mpl.rcParams["legend.handletextpad"] = 0.5
mpl.rcParams["xtick.minor.visible"] = True
mpl.rcParams["ytick.minor.visible"] = True
#mpl.rcParams["legend.columnspacing"] = 0
mpl.rcParams.update({'font.size': fontsize})

n_bins = 50
x_min = 601
x_max = 999
bin_width = (x_max - x_min)/n_bins
n_samples = 1000000

# Get the baseline
df = pd.read_csv("../data_4_gluino.csv", header=None).to_numpy()
mass = np.sqrt(df[0,0]**2 - df[0,1]**2 - df[0,2]**2 - df[0,3]**2)

with open("hyperparams.yaml") as file:
    hyperparams = yaml.safe_load(file)

# Generate from model_without_perm
model_without_perm = GluinoModel(hyperparams)
model_without_perm.load_state_dict(torch.load("results/model_without_perm.pt", map_location=torch.device('cpu')))
model_without_perm.eval()
with torch.no_grad():
    samples_without_perm, _ = model_without_perm.sample(n_samples, batch_size=2048)
    samples_without_perm = massless_to_massive(samples_without_perm, mass)

# Generate from model_with_perm
hyperparams["permutation"] = "stochastic"
model_with_perm = GluinoModel(hyperparams)
model_with_perm.load_state_dict(torch.load("results/model_with_perm.pt", map_location=torch.device('cpu')))
model_with_perm.eval()
with torch.no_grad():
    samples_with_perm, _ = model_with_perm.sample(n_samples, batch_size=2048)
    samples_with_perm = massless_to_massive(samples_with_perm, mass)


fig, axs = plt.subplots(2, 2, sharex=True, gridspec_kw={'hspace': 0, 'height_ratios': [5,2]})
fig.set_size_inches(textwidth, textheight)

# Plot baseline
MC_bins, MC_edges, _ = axs[0,0].hist(df[:,0], bins=np.linspace(x_min, x_max, n_bins), histtype='step', label='MC truth', color='black', linewidth=1.5, weights=np.ones(df.shape[0])/df.shape[0]/bin_width)
MC_bins, MC_edges, _ = axs[0,1].hist(df[:,0], bins=np.linspace(x_min, x_max, n_bins), histtype='step', label='MC truth', color='black', linewidth=1.5, weights=np.ones(df.shape[0])/df.shape[0]/bin_width)

MC_bin_centers = (MC_edges[1:] + MC_edges[:-1])/2

axs[1,0].hist(MC_bin_centers, bins=np.linspace(x_min, x_max, n_bins), histtype='step', color='black', linewidth=1.5, weights=np.ones(MC_bin_centers.shape[0]))
axs[1,1].hist(MC_bin_centers, bins=np.linspace(x_min, x_max, n_bins), histtype='step', color='black', linewidth=1.5, weights=np.ones(MC_bin_centers.shape[0]))

for i in range(4):
    without_perm_bins, _, _ = axs[0,0].hist(samples_without_perm.numpy()[:,i,0], bins=np.linspace(x_min, x_max, n_bins), histtype='step', label=labels[i], color=colors[i], weights=np.ones(n_samples)/n_samples/bin_width)
    with_perm_bins, _, _    = axs[0,1].hist(samples_with_perm.numpy()[:,i,0], bins=np.linspace(x_min, x_max, n_bins), histtype='step', label=labels[i], color=colors[i], weights=np.ones(n_samples)/n_samples/bin_width)

    axs[1,0].hist(MC_bin_centers, bins=np.linspace(x_min, x_max, n_bins), histtype='step', label=labels[i], color=colors[i], weights=without_perm_bins/MC_bins)
    axs[1,1].hist(MC_bin_centers, bins=np.linspace(x_min, x_max, n_bins), histtype='step', label=labels[i], color=colors[i], weights=with_perm_bins/MC_bins)

axs[0,0].set_xlim(xmin=x_min+1, xmax=x_max-1)
axs[0,1].set_xlim(xmin=x_min+1, xmax=x_max-1)
axs[0,0].set_ylim(ymin=1e-4, ymax=0.0059)
axs[0,1].set_ylim(ymin=1e-4, ymax=0.0059)
axs[1,0].set_ylim(ymin=0.8, ymax=1.2)
axs[1,1].set_ylim(ymin=0.8, ymax=1.2)

axs[1,0].set_xlabel(r'$E_{\tilde{g}} $ [GeV]')
axs[1,1].set_xlabel(r'$E_{\tilde{g}} $ [GeV]')
axs[0,0].set_ylabel(r'$\frac{1}{\sigma} \frac{d\sigma}{dE_{\tilde{g}}} \mathrm{[GeV]}^{-1}$')
axs[1,0].set_ylabel(r'Flow/MC')

axs[0,0].set_title("Without permutation")
axs[0,1].set_title("With permutation")

plt.axes(axs[0,0])
handles, labels = axs[0,0].get_legend_handles_labels()
new_handles = [Line2D([], [], c=h.get_edgecolor()) for h in handles]
plt.legend(handles=new_handles, labels=labels)

plt.savefig("results/plots.pdf", bbox_inches='tight')