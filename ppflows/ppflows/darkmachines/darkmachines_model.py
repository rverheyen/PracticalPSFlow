import torch
from torch import nn
import torch.nn.functional as F
import numpy as np
import math

from ppflows.rqs_flow.transforms import MaskedPiecewiseRationalQuadraticAutoregressiveTransform
from ppflows.rqs_flow.transforms import RandomPermutation, CompositeTransform

from ppflows.darkmachines.darkmachines_auxiliary import DarkMachinesData

from ppflows.distributions import StandardMixtureNormal
from ppflows.permuters import StochasticPermutation, IteratedPermutation
from ppflows.rqs_flow.transforms import Permutation

'''
Model that includes permutation, dropout and discrete mixture for darkmachines data
'''

class DarkMachinesModel(nn.Module):
    def __init__(self, hyperparams):
        super(DarkMachinesModel, self).__init__()

        # DarkMachinesData object
        self.dark_machines_data_ = DarkMachinesData(hyperparams)
    
        # Number of continuous dimensions
        self.flow_dim_ = 2 + 4*hyperparams["num_objects"]

        # Number of categories per dim
        num_categories_per_dim = torch.ones(hyperparams["num_objects"])*8
        self.register_buffer("num_categories_per_dim_", num_categories_per_dim, persistent=False)

        total_num_categories = int(torch.prod(self.num_categories_per_dim_).item())
    
        # Set up the permutation model
        permute_classes_continuous = []
        permute_classes_discrete = []
        for i in range(hyperparams["num_objects"]):
            permute_classes_continuous.append(np.array([2+4*i, 3+4*i, 4+4*i, 5+4*i]))
            permute_classes_discrete.append(np.array([i]))
        permute_classes_continuous = np.array(permute_classes_continuous)
        permute_classes_discrete = np.array(permute_classes_discrete)

        self.permuter_ = StochasticPermutation(permute_classes_continuous, permute_classes_discrete)
        # Permuter used for pretraining
        self.pretraining_permuter_ = IteratedPermutation(permute_classes_continuous, permute_classes_discrete)

        # ------------------------------------------------------------------------
        # Categorical encoding proceeds as follows:
        # - Particle IDs are converted into a category using category_factors_
        # - The category is converted into an index using category_to_index_
        # - The index is transformed to an embedding using embedding_
        # The inverse proceeds as:
        # - The index is converted into a category using index_to_category_
        # - The category is converted to particle IDs using category_factors_
        # ------------------------------------------------------------------------

        # Compute the factors required for the encoding of categorical labels 
        category_factors = torch.ones(self.num_categories_per_dim_.shape[0]).long()
        for i in range(1, self.num_categories_per_dim_.shape[0]):
            category_factors[i] = category_factors[i-1]*self.num_categories_per_dim_[i-1]
        self.register_buffer("category_factors_", category_factors, persistent=False)

        # ------------------------------------------------------------------------
        # ------------------------------- Pretraining ----------------------------
        # ------------------------------------------------------------------------
        # Load training data to compute embeddings 
        train_loader, _, _, _ = self.dark_machines_data_.convert_channel_to_dataloaders(train=True)

        # Find all categories in the training data
        category_dict = {}
        while True:
            # Training_loader
            for (_, pretrain_batch_disc) in train_loader:
                # Permute
                _, pretrain_batch_disc, _ = self.pretraining_permuter_.forward(None, pretrain_batch_disc)

                # Encode to category
                pretrain_batch_disc = torch.sum(self.category_factors_*pretrain_batch_disc, dim=-1).squeeze()

                # Fill dictionary
                for category in pretrain_batch_disc:
                    if category.item() in category_dict:
                        category_dict[category.item()] += 1
                    else:
                        category_dict[category.item()] = 1

            # Advance permuter
            if not self.pretraining_permuter_.next():
                break

        category_to_index = torch.ones(total_num_categories).long()*(-1)
        index_to_category = torch.zeros(len(category_dict)).long()
        log_probs_categorical = torch.zeros(len(category_dict))

        # Fill in the maps between index and category
        for index, category in enumerate(category_dict):
            category_to_index[category] = index
            index_to_category[index] = category
            log_probs_categorical[index] = category_dict[category]
        
        # Compute the categorical log probs
        log_probs_categorical /= torch.sum(log_probs_categorical)
        log_probs_categorical = torch.log(log_probs_categorical)

        self.register_buffer("category_to_index_", category_to_index, persistent=False)
        self.register_buffer("index_to_category_", index_to_category, persistent=False)
        
        # Set the embedding
        self.embedding_ = nn.Embedding(len(category_dict), hyperparams["embedding_size"])

        # Base distribution
        self.base_dist_ = StandardMixtureNormal(self.flow_dim_, len(category_dict))
        self.base_dist_.categorical_log_probs_ = log_probs_categorical

        # Set up the flow model
        flow_transforms = []
        for _ in range(hyperparams["n_flow_layers"]):
            flow_transforms.append(RandomPermutation(features=self.flow_dim_))
            flow_transforms.append(MaskedPiecewiseRationalQuadraticAutoregressiveTransform(
                features=self.flow_dim_, 
                hidden_features=hyperparams["n_made_units_per_dim"]*self.flow_dim_,
                context_features=hyperparams["embedding_size"],
                num_bins=hyperparams["n_RQS_knots"],
                num_blocks=hyperparams["n_made_layers"],
                tails="linear", 
                tail_bound = 3
            ))
        self.composite_flow_transform_ = CompositeTransform(flow_transforms)

        # Set up a transform that only contains the permutations of the flow
        # Used for propagating the masking operation in the inverse direction
        permute_transforms = []
        for transform in flow_transforms:
            if isinstance(transform, RandomPermutation):
                permute_transforms.append(transform)
        self.composite_permute_transform_ = CompositeTransform(permute_transforms)

    # ----- Forward and backward passes -----
    def _forward(self, inputs_continuous, inputs_discrete):
        # Permute
        inputs_continuous, inputs_discrete, permute_log_prob = self.permuter_.forward(inputs_continuous, inputs_discrete)

        # Encode the categorical data into single category
        # Skip encoding if only one categorical dimension
        outputs_discrete = torch.sum(self.category_factors_*inputs_discrete, dim=-1).squeeze()
        # Convert category to index
        outputs_discrete = self.category_to_index_[outputs_discrete]

        # Identify any data points with category not in the training set
        unknown_category_mask = outputs_discrete == -1
        
        # For the purposes of passing through the flow, set the category indices of unknown categories to 0
        outputs_discrete[unknown_category_mask] = 0
        
        # Pass through the flow network
        outputs_continuous, transform_log_prob = self.composite_flow_transform_(inputs_continuous, context=self.embedding_(outputs_discrete.clone()))

        # Now correct for unknown categories
        outputs_discrete[unknown_category_mask] = -1
        transform_log_prob[unknown_category_mask] = -float("inf")
        
        return outputs_continuous, outputs_discrete, transform_log_prob + permute_log_prob

    def _inverse(self, inputs_continuous, inputs_discrete):
        # inputs_discrete is an index here - convert to category first
        inputs_discrete_index = self.index_to_category_[inputs_discrete]
        # Then convert to particle IDs
        outputs_discrete = torch.empty((inputs_continuous.shape[0], self.num_categories_per_dim_.shape[0]), dtype=torch.long, device=inputs_continuous.device)
        for i in range(self.num_categories_per_dim_.shape[0]-1, -1, -1):
            outputs_discrete[:,i] = (inputs_discrete_index - inputs_discrete_index % self.category_factors_[i])/self.category_factors_[i]
            inputs_discrete_index -= outputs_discrete[:,i]*self.category_factors_[i]

        # Perform dropout
        dropout_mask = torch.zeros_like(inputs_continuous).bool()
        # Empty entries
        dropout_mask[:,2:] = torch.repeat_interleave(outputs_discrete == 0, 4, dim=1)
        # Non-jet entries
        for i in range(outputs_discrete.shape[1]):
            dropout_mask[:,5+4*i] = torch.logical_or(outputs_discrete[:,i] == 0, outputs_discrete[:,i] > 2)
        
        # Pass the mask through the permute transform
        dropout_mask, _ = self.composite_permute_transform_.forward(dropout_mask)

        # Then apply dropout
        inputs_continuous[dropout_mask] = float('nan')

        # Pass through the flow network
        outputs_continuous, transform_log_prob = self.composite_flow_transform_.inverse(inputs_continuous, context=self.embedding_(inputs_discrete))
        
        # Permute
        outputs_continuous, outputs_discrete, permute_log_prob = self.permuter_.inverse(outputs_continuous, outputs_discrete)

        return outputs_continuous, outputs_discrete, transform_log_prob + permute_log_prob

    def log_prob_conditional(self, inputs_continuous, inputs_discrete):
        inputs_continuous, inputs_discrete, total_log_prob = self._forward(inputs_continuous, inputs_discrete)

        # Evaluate the base distribution
        total_log_prob += self.base_dist_.log_prob_conditional(inputs_continuous, inputs_discrete)

        return total_log_prob

    def log_prob(self, inputs_continuous, inputs_discrete):
        inputs_continuous, inputs_discrete, total_log_prob = self._forward(inputs_continuous, inputs_discrete)

        # Evaluate the base distribution
        total_log_prob += self.base_dist_.log_prob_joint(inputs_continuous, inputs_discrete)

        return total_log_prob

    def sample(self, num_samples, batch_size=None):
        if batch_size is None:
            return self._sample(num_samples)
        
        else:
            num_batches = num_samples // batch_size
            num_leftover = num_samples % batch_size
            samples_continuous = []
            samples_discrete = []

            for _ in range(num_batches):
                sample_continuous, sample_discrete = self._sample(batch_size)
                samples_continuous.append(sample_continuous)
                samples_discrete.append(sample_discrete)
            
            if num_leftover > 0:
                sample_continuous, sample_discrete = self._sample(num_leftover)
                samples_continuous.append(sample_continuous)
                samples_discrete.append(sample_discrete)

        return torch.cat(samples_continuous, dim=0), torch.cat(samples_discrete, dim=0)


    def sample_conditional(self, inputs_discrete, batch_size=None):
        if batch_size is None:
            return self._sample_conditional(inputs_discrete)

        else:
            num_batches = inputs_discrete.shape[0] // batch_size
            num_leftover = inputs_discrete.shape[0] % batch_size
            samples_continuous = []
            samples_discrete = []

            for i in range(num_batches):
                sample_continuous, sample_discrete = self._sample_conditional(inputs_discrete[i*batch_size : (i+1)*batch_size])
                samples_continuous.append(sample_continuous)
                samples_discrete.append(sample_discrete)
            
            if num_leftover > 0:
                sample_continuous, sample_discrete = self._sample_conditional(inputs_discrete[num_batches*batch_size : ])
                samples_continuous.append(sample_continuous)
                samples_discrete.append(sample_discrete)
        
        return torch.cat(samples_continuous, dim=0), torch.cat(samples_discrete, dim=0)


    # NOTE: This function does not unnormalize the data
    def _sample(self, num_samples):
        samples_continuous, samples_discrete = self.base_dist_.sample(num_samples)

        samples_continuous, samples_discrete, _ = self._inverse(samples_continuous, samples_discrete)
        return samples_continuous, samples_discrete

    # NOTE: This function expects particle ID categories
    # NOTE: For categories that are not in the training data, sampling is skipped
    def _sample_conditional(self, inputs_discrete):
        # Encode the categorical data into single category
        # Skip encoding if only one categorical dimension
        samples_discrete = torch.sum(self.category_factors_*inputs_discrete, dim=-1).squeeze()
        # Convert category to index
        samples_discrete = self.category_to_index_[samples_discrete]

        # Kick out -1's
        samples_discrete = samples_discrete[samples_discrete != -1]
    
        samples_continuous = self.base_dist_.sample_conditional(samples_discrete)

        samples_continuous, samples_discrete, _ = self._inverse(samples_continuous, samples_discrete)
        return samples_continuous, samples_discrete

    