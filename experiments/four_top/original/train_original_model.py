#!/usr/bin/python3

import torch
import torch.optim as optim
import wandb
import os
import argparse
from ray import tune

from ppflows.four_top.four_top_auxiliary import get_training_dataloaders_orig
from ppflows.four_top.four_top_model import FourTopModelOrig
from ppflows.permuters import IteratedPermutation

from ppflows.utils import EarlyStopper

from ray.tune.integration.wandb import wandb_mixin

def compute_loss_over_dataloader(model, dataloader, config):
    loss = 0
    data_size = 0
    with torch.no_grad():
        for batch in dataloader:
            loss_now = -model.log_prob(batch).mean()

            loss = loss*data_size + loss_now.item()*batch.shape[0]
            data_size += batch.shape[0]
            loss /= data_size

    return loss

@wandb_mixin
def train(config, checkpoint_dir=None):
    if config["data"] == "background":
        train_loader, val_loader, _, _ = get_training_dataloaders_orig(config)
    elif config["data"] == "signal":
        _, _, train_loader, val_loader = get_training_dataloaders_orig(config)
        
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    model = FourTopModelOrig(config)
    model.to(device)

    optimizer = optim.Adam(model.parameters(), lr=config["learning_rate"])

    # Scheduling with reduction on plateau
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=config["lr_decay"], patience=config["lr_decay_patience"], verbose=True)

    # Early stopping
    early_stopper = EarlyStopper(patience=config["early_stopping_patience"])

    batch_generator = iter(train_loader)
    best_validation_loss = float("inf")
    validation_counter = 0
    test_loss = 0
    while True:
        # Catch dataloader exceptions when hitting end of dataset
        try:
            train_batch = next(batch_generator)
        except StopIteration:
            # Restart the generator
            batch_generator = iter(train_loader)
            train_batch = next(batch_generator)

        # SGD
        optimizer.zero_grad()
        loss = -model.log_prob(train_batch).mean()    
        loss.backward()
        optimizer.step()
        wandb.log({"batch_loss": loss})

        # ---------------- Validation -----------------
        validation_counter += 1
        if validation_counter == config["validation_interval"]:
            validation_counter = 0

            # Compute validation loss
            validation_loss = compute_loss_over_dataloader(model, val_loader, config)
            wandb.log({"validation_loss": validation_loss})

            # Compute test loss and save best model
            if validation_loss < best_validation_loss:
                best_validation_loss = validation_loss
                torch.save(model, os.path.join(wandb.run.dir, "model.pt"))
                
            # Update lr
            scheduler.step(validation_loss)
            wandb.log({"learning_rate": optimizer.param_groups[0]['lr']})

            # Check early stopping
            wandb.log({"bad_iterations": early_stopper.counter_})
            if early_stopper(validation_loss):
                break
            
config = {
    "max_num_jets_orig": 4,
    "max_num_leps_orig": 2,
    "train_fraction": 0.9,
    "data": "background",

    "batch_size": 512,
    "n_RQS_knots": 16,
    "n_made_layers": 3,
    "n_made_units_per_dim": 5,
    "n_flow_layers": 8,
    "learning_rate": 1e-3,

    "validation_interval": 50,
    "lr_decay": 0.5,
    "lr_decay_patience": 25,
    "early_stopping_patience": 50,
}

config["file_dir"] = os.getcwd() + "/.."

# Add weights and biases info
config["wandb"] = {"project": "four-top-original-background","api_key": "4494a6b931b14488cee12d47fb9d7696822c00d7"}

analysis = tune.run(train, config=config, num_samples=2, resources_per_trial={"gpu": 0.5})

config["wandb"] = {"project": "four-top-original-signal","api_key": "4494a6b931b14488cee12d47fb9d7696822c00d7"}
config["data"] = "signal"

analysis = tune.run(train, config=config, num_samples=2, resources_per_trial={"gpu": 0.5})