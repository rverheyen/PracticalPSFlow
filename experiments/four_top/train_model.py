#!/usr/bin/python3

import torch
import torch.optim as optim
import wandb
import os
import argparse
import yaml
from ray import tune

from ppflows.four_top.four_top_auxiliary import get_training_dataloaders
from ppflows.four_top.four_top_model import FourTopModelOrig, FourTopModelFac

from ray.tune.integration.wandb import wandb_mixin

def compute_loss_over_dataloader(model, dataloader, config):
    loss = 0
    data_size = 0
    with torch.no_grad():
        for batch in dataloader:
            if config["model"] == "original":
                loss_now = -model.log_prob(batch).mean()    
                loss = loss*data_size + loss_now.item()*batch.shape[0]
                data_size += batch.shape[0]
                loss /= data_size
            elif config["model"] == "factorized":
                loss_now = -model.log_prob(batch[0], batch[1]).mean()
                loss = loss*data_size + loss_now.item()*batch[0].shape[0]
                data_size += batch[0].shape[0]
                loss /= data_size

    return loss
            
with open("hyperparams.yaml") as file:
    hyperparams = yaml.safe_load(file)
    
hyperparams["file_dir"] = os.getcwd()

parser = argparse.ArgumentParser(description='Training for four top model')
parser.add_argument('--data',  required=True, choices=['background', 'signal'], help='background or signal')
parser.add_argument('--model', required=True, choices=['original', 'factorized'], help='type of model')
parser.add_argument('--perm',  required=True, choices=['ordered', 'stochastic'], help='permutation strategy (for factorized model)')

args = parser.parse_args()
hyperparams["data"] = args.data
hyperparams["model"] = args.model
hyperparams["permutation"] = args.perm


# ----------------------------------- Training -----------------------------------
wandb.init(project='four-top', config=hyperparams)
new_name = args.model + "-" + args.data + "-" + args.perm + "-" + wandb.run.id
wandb.run.name = new_name
wandb.run.save()

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

if hyperparams["model"] == "original":
    model = FourTopModelOrig(hyperparams)
elif hyperparams["model"] == "factorized":
    model = FourTopModelFac(hyperparams)
model.to(device)

# Train loader
train_loader, val_loader = get_training_dataloaders(hyperparams)
    
optimizer = optim.Adam(model.parameters(), lr=hyperparams["learning_rate"])

# Scheduling with reduction on plateau
scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=hyperparams["lr_decay"], patience=hyperparams["lr_decay_patience"], verbose=True)

batch_generator = iter(train_loader)
best_validation_loss = float("inf")
validation_counter = 0
test_loss = 0
while True:
    # Catch dataloader exceptions when hitting end of dataset
    try:
        train_batch = next(batch_generator)
    except StopIteration:
        # Restart the generator
        batch_generator = iter(train_loader)
        train_batch = next(batch_generator)

    # SGD
    optimizer.zero_grad()
    if hyperparams["model"] == "original":
        loss = -model.log_prob(train_batch).mean()    
    elif hyperparams["model"] == "factorized":
        loss = -model.log_prob(train_batch[0], train_batch[1]).mean()
    loss.backward()
    optimizer.step()

    # ---------------- Validation -----------------
    validation_counter += 1
    if validation_counter == hyperparams["validation_interval"]:
        validation_counter = 0

        # Compute validation loss
        validation_loss = compute_loss_over_dataloader(model, val_loader, hyperparams)

        # Compute test loss and save best model
        if validation_loss < best_validation_loss:
            best_validation_loss = validation_loss
            torch.save(model.state_dict(), os.path.join(wandb.run.dir, "model.pt"))
            
        # Update lr
        scheduler.step(validation_loss)

        wandb.log({ "batch_loss": loss,
                    "num_bad_epochs": scheduler.num_bad_epochs,
                    "learning_rate": optimizer.param_groups[0]['lr'],
                    "validation_loss": validation_loss})

        # Break when lr has been reduced by 3 orders of magnitude
        if optimizer.param_groups[0]['lr'] < hyperparams["learning_rate"]*1e-3:
            break