import torch
from torch import nn
import torch.nn.functional as F
import numpy as np
import math

from ppflows.rqs_flow.transforms import MaskedPiecewiseRationalQuadraticAutoregressiveTransform
from ppflows.rqs_flow.transforms import RandomPermutation, CompositeTransform
from ppflows.gluino.gluino_auxiliary import build_dataloader

from ppflows.gluino.gluino_auxiliary import GluinoEncoder, energy_ordered_permutation
from ppflows.distributions import BetaMixtureBox, UniformBox, UniformMixtureBox
from ppflows.permuters import IteratedPermutation, StochasticPermutation, SortPermutation
from ppflows.argmax import ArgmaxUniform, ArgmaxFlow
from ppflows.dequantization import DequantizationUniform, DequantizationFlow
from ppflows.rqs_flow.transforms import Permutation


'''
Model that includes permutations and discrete features through a surjective layer
'''
class GluinoModel(nn.Module):
    def __init__(self, hyperparams):
        super(GluinoModel, self).__init__()

        # Gluino encoder
        self.encoder_  = GluinoEncoder()

        # Number of continuous dimensions
        dim_continuous = 8

        # ------------------------- Set up the discrete model -------------------------
        self.num_categories_per_dim_ = torch.tensor([64,120])
        
        self.discrete_layer_ = None
        if "discrete_mode" in hyperparams:
            assert(hyperparams["permutation"] == "stochastic" or hyperparams["permutation"] == "ordered")

            # Option 1: argmax with uniform dequantization
            if hyperparams["discrete_mode"] == "uniform_argmax":
                self.discrete_layer_ = ArgmaxUniform(self.num_categories_per_dim_, dim_continuous)
            # Option 2: argmax with flow distribution dequantization
            if hyperparams["discrete_mode"] == "flow_argmax":
                self.discrete_layer_ = ArgmaxFlow(self.num_categories_per_dim_, dim_continuous)
            # Option 3: dequantization with uniform dequantization
            if hyperparams["discrete_mode"] == "uniform_dequantization":
                self.discrete_layer_ = DequantizationUniform(self.num_categories_per_dim_, dim_continuous)
            # Option 4: dequantization with flow dequantization
            if hyperparams["discrete_mode"] == "flow_dequantization":
                self.discrete_layer_ = DequantizationFlow(self.num_categories_per_dim_, dim_continuous)

        # Set the flow dimension
        if self.discrete_layer_ is not None:
            self.flow_dim_ = self.discrete_layer_.num_flow_dimensions()
        else:
            self.flow_dim_ = dim_continuous

        # ------------------------- Set up the permutation model -------------------------
        # Only include discrete permutation if we are doing a discrete model
        permute_classes_continuous = np.array([[0],[1],[2],[3]])
        if self.discrete_layer_ is not None:
            permute_classes_discrete = np.array([[2,7],[3,8],[4,9],[5,10]])
        else:
            permute_classes_discrete = None

        # Default corresponds with no permutation
        self.permuter_ = None
        if "permutation" in hyperparams:
            if hyperparams["permutation"] == "stochastic":
                self.permuter_ = StochasticPermutation(permute_classes_continuous, permute_classes_discrete)
            if hyperparams["permutation"] == "ordered":
                self.permuter_ = SortPermutation(energy_ordered_permutation, permute_classes_continuous, permute_classes_discrete)
        
        # ------------------------- Set up the flow model -------------------------
        self.base_dist_ = UniformBox(self.flow_dim_)

        # Set up the flow distribution
        flow_transforms = []
        for _ in range(hyperparams["n_flow_layers"]):
            flow_transforms.append(RandomPermutation(features=self.flow_dim_))
            flow_transforms.append(MaskedPiecewiseRationalQuadraticAutoregressiveTransform(
                features=self.flow_dim_, 
                hidden_features=hyperparams["n_made_units_per_dim"]*self.flow_dim_,
                num_bins=hyperparams["n_RQS_knots"],
                num_blocks=hyperparams["n_made_layers"],
                tails="restricted",
            ))
        self.composite_flow_transform_ = CompositeTransform(flow_transforms)

    # ----- Forward and backward passes -----
    def _forward(self, inputs_continuous, inputs_discrete, permute=True):
        total_log_prob = torch.zeros(inputs_continuous.shape[0], device=inputs_continuous.device)

        # Permute
        permute_log_prob = 0
        if permute and self.permuter_ is not None:
            if self.discrete_layer_ is not None:
                inputs_continuous, inputs_discrete, permute_log_prob = self.permuter_.forward(inputs_continuous, inputs_discrete)
            else:
                inputs_continuous, _, permute_log_prob = self.permuter_.forward(inputs_continuous, None)
        total_log_prob += permute_log_prob

        # Encoder
        inputs_continuous, encode_log_prob = self.encoder_.encode_continuous(inputs_continuous)
        total_log_prob += encode_log_prob
        if self.discrete_layer_ is not None:
            inputs_discrete = self.encoder_.encode_discrete(inputs_discrete)

        # Discrete layer
        if self.discrete_layer_ is not None:
            inputs_continuous, discrete_log_prob = self.discrete_layer_.forward(inputs_continuous, inputs_discrete)
            total_log_prob += discrete_log_prob

        # Pass through the flow transform
        inputs_continuous, transform_log_prob = self.composite_flow_transform_(inputs_continuous)
        total_log_prob += transform_log_prob

        return inputs_continuous, total_log_prob

    def _inverse(self, inputs_continuous, permute=True):
        total_log_prob = torch.zeros(inputs_continuous.shape[0], device=inputs_continuous.device)

        # Flow transforms
        inputs_continuous, transform_log_prob = self.composite_flow_transform_.inverse(inputs_continuous)
        total_log_prob += transform_log_prob

        # Apply discrete layer
        inputs_discrete = None
        if self.discrete_layer_ != None:
            inputs_continuous, inputs_discrete, discrete_log_prob = self.discrete_layer_.inverse(inputs_continuous)
            total_log_prob += discrete_log_prob

            # Kick out samples with non-viable category
            category_mask = ~torch.any(inputs_discrete > self.num_categories_per_dim_, dim=-1)
            inputs_continuous = inputs_continuous[category_mask]
            inputs_discrete = inputs_discrete[category_mask]
            total_log_prob = total_log_prob[category_mask]
        
        # Decode 
        inputs_continuous, decode_log_prob = self.encoder_.decode_continuous(inputs_continuous)
        total_log_prob += decode_log_prob
        if inputs_discrete != None:
            inputs_discrete = self.encoder_.decode_discrete(inputs_discrete)
        
        # Permute
        permute_log_prob = 0
        if permute and self.permuter_ is not None:
            if self.discrete_layer_ is not None:
                inputs_continuous, inputs_discrete, permute_log_prob = self.permuter_.inverse(inputs_continuous, inputs_discrete)
            else:
                inputs_continuous, _, permute_log_prob = self.permuter_.inverse(inputs_continuous, None)
        total_log_prob += permute_log_prob

        return inputs_continuous, inputs_discrete, total_log_prob

    # ----- Sampling and likelihood evaluation -----
    def log_prob(self, inputs_continuous, inputs_discrete=None, permute=True):
        inputs_continuous, total_log_prob = self._forward(inputs_continuous, inputs_discrete, permute=permute)

        # Evaluate the base distribution
        total_log_prob += self.base_dist_.log_prob(inputs_continuous)

        return total_log_prob

    def sample(self, num_samples, batch_size=None, permute=True):
        if batch_size is None:
            return self._sample(num_samples, permute=permute)
        
        else:
            num_batches = num_samples // batch_size
            num_leftover = num_samples % batch_size
            samples_continuous = []
            samples_discrete = []

            for _ in range(num_batches):
                sample_continuous, sample_discrete = self._sample(batch_size, permute=permute)
                samples_continuous.append(sample_continuous)
                samples_discrete.append(sample_discrete)
            
            if num_leftover > 0:
                sample_continuous, sample_discrete = self._sample(num_leftover, permute=permute)
                samples_continuous.append(sample_continuous)
                samples_discrete.append(sample_discrete)

        if self.discrete_layer_ is not None:
            return torch.cat(samples_continuous, dim=0), torch.cat(samples_discrete, dim=0)
        else:
            return torch.cat(samples_continuous, dim=0), None

    def _sample(self, num_samples, permute):
        # Sample from the base distribution
        samples_continuous = self.base_dist_.sample(num_samples)

        samples_continuous, samples_discrete, _ = self._inverse(samples_continuous, permute=permute)

        return samples_continuous, samples_discrete

#------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------
'''
Model that includes discrete features through a mixture prior
'''
class MixtureGluinoModel(nn.Module):
    def __init__(self, hyperparams):
        super(MixtureGluinoModel, self).__init__()

        # Gluino encoder
        self.encoder_  = GluinoEncoder()

        # Number of continuous dimensions
        self.flow_dim_ = 8

        # Number of categories per dimension
        self.num_categories_per_dim_ = torch.tensor([64,120])
        self.total_num_categories_ = torch.prod(self.num_categories_per_dim_).item()

        # ------------------------ Embedding for categories -----------------------
        self.embedding_size_ = math.ceil(math.sqrt(self.total_num_categories_))
        self.embedding_ = nn.Embedding(self.total_num_categories_, self.embedding_size_)
        
        # ------------------------- Set up the permutation model -------------------------
        # Only include discrete permutation if we are doing a discrete model
        permute_classes_continuous  = np.array([[0],[1],[2],[3]])
        permute_classes_discrete    = np.array([[2,7],[3,8],[4,9],[5,10]])

        self.permuter_ = None
        if "permutation" in hyperparams:
            if hyperparams["permutation"] == "stochastic":
                self.permuter_ = StochasticPermutation(permute_classes_continuous, permute_classes_discrete)
                self.pretraining_permuter_ = IteratedPermutation(permute_classes_continuous, permute_classes_discrete)
            elif hyperparams["permutation"] == "ordered":
                self.permuter_ = SortPermutation(energy_ordered_permutation, permute_classes_continuous, permute_classes_discrete)
                        
        # ------------------------- Set up the flow model -------------------------
        self.base_dist_ = UniformMixtureBox(self.flow_dim_, self.total_num_categories_)
        
        # Set up the flow distribution
        flow_transforms = []
        for _ in range(hyperparams["n_flow_layers"]):
            flow_transforms.append(RandomPermutation(features=self.flow_dim_))
            flow_transforms.append(MaskedPiecewiseRationalQuadraticAutoregressiveTransform(
                features=self.flow_dim_, 
                hidden_features=hyperparams["n_made_units_per_dim"]*self.flow_dim_,
                context_features=self.embedding_size_,
                num_bins=hyperparams["n_RQS_knots"],
                num_blocks=hyperparams["n_made_layers"],
                tails="restricted",
            ))
        self.composite_flow_transform_ = CompositeTransform(flow_transforms)

        # ------------------------------------------------------------------------
        # ------------------------------- Pretraining ----------------------------
        # ------------------------------------------------------------------------
        train_loader = build_dataloader(hyperparams, pretrain=True)

        log_probs_categorical = torch.zeros(self.total_num_categories_).long()

        # Separate cases for ordered or stochastic permutation
        if hyperparams["permutation"] == "ordered":
            # Loop over the data a single time
            for (pretrain_batch_cont, pretrain_batch_disc) in train_loader:
                # Permute
                pretrain_batch_cont, pretrain_batch_disc, _ = self.permuter_.forward(pretrain_batch_cont, pretrain_batch_disc)

                # Encode
                pretrain_batch_disc = self.encoder_.encode_discrete(pretrain_batch_disc)

                # Encode to a single number
                pretrain_batch_disc = pretrain_batch_disc[:,0] + pretrain_batch_disc[:,1]*self.num_categories_per_dim_[0]

                # Add to categorical counts
                log_probs_categorical.index_add_(0, pretrain_batch_disc, torch.ones(pretrain_batch_disc.shape[0]).long())


        elif hyperparams["permutation"] == "stochastic":
            # Loop over the data until we ran over all permutations
            while True:
                # Training_loader
                for (pretrain_batch_cont, pretrain_batch_disc) in train_loader:
                    # Permute
                    pretrain_batch_cont, pretrain_batch_disc, _ = self.pretraining_permuter_.forward(pretrain_batch_cont, pretrain_batch_disc)

                    # Encode
                    pretrain_batch_disc = self.encoder_.encode_discrete(pretrain_batch_disc)

                    # Encode to a single number
                    pretrain_batch_disc = pretrain_batch_disc[:,0] + pretrain_batch_disc[:,1]*self.num_categories_per_dim_[0]

                    # Add to categorical counts
                    log_probs_categorical.index_add_(0, pretrain_batch_disc, torch.ones(pretrain_batch_disc.shape[0]).long())

                # Advance permuter
                if not self.pretraining_permuter_.next():
                    break

        # Convert to float
        log_probs_categorical = log_probs_categorical.float()

        # Compute the categorical log probs
        log_probs_categorical /= torch.sum(log_probs_categorical)
        log_probs_categorical = torch.log(log_probs_categorical)

        self.base_dist_.categorical_log_probs_ = log_probs_categorical
        
    # ----- Forward and backward passes -----
    def _forward(self, inputs_continuous, inputs_discrete, permute=True):
        total_log_prob = torch.zeros(inputs_continuous.shape[0], device=inputs_continuous.device)

        # Permute
        if permute:
            inputs_continuous, inputs_discrete, permute_log_prob = self.permuter_.forward(inputs_continuous, inputs_discrete)
            total_log_prob += permute_log_prob

        # Encoder
        inputs_continuous, encode_log_prob = self.encoder_.encode_continuous(inputs_continuous)
        inputs_discrete = self.encoder_.encode_discrete(inputs_discrete)
        inputs_discrete = inputs_discrete[:,0] + inputs_discrete[:,1]*self.num_categories_per_dim_[0]
        total_log_prob += encode_log_prob

        # Pass through the flow transform
        inputs_continuous, transform_log_prob = self.composite_flow_transform_(inputs_continuous, context=self.embedding_(inputs_discrete))
        total_log_prob += transform_log_prob

        return inputs_continuous, inputs_discrete, total_log_prob

    def _inverse(self, inputs_continuous, inputs_discrete, permute=True):
        total_log_prob = torch.zeros(inputs_continuous.shape[0], device=inputs_continuous.device)

        # Flow transforms
        inputs_continuous, transform_log_prob = self.composite_flow_transform_.inverse(inputs_continuous, context=self.embedding_(inputs_discrete))
        total_log_prob += transform_log_prob
        
        # Decode 
        inputs_discrete = torch.stack((inputs_discrete % self.num_categories_per_dim_[0], inputs_discrete // self.num_categories_per_dim_[0]), dim=1)
        inputs_continuous, decode_log_prob = self.encoder_.decode_continuous(inputs_continuous)
        inputs_discrete = self.encoder_.decode_discrete(inputs_discrete)
        total_log_prob += decode_log_prob

        # Permute
        if permute and self.permuter_ != None:
            inputs_continuous, inputs_discrete, permute_log_prob = self.permuter_.inverse(inputs_continuous, inputs_discrete)
            total_log_prob += permute_log_prob

        return inputs_continuous, inputs_discrete, total_log_prob

    # ----- Sampling and likelihood evaluation -----
    def log_prob_conditional(self, inputs_continuous, inputs_discrete, permute=True):
        inputs_continuous, inputs_discrete, total_log_prob = self._forward(inputs_continuous, inputs_discrete, permute=permute)

        # Evaluate the base distribution
        total_log_prob += self.base_dist_.log_prob_conditional(inputs_continuous, inputs_discrete)

        return total_log_prob

    def log_prob(self, inputs_continuous, inputs_discrete, permute=True):
        inputs_continuous, inputs_discrete, total_log_prob = self._forward(inputs_continuous, inputs_discrete, permute=permute)

        # Evaluate the base distribution
        total_log_prob += self.base_dist_.log_prob_joint(inputs_continuous, inputs_discrete)

        return total_log_prob

    def sample(self, num_samples, batch_size=None, permute=True):
        if batch_size is None:
            return self._sample(num_samples, permute=permute)
        
        else:
            num_batches = num_samples // batch_size
            num_leftover = num_samples % batch_size
            samples_continuous = []
            samples_discrete = []

            for _ in range(num_batches):
                sample_continuous, sample_discrete = self._sample(batch_size, permute=permute)
                samples_continuous.append(sample_continuous)
                samples_discrete.append(sample_discrete)
            
            if num_leftover > 0:
                sample_continuous, sample_discrete = self._sample(num_leftover, permute=permute)
                samples_continuous.append(sample_continuous)
                samples_discrete.append(sample_discrete)

        return torch.cat(samples_continuous, dim=0), torch.cat(samples_discrete, dim=0)
    
    def sample_conditional(self, inputs_discrete, batch_size=None, permute=True):
        if batch_size is None:
            return self._sample_conditional(inputs_discrete, permute=permute)

        else:
            num_batches = inputs_discrete.shape[0] // batch_size
            num_leftover = inputs_discrete.shape[0] % batch_size
            samples_continuous = []
            samples_discrete = []

            for i in range(num_batches):
                sample_continuous, sample_discrete = self._sample_conditional(inputs_discrete[i*batch_size : (i+1)*batch_size], permute=permute)
                samples_continuous.append(sample_continuous)
                samples_discrete.append(sample_discrete)
            
            if num_leftover > 0:
                sample_continuous, sample_discrete = self._sample_conditional(inputs_discrete[num_batches*batch_size : ], permute=permute)
                samples_continuous.append(sample_continuous)
                samples_discrete.append(sample_discrete)
        
        return torch.cat(samples_continuous, dim=0), torch.cat(samples_discrete, dim=0)

    def _sample(self, num_samples, permute=True):
        samples_continuous, samples_discrete = self.base_dist_.sample(num_samples)

        samples_continuous, samples_discrete, _ = self._inverse(samples_continuous, samples_discrete, permute=permute)
        return samples_continuous, samples_discrete

    def _sample_conditional(self, inputs_discrete, permute=True):
        # This function expects unencoded input -> have to encode
        inputs_discrete = self.encoder_.encode_discrete(inputs_discrete)
        inputs_discrete = inputs_discrete[:,0] + inputs_discrete[:,1]*self.num_categories_per_dim_[0]

        samples_continuous = self.base_dist_.sample_conditional(inputs_discrete)

        samples_continuous, samples_discrete, _ = self._inverse(samples_continuous, inputs_discrete, permute=permute)
        return samples_continuous, samples_discrete

#------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------
'''
Model that mixes 2gluino and 4gluino samples through a mixture prior
'''
class DropoutGluinoModel(nn.Module):
    def __init__(self, hyperparams):
        super(DropoutGluinoModel, self).__init__()

        # Gluino encoder
        self.encoder_  = GluinoEncoder()

        # Number of continuous dimensions
        self.flow_dim_ = 8
        
        # ------------------------- Set up the permutation model -------------------------
        # Only include discrete permutation if we are doing a discrete model
        permute_classes_continuous  = np.array([[0],[1],[2],[3]])

        # Default corresponds with no permutation
        self.permuter_ = None
        if "permutation" in hyperparams:
            if hyperparams["permutation"] == "stochastic":
                self.permuter_ = StochasticPermutation(permute_classes_continuous, None)
            if hyperparams["permutation"] == "ordered":
                self.permuter_ = SortPermutation(energy_ordered_permutation, permute_classes_continuous, None)

        
        # ----------------- Embedding for dropout categories ----------------        
        self.embedding_ = nn.Embedding(2, 2)

        # ------------------------- Set up the flow model -------------------------
        self.base_dist_ = UniformMixtureBox(self.flow_dim_, 2)
        # These are the log of the normalized cross sections
        self.base_dist_.categorical_log_probs_ = torch.tensor([-10.1248937579, -0.00004007035])

        # Set up the flow distribution
        flow_transforms = []
        for _ in range(hyperparams["n_flow_layers"]):
            flow_transforms.append(RandomPermutation(features=self.flow_dim_))
            flow_transforms.append(MaskedPiecewiseRationalQuadraticAutoregressiveTransform(
                features=self.flow_dim_, 
                hidden_features=hyperparams["n_made_units_per_dim"]*self.flow_dim_,
                context_features=2,
                num_bins=hyperparams["n_RQS_knots"],
                num_blocks=hyperparams["n_made_layers"],
                tails="restricted",
            ))
        self.composite_flow_transform_ = CompositeTransform(flow_transforms)  

        # We need to figure out where dropped indices end up after permutations
        dropout_indices = torch.ones(1, 8)
        dropout_indices[0,0] = 0
        dropout_indices[0,1] = 0
        for transform in self.composite_flow_transform_._transforms:
            if isinstance(transform, RandomPermutation):
                dropout_indices, _ = transform(dropout_indices)
        self.register_buffer("dropout_indices_", dropout_indices.squeeze().bool())
    
    # ----- Forward and backward passes -----
    def _forward(self, inputs_continuous, permute=True):
        total_log_prob = torch.zeros(inputs_continuous.shape[0], device=inputs_continuous.device)

        # Determine the dropout category
        # 0 corresponds with no dropout
        # 1 corresponds with dropout
        inputs_discrete = torch.any(inputs_continuous[:,:,0] == 0, dim=1).long()

        # Permute - Only acts on the 4 body states
        if permute and self.permuter_ != None:
            inputs_continuous[~inputs_discrete.bool()], _, permute_log_prob = self.permuter_.forward(inputs_continuous[~inputs_discrete.bool()], None)
            total_log_prob[~inputs_discrete.bool()] += permute_log_prob

        # Encoder
        inputs_continuous = self.encoder_.encode_mixed_continuous(inputs_continuous)

        # Pass through the flow transform
        inputs_continuous, transform_log_prob = self.composite_flow_transform_(inputs_continuous, context=self.embedding_(inputs_discrete))
        total_log_prob += transform_log_prob

        return inputs_continuous, inputs_discrete, total_log_prob

    def _inverse(self, inputs_continuous, inputs_discrete, permute=True):
        total_log_prob = torch.zeros(inputs_continuous.shape[0], device=inputs_continuous.device)

        # Dropout on rows that have inputs_discrete = 1
        inputs_continuous[torch.nonzero(inputs_discrete), self.dropout_indices_] = -1

        # Flow transforms
        inputs_continuous, transform_log_prob = self.composite_flow_transform_.inverse(inputs_continuous, context=self.embedding_(inputs_discrete))
        total_log_prob += transform_log_prob
        
        # Decode 
        inputs_continuous = self.encoder_.decode_mixed_continuous(inputs_continuous)

        if permute and self.permuter_ != None:
            inputs_continuous[~inputs_discrete.bool()], _, permute_log_prob = self.permuter_.inverse(inputs_continuous[~inputs_discrete.bool()], None)
            total_log_prob[~inputs_discrete.bool()] += permute_log_prob

        return inputs_continuous, total_log_prob

    # ----- Sampling and likelihood evaluation -----
    def log_prob_conditional(self, inputs_continuous, permute=True):
        inputs_continuous, inputs_discrete, total_log_prob = self._forward(inputs_continuous, permute=permute)

        total_log_prob += self.base_dist_.log_prob_conditional(inputs_continuous, inputs_discrete)

        return total_log_prob

    def log_prob(self, inputs_continuous, permute=True):
        inputs_continuous, inputs_discrete, total_log_prob = self._forward(inputs_continuous, permute=permute)

        total_log_prob += self.base_dist_.log_prob_joint(inputs_continuous, inputs_discrete)

        return total_log_prob

    def sample(self, num_samples, batch_size=None, permute=True):
        if batch_size is None:
            return self._sample(num_samples, permute=permute)
        
        else:
            num_batches = num_samples // batch_size
            num_leftover = num_samples % batch_size
            samples_continuous = []

            for _ in range(num_batches):
                sample_continuous = self._sample(batch_size, permute=permute)
                samples_continuous.append(sample_continuous)
                
            if num_leftover > 0:
                sample_continuous = self._sample(num_leftover, permute=permute)
                samples_continuous.append(sample_continuous)

        return torch.cat(samples_continuous, dim=0)
    
    def sample_conditional(self, inputs_discrete, batch_size=None, permute=True):
        if batch_size is None:
            return self._sample_conditional(inputs_discrete, permute=permute)

        else:
            num_batches = inputs_discrete.shape[0] // batch_size
            num_leftover = inputs_discrete.shape[0] % batch_size
            samples_continuous = []

            for i in range(num_batches):
                sample_continuous = self._sample_conditional(inputs_discrete[i*batch_size : (i+1)*batch_size], permute=permute)
                samples_continuous.append(sample_continuous)
            
            if num_leftover > 0:
                sample_continuous = self._sample_conditional(inputs_discrete[num_batches*batch_size : ])
                samples_continuous.append(sample_continuous)
        
        return torch.cat(samples_continuous, dim=0)

    def _sample(self, num_samples, permute=True):
        samples_continuous, samples_discrete = self.base_dist_.sample(num_samples)

        samples_continuous, _ = self._inverse(samples_continuous, samples_discrete, permute=permute)
        return samples_continuous

    def _sample_conditional(self, inputs_discrete, permute=True):
        samples_continuous = self.base_dist_.sample_conditional(inputs_discrete)

        samples_continuous, _ = self._inverse(samples_continuous, inputs_discrete, permute=permute)
        return samples_continuous
