import torch
import matplotlib as mpl
from matplotlib import pyplot as plt 
from matplotlib.lines import Line2D
import yaml
from ppflows.gluino.gluino_models import GluinoModel

import pandas as pd 
import numpy as np

from ppflows.gluino.gluino_auxiliary import massless_to_massive

def plot_hist(data, data_baseline, ax, ax_ratio, label, colour, linewidth, x_min, x_max, n_bins):
    bins = np.linspace(x_min, x_max, n_bins)
    bin_width = (x_max - x_min)/n_bins

    # Compute errors
    counts, edges = np.histogram(data, bins=bins)
    errors = np.sqrt(counts)

    # Normalize to cross section
    counts = counts/data.shape[0]/bin_width
    errors = errors/data.shape[0]/bin_width

    centers = (edges[1:] + edges[:-1])/2

    fill_x = np.array([e for edge in bins for e in [edge, edge]][1:-1])
    fill_delta_y = np.array([y for error in errors for y in [error, error]])
    fill_central_y = np.array([c for count in counts for c in [count, count]])

    # Regular plot
    ax.hist(centers, bins=bins, histtype='step', label=label, color=colour, linewidth=linewidth, weights=counts)
    ax.fill_between(fill_x, fill_central_y - fill_delta_y, fill_central_y + fill_delta_y, facecolor=colour, alpha=0.3)
    
    # Ratio plot
    counts_baseline, _ = np.histogram(data_baseline, bins=bins)
    counts_baseline = counts_baseline/data_baseline.shape[0]/bin_width
    ax_ratio.hist(centers, bins=bins, histtype='step', label=label, color=colour, linewidth=linewidth, weights=counts/counts_baseline)

    # Errors in ratio
    ratio_counts = counts/counts_baseline
    ratio_counts[np.isinf(ratio_counts)] = 1
    ratio_errors = errors/counts_baseline
    ratio_errors[np.isinf(ratio_errors)] = 1
    
    fill_central_y = np.array([c for count in ratio_counts for c in [count, count]])
    fill_delta_y = np.array([y for error in ratio_errors for y in [error, error]])
    ax_ratio.fill_between(fill_x, fill_central_y - fill_delta_y, fill_central_y + fill_delta_y, facecolor=colour, alpha=0.3)

textwidth = 9
textheight = textwidth/2.4
fontsize = 10.95

colors = ["#e41a1c", "#377eb8", "#4daf4a", "#984ea3", "#ff7f00", "#ffff33", "#a65628", "#f781bf"]
labels = ["Flow (gluino 1)", "Flow (gluino 2)", "Flow (gluino 3)", "Flow (gluino 4)"]

mpl.rcParams['text.usetex'] = True
mpl.rcParams['font.family'] = 'serif'

mpl.rcParams['xtick.direction'] = 'in'
mpl.rcParams['xtick.major.size'] = 4.0
mpl.rcParams['xtick.minor.size'] = 2.0
mpl.rcParams['xtick.major.width'] = 0.5
mpl.rcParams['xtick.minor.width'] = 0.5

mpl.rcParams['ytick.direction'] = 'in'
mpl.rcParams['ytick.major.size'] = 4.0
mpl.rcParams['ytick.minor.size'] = 2.0
mpl.rcParams['ytick.major.width'] = 0.5
mpl.rcParams['ytick.minor.width'] = 0.5

mpl.rcParams['lines.linewidth'] = 1.2
mpl.rcParams['lines.markersize'] = 2
mpl.rcParams["legend.labelspacing"] = 0.1
mpl.rcParams["legend.frameon"] = False
mpl.rcParams["legend.handletextpad"] = 0.5
mpl.rcParams["xtick.minor.visible"] = True
mpl.rcParams["ytick.minor.visible"] = True
#mpl.rcParams["legend.columnspacing"] = 0
mpl.rcParams.update({'font.size': fontsize})

n_bins = 50
x_min = 601
x_max = 999

n_train = 100000
n_samples = 10000000

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Get the baseline
data = pd.read_csv("../data_4_gluino.csv", header=None).to_numpy()[:n_train,:]
mass = np.sqrt(data[0,0]**2 - data[0,1]**2 - data[0,2]**2 - data[0,3]**2)

with open("hyperparams.yaml") as file:
    hyperparams = yaml.safe_load(file)

# Generate from model_without_perm
model_without_perm = GluinoModel(hyperparams)
model_without_perm.load_state_dict(torch.load("results/model_without_perm_100k.pt", map_location=torch.device(device)))
model_without_perm.eval()
model_without_perm.to(device)
with torch.no_grad():
    samples_without_perm, _ = model_without_perm.sample(n_samples, batch_size=2048)
    samples_without_perm = massless_to_massive(samples_without_perm, mass).cpu().numpy()

# Generate from model_stochastic
hyperparams["permutation"] = "stochastic"
model_stochastic = GluinoModel(hyperparams)
model_stochastic.load_state_dict(torch.load("results/model_stochastic_100k.pt", map_location=torch.device(device)))
model_stochastic.eval()
model_stochastic.to(device)
with torch.no_grad():
    samples_stochastic, _ = model_stochastic.sample(n_samples, batch_size=2048)
    samples_stochastic = massless_to_massive(samples_stochastic, mass).cpu().numpy()

# Generate from model_sort
hyperparams["permutation"] = "ordered"
model_ordered = GluinoModel(hyperparams)
model_ordered.load_state_dict(torch.load("results/model_ordered_100k.pt", map_location=torch.device(device)))
model_ordered.eval()
model_ordered.to(device)
with torch.no_grad():
    samples_ordered, _ = model_ordered.sample(n_samples, batch_size=2048)
    samples_ordered = massless_to_massive(samples_ordered, mass).cpu().numpy()

fig, axs = plt.subplots(2, 3, sharex='col', sharey='row', gridspec_kw={'hspace': 0, 'wspace': 0, 'height_ratios': [5,2]})
fig.set_size_inches(textwidth, textheight)

# Plot baseline
plot_hist(data[:,0], data[:,0], axs[0,0], axs[1,0], label='Train', colour='black', linewidth=1.5, x_min=x_min, x_max=x_max, n_bins=n_bins)
plot_hist(data[:,0], data[:,0], axs[0,1], axs[1,1], label='Train', colour='black', linewidth=1.5, x_min=x_min, x_max=x_max, n_bins=n_bins)
plot_hist(data[:,0], data[:,0], axs[0,2], axs[1,2], label='Train', colour='black', linewidth=1.5, x_min=x_min, x_max=x_max, n_bins=n_bins)

for i in range(4):
    plot_hist(samples_without_perm[:,i,0],  data[:,0], axs[0,0], axs[1,0], label=labels[i], colour=colors[i], linewidth=1, x_min=x_min, x_max=x_max, n_bins=n_bins)
    plot_hist(samples_stochastic[:,i,0],    data[:,0], axs[0,1], axs[1,1], label=labels[i], colour=colors[i], linewidth=1, x_min=x_min, x_max=x_max, n_bins=n_bins)
    plot_hist(samples_ordered[:,i,0],       data[:,0], axs[0,2], axs[1,2], label=labels[i], colour=colors[i], linewidth=1, x_min=x_min, x_max=x_max, n_bins=n_bins)

axs[0,0].set_xlim(xmin=x_min+1, xmax=x_max-1)
axs[0,1].set_xlim(xmin=x_min+1, xmax=x_max-1)
axs[0,2].set_xlim(xmin=x_min+1, xmax=x_max-1)
axs[0,0].set_ylim(ymin=1e-4, ymax=0.0049)
axs[0,1].set_ylim(ymin=1e-4, ymax=0.0049)
axs[0,2].set_ylim(ymin=1e-4, ymax=0.0049)
axs[1,0].set_ylim(ymin=0.8, ymax=1.2)
axs[1,1].set_ylim(ymin=0.8, ymax=1.2)
axs[1,2].set_ylim(ymin=0.8, ymax=1.2)

axs[1,0].set_xlabel(r'$E_{\tilde{g}} $ [GeV]')
axs[1,1].set_xlabel(r'$E_{\tilde{g}} $ [GeV]')
axs[1,2].set_xlabel(r'$E_{\tilde{g}} $ [GeV]')
axs[0,0].set_ylabel(r'$\frac{1}{\sigma} \frac{d\sigma}{dE_{\tilde{g}}} \mathrm{[GeV]}^{-1}$')
axs[1,0].set_ylabel(r'Flow/MC')

axs[0,0].set_title("No permutation")
axs[0,1].set_title("Stochastic permutation")
axs[0,2].set_title("Sort permutation")

plt.axes(axs[0,0])
handles, labels = axs[0,0].get_legend_handles_labels()
new_handles = [Line2D([], [], c=h.get_edgecolor()) for h in handles]
plt.legend(handles=new_handles, labels=labels, loc='upper left', bbox_to_anchor=(0.01,0.5))

plt.savefig("plots_energies.pdf", bbox_inches='tight')