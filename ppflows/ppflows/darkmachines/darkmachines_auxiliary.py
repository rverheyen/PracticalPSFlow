# Parts of the code below is takes from the DarkMachines collaboration

import pandas as pd
import os
import torch
from torch import nn
import math
from ppflows.file_lock import FileLock

# Convert data to torch tensors
# Also convert energy to mass, adding a 0 if there is no jet
# Output format is 
# [MET, METphi, pT1, eta1, phi1, E1, pT2, m2, eta2, phi2, E2, .....]
# [obj1, obj2, ....]
# Energies are not stored for objects that are not jets, as they contain no additional information 
# All energy-scaling observables are log-transformed
# Categorical encoding for objects is 
# 0 - empty
# 1 - jet (j)
# 2 - b-jet (b)
# 3 - photon (g)
# 4 - positron (e+)
# 5 - electro (e-)
# 6 - antimuon (m+)
# 7 - muon (m-)

class DarkMachinesData(nn.Module):
    def __init__(self, hyperparams):
        super(DarkMachinesData, self).__init__()

        self.categorical_map_ = {
            "j": 1,
            "b": 2,
            "g": 3,
            "e+": 4,
            "e-": 5,
            "m+": 6,
            "mu+": 6,
            "m-": 7,
            "mu-": 7,
        }

        self.num_objects_           = hyperparams["num_objects"]
        self.file_dir_train_        = "training_files/chan" + hyperparams["channel"]
        self.file_dir_secret_       = "secret_data/chan"    + hyperparams["channel"]
        self.validation_fraction_   = hyperparams["validation_fraction"]
        self.batch_size_            = hyperparams["batch_size"]

        self.register_buffer("means_", torch.zeros(2 + 4*self.num_objects_))
        self.register_buffer("stds_",  torch.zeros(2 + 4*self.num_objects_))

    def convert_to_tensor(self, file_name):
        with FileLock(file_name):
            num_lines = sum(1 for line in open(file_name))

            continuous_data = torch.zeros(num_lines, 2 + 4*self.num_objects_)*float('nan')
            discrete_data = torch.zeros(num_lines, self.num_objects_).long()

            with open(file_name, 'r') as file:
                for i, line in enumerate(file.readlines()):
                    line = line.replace(';', ',')
                    line = line.rstrip(',\n')
                    line = line.split(',')

                    continuous_data[i][0] = math.log(float(line[3]))
                    continuous_data[i][1] = float(line[4])

                    for j in range(0, min(int((len(line) - 5)/5), self.num_objects_)):
                        continuous_data[i][2+4*j] = math.log(float(line[5*j + 7]))
                        continuous_data[i][3+4*j] = float(line[5*j + 8])
                        continuous_data[i][4+4*j] = float(line[5*j + 9])

                        discrete_data[i][j] = self.categorical_map_[line[5*j+5]]
                        if discrete_data[i][j] == 1 or discrete_data[i][j] == 2:
                            continuous_data[i][5+4*j] = math.log(float(line[5*j+6]))

        return continuous_data, discrete_data

    def convert_channel_to_dataloaders(self, train=False, validation=False, test=False, secret=False):
        if validation:
            assert train

        training_loader = None
        validation_loader = None
        test_loaders = None
        secret_loader = None

        if train:
            for file_name in os.scandir(self.file_dir_train_):
                if "background" in file_name.name:
                    training_data_continuous, training_data_discrete = self.convert_to_tensor(file_name)
                    break

            # Compute the normalization
            # Have to do this sequentially because of the presence of nans
            for i in range(training_data_continuous.shape[1]):
                col_without_nans = training_data_continuous[:,i]
                col_without_nans = col_without_nans[~col_without_nans.isnan()]
                self.means_[i] = col_without_nans.mean()
                self.stds_[i]  = col_without_nans.std()

            # Normalize
            training_data_continuous = (training_data_continuous - self.means_)/self.stds_

            if validation:
                n_train = int(self.validation_fraction_*training_data_continuous.shape[0])

                # Split the data
                training_data_combined = torch.utils.data.TensorDataset(training_data_continuous[:n_train], training_data_discrete[:n_train])
                validation_data_combined = torch.utils.data.TensorDataset(training_data_continuous[n_train:], training_data_discrete[n_train:])

                # Construct dataloaders
                training_loader = torch.utils.data.DataLoader(dataset=training_data_combined, batch_size=self.batch_size_, shuffle=True)
                validation_loader = torch.utils.data.DataLoader(dataset=validation_data_combined, batch_size=self.batch_size_)
            else:
                # Only construct train loader
                training_data_combined = torch.utils.data.TensorDataset(training_data_continuous, training_data_discrete)
                training_loader = torch.utils.data.DataLoader(dataset=training_data_combined, batch_size=self.batch_size_, shuffle=True)


        # Test data
        if test:
            test_loaders = []
            for file_name in os.scandir(self.file_dir_train_):
                if "background" not in file_name.name:
                    print(file_name.name)
                    data_continuous, data_discrete = self.convert_to_tensor(file_name)
                    # Normalize
                    data_continuous = (data_continuous - self.means_)/self.stds_
                    # Store
                    test_data_combined = torch.utils.data.TensorDataset(data_continuous, data_discrete)
                    test_loaders.append([torch.utils.data.DataLoader(dataset=test_data_combined, batch_size=self.batch_size_), file_name.name])

        # Secret data
        if secret:
            for file_name in os.scandir(self.file_dir_secret_):
                secret_data_continuous, secret_data_discrete = self.convert_to_tensor(file_name)
                # Normalize
                secret_data_continuous = (secret_data_continuous - self.means_)/self.stds_

            secret_data_combined = torch.utils.data.TensorDataset(secret_data_continuous, secret_data_discrete)
            secret_loader = torch.utils.data.DataLoader(dataset=secret_data_combined, batch_size=self.batch_size_)

        return training_loader, validation_loader, test_loaders, secret_loader

    # Undo normalization and log scaling 
    def unnormalize(self, inputs_continuous):
        outputs_continuous = inputs_continuous*self.stds_ + self.means_
        outputs_continuous[:,0] = torch.exp(outputs_continuous[:,0])
        for i in range(int((inputs_continuous.shape[0]-2)/4)):
            outputs_continuous[:, 2+4*i] = torch.exp(outputs_continuous[:, 2+4*i])
            outputs_continuous[:, 5+4*i] = torch.exp(outputs_continuous[:, 5+4*i])

        return outputs_continuous
        
