import h5py
import numpy as np

hf = h5py.File("4top_flow.h5", 'r')

data_process = np.array(hf.get('type'))
data_continuous = np.array(hf.get('reg'), dtype=np.float32)
data_discrete = np.array(hf.get('clas'), dtype=np.compat.long)

np.savetxt("4top_continuous.csv", data_continuous, delimiter=",", fmt='%.8e')
np.savetxt("4top_discrete.csv", data_discrete, delimiter=",", fmt='%i')
np.savetxt("4top_process.csv", data_process, delimiter=",", fmt='%i')