\documentclass[a4paper,11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}
\usepackage{fullpage}
\usepackage{hyperref}
\usepackage{cite}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{multirow}% http://ctan.org/pkg/multirow
\usepackage{slashed}
\usepackage[b]{esvect}
\usepackage{xspace}
\usepackage{epstopdf}
\usepackage{xcolor}
\usepackage{booktabs,tabularx}
\usepackage{tikz}
\usepackage{amssymb}

\usetikzlibrary{positioning}
\usetikzlibrary{shapes.geometric}
\tikzstyle{node} = [circle,fill=white, draw=black, minimum size=0.7cm]
\tikzstyle{blob} = [circle, minimum size=0.9cm,inner sep=0pt, thin, draw=black]
\tikzset{arrow/.style={-stealth, semithick, draw=black}}
\tikzset{dashedarrow/.style={-stealth, semithick, draw=black}}
\usetikzlibrary{decorations.pathreplacing}

\hypersetup{
  colorlinks=true,
  linkcolor=red,          % color of internal links (change box color with linkbordercolor)
  citecolor=blue
}

\newcommand{\gencomment}[3]{\textbf{\textcolor{#3}{[#1]$_\text{#2}$}}}
\newcommand{\commentps}[1]{\gencomment{#1}{PS}{red}}
\newcommand{\commentrv}[1]{\gencomment{#1}{RV}{purple}}
\newcommand{\rv}[1]{\commentrv{#1}}
\newcommand{\ps}[1]{\commentps{#1}}


\newcommand{\cf}{{\sl cf.}\xspace}
\newcommand{\ie}{{\sl i.e.}\xspace}
\newcommand{\upd}{\ensuremath{\mathrm{d}}\xspace}
\newcommand{\prob}{\ensuremath{\cal P}\xspace}
\newcommand{\pT}[1][]{\ensuremath{p_{\perp#1}}\xspace}
\newcommand{\qEvol}[1][]{\ensuremath{Q}\xspace}

\begin{document}
\renewcommand*{\thefootnote}{\fnsymbol{footnote}}
\begin{minipage}{\textwidth}
\flushright\footnotesize 
MCNET-21-13
\end{minipage}\\[1mm]
\begin{center}\scalebox{0.99}{\Large\textbf{Event Generation and Density Estimation with }}\end{center}
\begin{center}\scalebox{0.99}{\Large\textbf{Surjective Normalizing Flows}}\end{center}
\begin{center}{\large Rob Verheyen$^1$\footnote{{\texttt{r.verheyen@ucl.ac.uk}}}}
\end{center}
$^1$University College London, WC1E 6BT London, United Kingdom\\[1.5ex]
\renewcommand*{\thefootnote}{\arabic{footnote}}
\addtocounter{footnote}{-1}
\begin{abstract}
This is an abstract.
\end{abstract}
\vspace*{2mm}

\section{Introduction}

\section{Surjective Normalizing Flows} \label{sec:latent-variable-models}

We are interested in setting up a generative model that allows for both the generation of new events, as well as the evaluation of the
likelihood of existing events. 
One class of models that enables both are known as \emph{latent variable models}.
They can be built to be sufficiently expressive to learn the complicated probability distributions that are commonly encountered in particle physics.
Consider a set of physical events $x \in \mathcal{X}$, we define a auxiliary set of latent variables $z \in \mathcal{Z}$
with an associated joint probability distribution $p_{\theta}(x,z)$, specified by parameters $\theta$. 
The marginal probability density 
\begin{equation} \label{eq:latent-variable-model}
    p_{\theta}(x) = \int_{\mathcal{Z}} dz \, p_{\theta}(x,z) = \int_{\mathcal{Z}} dz \, p_{\theta}(z) \, p_{\theta}(x|z)
\end{equation}
is then the distribution of interest. 
The second equality in eq.~\eqref{eq:latent-variable-model}, through the general product rule, hints at the corresponding generative process, which is given by 
\begin{align} \label{eq:latent-sampling}
    z &\sim p(z) \nonumber \\
    x &\sim p(x|z),
\end{align}
where we have dropped the implicit dependence on the parameters $\theta$.
Eq.~\eqref{eq:latent-sampling} gives an indication of the fact that latent variable models can form highly expressive parametric models through the
conditioning of the density over the real space $\mathcal{X}$ on the random variable $z$. 
This however comes at a cost, which is the fact that the integral in eq.~\eqref{eq:latent-variable-model} is generally not tractible.
Latent variable models may be trained by, for instance, maximizing the marginal likelihood $p(x)$ over a dataset $\{x_i\}_{i=1}^N \in \mathcal{X}$. 
Furthermore, density estimation may be an object for these models in its own right.
As such, the intractability of eq~\eqref{eq:latent-sampling} is a significant issue, for which several solutions exist. 

\subsection{Normalizing Flows}
One option to resolve the intractability of eq.~\eqref{eq:latent-variable-model} is to remove the stochastic component from the conditional probability distribution, $p(x|z) = \delta(x - f(z))$, leading to (in log-space)
\begin{align} \label{eq:flow-step-llh}
    \log p(x) &= \log\bigg[ \int_{\mathcal{Z}} dz \, p(z) \, \delta(x - f(z)) \bigg] = \log p(z) + \log |J(x)|,
\end{align}
where $|J(x)|$ is the Jacobian determinant associated with the transform $f(z)$. 
The evaluation of $p(z) = p(f^{-1}(x))$ requires $f(z)$ to be a bijective function, enforcing the dimensionality of $\mathcal{X}$ and $\mathcal{Z}$ to be equal.
Eq.~\eqref{eq:flow-step-llh} is the fundamental step in \emph{normalizing flow} architectures \cite{cms/1266935020,Tabak2013AFO,pmlr-v37-rezende15}.
Normalizing flow transforms are composable, meaning that multiple may be combined, $z_0 \to z_1 \to ... z_n \to x$, to further improve expressivity, leading to 
\begin{equation} \label{eq:flow-llh}
    \log p(x) = \log p(z_0) + \sum_{i=1} \log |J_i(z_i)|.
\end{equation}
Much of the research on normalizing flows has focussed on improving the efficiency of the bijective transform, see e.g. \cite{KobyzevPAMI2020,papamakarios2021normalizing} for reviews.
Here, we opt to make use of an \emph{autoregressive flow} \cite{papamakarios2017masked} similar to the one used in \cite{Stienen:2020gns,Caron:2021wmq}.
In this model, the bijection $f_i$ on a $d$-dimensional event space $\mathcal{X}$ is factorized into a set of $d$ one-dimensional transforms that may be characterized by 
\begin{equation} \label{eq:autoregressive-transform}
    z_{i+1}^j = f^j_{i}(z_i^j; \theta_i^j(z_{i+1}^{1:j-1})),
\end{equation}
where for $j \in [1,d]$, $z^j$ is the $j$th component of $z$. 
The bijection $f_i^j$ is parameterized by a function $\theta_i^j$ of the preceeding components $z_{i+1}^0$ through $z_{i+1}^{j-1}$.
As such, the transform from $z_i$ to $z_{i+1}$ (we will refer to this as the \emph{forward} direction) must be performed sequentially starting from $z_{i+1}^0$.
On the contrary, the transform from $z_{i+1}$ to $z_i$ (the \emph{inverse} direction) can be performed in parallel.
In our implementation, the functional form of $f_i^j$ is a rational quadratic spline \cite{durkan2019neural} and $\theta_i^j$ is a MADE network \cite{khajenezhad2020masked}.
This setup results in a highly expressive model that is well-suited for density estimation in partcle physics \cite{Stienen:2020gns}.


\subsection{Surjective and Stochastic Transforms}
Instead of solving the intractability of eq.~\eqref{eq:latent-variable-model} by constraining $p(x|z)$ to a delta function, one may opt to resort to \emph{variational inference}.
\footnote{The objective of variational inference is often stated as the computation of the posterior $p(z|x) = p(z,x)/p(x)$, for which the marginalized distribution $p(x)$ (often referred to as the evidence) is required.}.
In that case, one introduces a variational approximation $q(z|x)$ to the true posterior $p(z|x)$.
The log-likelihood may then be rewritten as 
\begin{align} \label{eq:elbo-1}
    \log p(x) &= \int_{\mathcal{Z}} dz \, q(z|x) \, \log \frac{p(x|z) p(z)}{p(z|x)} \nonumber \\
    &= \int_{\mathcal{Z}} dz \, q(z|x) \, \bigg[ \log p(x|z) - \log \frac{q(z|x)}{p(z)} + \log \frac{q(z|x)}{p(z|x)} \bigg]\nonumber \\
    &= \mathbb{E}_{q(z|x)} \big[\log p(x|z)\big] - \mathbb{D}_{\text{KL}}\big[q(z|x), p(z)\big] + \mathbb{D}_{\text{KL}}\big[q(z|x), p(z|x)\big],
\end{align}
where $\mathbb{E}$ indicates an expectation value, and $\mathbb{D}_{\text{KL}}$ is the Kullback-Leibler divergence. 
The intractability of $\log p(x)$ is now isolated in the third term of eq.~\eqref{eq:elbo-1}, which is strictly positive. 
The combination of the other two terms is commonly referred to as the estimate lower bound (ELBO), which may be optimized in place of the full likelihood. 
Eq.~\eqref{eq:elbo-1} serves as the foundation of the variational autoencoder (VAE) \cite{kingma2013auto}, in which $p(x|z)$ and $q(z|x)$ are parameterized by deep neural networks and the ELBO is evaluated with a single Monte Carlo sample $z \sim q(z|x)$.
Contrary to the normalizing flow approach, variational autoencoders do not require any restrictions on the form of $p(x|z)$.
However, the gap between the likelihood and the ELBO vanishes only in the limit where $q(z|x) = p(z|x)$, which in difficult to accomplish in practice.

In \cite{nielsen2020survae} it was pointed out that the normalizing flow and VAE paradigms may the unified by rewriting eq.~\eqref{eq:elbo-1} as 
\begin{align} \label{eq:elbo-2}
    \log p(x) &= \mathbb{E}_{q(z|x)} \bigg[ \log p(z) + \underbrace{\log \frac{p(x|z)}{q(z|x)}}_{\mathcal{V}(x,z)} + \underbrace{\log \frac{q(z|x)}{p(z|x)}}_{\mathcal{E}(x,z)} \bigg],
\end{align}
where $\mathcal{V}(x,z)$ is the likelihood contribution and $\mathcal{E}(x,z)$ are the bound looseness. 
For a normalizing flow transform, no variational approximation to the posterior is required, i.e $p(x|z) = \delta(x - f(z))$ and $q(z|x) = \delta(z - f^{-1}(x))$.
The result is that $\mathcal{V}(x,z) = \log J(x)$ and $\mathcal{E}(x,z) = 0$, recovering eq.~\eqref{eq:flow-step-llh}.
However, for stochastic transforms like that of the VAE, $\mathcal{V}(x,z)$ may be evaluated with a single Monte Carlo sample, while $\mathcal{E}(x,z)$ remains intractable, again serving as a (strictly positive) error on the full likelihood. 
Furthermore, it is possible to define \emph{surjective} transforms, which are deterministic in one direction and stochastic in the other. 
In section \ref{sec:gluino} we will employ several of such transforms, which will turn out to be useful in the modelling of several features commonly encountered in particle collision events.
Note that eq.~\eqref{eq:elbo-2} naturally supports the composable nature of a normalizing flow akin to eq.~\eqref{eq:flow-llh}, such that bijective, surjective and stochastic transforms may be combined.

\section{Generative models for particle physics events} \label{sec:gluino}
In this section we explore the use of surjective transforms as part of a normalizing flow to improve the handling of several features of particle physics events: permutation invariance, varying dimensionalities and discrete features.
Here, we establish a baseline of techniques by considering a relatively low-dimensional matrix element-level process that can be set up to display all of these features. 
We choose the process 
\begin{equation}
    g \, g \rightarrow \tilde{g} \, \tilde{g} \, \tilde{g} \, \tilde{g},
\end{equation}
at $3$ TeV, using the default parameters of the \textsc{MSSM\textunderscore SLHA} model of \textsc{Madgraph5\textunderscore aMC@NLO} \cite{Alwall:2014hca}, which sets the gluino mass to $m_{\tilde{g}} = 607.71$ GeV.
This process has a four-fold permutation symmetry in the final state, allowing us to explore techniques that can force permutation invariance in the generative model.
Furthermore, it has rich discrete structure, with 120 possible colour orderings and 64 helicity configurations. 
No infrared divergences are present due to the gluino masses, which also lead to a more complicated probability density through mass-suppressed helicity configurations. 
For experiments with varying dimensionality, we mix in $g \, g \rightarrow \tilde{g} \, \tilde{g}$ events. 
In total, we generate $2$M $g \, g \rightarrow \tilde{g} \, \tilde{g} \, \tilde{g} \, \tilde{g}$ event, but we use different numbers of them for the experiments below.
The raw four-vector data is preprocessed using the algorithm of \cite{Platzer:2013esa}, leading to a data representation in $[0,1]^8$.

We will make use of a fixed baseline autoregressive flow architecture, implemented in \textsc{PyTorch} \cite{paszke2019pytorch}, which is expanded upon with various additional components. 
The model hyperparameters are listed in table \ref{tab:gluino-hyper}, and further details may be found in \cite{Stienen:2020gns}.
Notably, the base distribution $p(z_0)$ is chosen to be a uniform distribution over $[0,1]^8$, such that the flow transforms are constrained to $[0,1] \to [0,1]$.
Models are trained with the Adam optimizer \cite{kingma2014adam} with default values of $\beta_1$ and $\beta_2$.
Because our experiments feature different amounts of training data, we formulate the training procedure, of which the parameters are also listen in table \ref{tab:gluino-hyper}, in terms of iterations rather than epochs. 
After fixed intervals, the model is validated by a fixed set of $50$k independent events. 
If the loss has not improved for a fixed number of validations (the decay patience), the learning rate is multiplied by a decay factor. 
This procedure repeats until the learning rate drops low enough for training to have effectively ceased (in practice, a factor of $10^{-3}$ of the initial learning rate).

\begin{table}[]
\centering
\begin{tabular}{ll|ll}
\multicolumn{2}{c|}{Model}             & \multicolumn{2}{c}{Training} \\ \hline
Parameter           & Value     & Parameter             & Value       \\ \hline
RQS knots           & 32        & Batch size            & 4096        \\
MADE layers         & 2         & Optimizer             & Adam        \\
MADE units per dim  & 10        & Learning rate         & $10^{-3}$   \\
Flow layers         & 8         & Validation interval   & 25          \\
                    &           & LR decay              & 0.5         \\
                    &           & LR decay patience     & 50          \\
\end{tabular}
\caption{Table of hyperparameters and training setup used in the experiments of section \ref{sec:gluino}.}
\label{tab:gluino-hyper}
\end{table}

\subsection{Permutation invariance}
Particle physics events often display a large degree of permutation invariance. 
In the matrix element-level example used here, the final state has a four-fold permutation symmetry. 
More generally, jet constituents are permutation invariant, a fact that is already exploited in other ML architectures \cite{Komiske:2018cqr,Dolan:2020qkr}.
Similarly, at the hadron-level or detector-level, identified objects are also permutation invariant UGLY.

In \cite{nielsen2020survae}, two methods were proposed to instill permutation invariance into a normalizing flow model: a sorting surjection and a stochastic permutation. 
The forward and backward transforms are defined as 
\begin{alignat}{3}
    p_{\text{sort}}(x|z) &= \sum_{\mathcal{I}_p}  \, \frac{1}{D!} \, \delta(x - z_{\mathcal{I}_{p}^{-1}}), && p_{\text{stoch}}(x|z) && = \sum_{\mathcal{I}_p} \, \frac{1}{D!} \, \delta(x - z_{\mathcal{I}_p^{-1}}), \nonumber \\
    q_{\text{sort}}(z|x) &= \sum_{\mathcal{I}_p} \, \delta_{\mathcal{I}_p, \text{argsort}(x)} \, \delta(z - x_{\mathcal{I}_p}),  \quad \quad   && q_{\text{stoch}}(z|x) &&= \sum_{\mathcal{I}_p} \, \frac{1}{D!} \, \delta(z - x_{\mathcal{I}_p}), \nonumber \\
    \mathcal{V}_{\text{sort}}(x,y) &= -\log(D!), && \mathcal{V}_{\text{stoch}}(x,y) && =  0.
\end{alignat} \label{eq:permute-transforms}

where $\mathcal{I}_p$ is a set of permutation indices for the components of $x$ or $z$, $\mathcal{I}_p^{-1}$ are their inverse and $D$ is the number of permutable classes.
That is, in the inverse direction, the sort surjection orders $x$ following some predicate, while the stochastic permutation randomly shuffles $x$. 
In the forward direction, both transforms randomly shuffle $x$, leading to permutation-invariant samples.
The sort transform is surjective in the inverse direction and has $\mathcal{E}_{\text{sort}}(x,y) = 0$, while the stochastic permutation does not lead to a vanishing bound looseness.
Both transforms lead to improved modelling in different ways. 
The stochastic permutation may be viewed as a way to multiply the training statistics by a factor $D!$, while the sort surjection folds the space $\mathcal{X}$ into a volume of $1/D!$ the size. 

In our example process, permutation invariance is only manifest at the four-vector level.
The transform between the space of four-vectors and the unit hypercube $[0,1]^8$ is thus also implemented as a transform layer, as is illustrated in figure \ref{fig:perm}. 

\begin{figure}
    \centering
    \begin{tikzpicture}
        % Sort figure

        % Nodes
        \node at (-1.7,0.0)(pz0) {$\text{Unif(0,1)}^8 \sim $};
        \node[blob] at (0.0,0.0)(z0) {$z_0$};
        \node[blob] at (2.0,0.0)(z1) {$z_1$};
        \node[blob] at (4.0,0.0)(z2) {$z_2$};
        \node[blob] at (6.0,0.0)(zn) {$z_n$};
        \node[blob] at (8.0,0.0)(x) {$x$};
        \node[blob] at (10.0,0.0)(xI) {$x_{\mathcal{I}_p}$};
            
        % Arrows between zs
        \draw[arrow] (z0) to [out=45,in=135] (z1);
        \draw[arrow] (z1) to [out=225,in=315] (z0);
        \draw[arrow] (z1) to [out=45,in=135] (z2);
        \draw[arrow] (z2) to [out=225,in=315] (z1);
        \draw[arrow] (z2) to [out=45,in=135] (zn);
        \draw[arrow] (zn) to [out=225,in=315] (z2);
        \draw[arrow] (zn) to [out=45,in=135] (x);
        \draw[arrow] (x) to [out=225,in=315] (zn);
        \draw[dashed,arrow] (x) to [out=45,in=135] (xI);
        \draw[arrow] (xI) to [out=225,in=315] (x);
                
        % Function transforms
        \node at (1.0, 0.0) {$f_1$};
        \node at (3.0, 0.0) {$f_2$};
        \node at (5.,0) {...};
        \node at (7.0, 0.17) {\footnotesize phase};
        \node at (7.0, -0.17) {\footnotesize space};
        \node at (9.0, 0.0) {sort};       
        
        % Stoch figure

        % Nodes
        \node at (-1.7,-2.0)(pz0) {$\text{Unif(0,1)}^8 \sim $};
        \node[blob] at (0.0,-2.0)(z0) {$z_0$};
        \node[blob] at (2.0,-2.0)(z1) {$z_1$};
        \node[blob] at (4.0,-2.0)(z2) {$z_2$};
        \node[blob] at (6.0,-2.0)(zn) {$z_n$};
        \node[blob] at (8.0,-2.0)(x) {$x$};
        \node[blob] at (10.0,-2.0)(xI) {$x_{\mathcal{I}_p}$};
            
        % Arrows between zs
        \draw[arrow] (z0) to [out=45,in=135] (z1);
        \draw[arrow] (z1) to [out=225,in=315] (z0);
        \draw[arrow] (z1) to [out=45,in=135] (z2);
        \draw[arrow] (z2) to [out=225,in=315] (z1);
        \draw[arrow] (z2) to [out=45,in=135] (zn);
        \draw[arrow] (zn) to [out=225,in=315] (z2);
        \draw[arrow] (zn) to [out=45,in=135] (x);
        \draw[arrow] (x) to [out=225,in=315] (zn);
        \draw[dashed,arrow] (x) to [out=45,in=135] (xI);
        \draw[dashed,arrow] (xI) to [out=225,in=315] (x);
                
        % Function transforms
        \node at (1.0, -2.0) {$f_1$};
        \node at (3.0, -2.0) {$f_2$};
        \node at (5.,-2.0) {...};
        \node at (7.0, -1.83) {\footnotesize phase};
        \node at (7.0, -2.17) {\footnotesize space};
        \node at (9.0, -2.0) {stoch};
    \end{tikzpicture}
    \caption{
    Visualisation of the normalizing flow architecture, including a translation between phase space and unit hypercube and a permutation transform.
    The forward direction starts from a uniform base distribution over the hypercube, after which $n$ flow bijections $f_i$ are applied. 
    Next, the hypercube variables $z_n$ are transformed to four-vectors $x$, after which a permutation transform is applied.
    Solid arrows indicate deterministic transforms, while dashed arrows are stochastic.
    }
    \label{fig:perm}
\end{figure}

\subsection{Varying dimensionality}
Particle physics events typically do not contain a constant number of objects.
As a result, the dimensionality of phase space can vary on an event-by-event basis.
Normalizing flow models on the other hand learn probability distributions of fixed dimension.
One method of modelling varying dimensionalities was presented in \cite{Butter:2021csz} for the specific case of $pp \to Z_{\mu \mu} + \{1,2,3\} \text{ jets}$, 
where conditional flow networks are trained to add jets to baseline $Z_{\mu \mu}$ events.
Alternatively, one could train multiple generative models for all individual configurations.
The downside of this approach is that the training statistics are split between the models.
On the other hand, a single model that is able to generate all configurations will be able to learn any underlying patterns that are common between them.
The architecture of \cite{Butter:2021csz} accomplishes this, but it does not generalize easily to many different configurations.

Here, we introduce a surjective transform that is able to combine an arbitrary number of configurations into a single model. 
We refer to it as a \emph{dropout} transform, as its function is to stochastically drop a subset of the components of the latent variables.
To that end, we introduce a set of dropout indices $\mathcal{I}_{\downarrow}$ for the components of $x$ and $z$, 
as well as their complement $\mathcal{I}_{\uparrow}$ such that $\{\mathcal{I}_{\downarrow}, \mathcal{I}_{\uparrow}\} = \{1,...,d\}$.
Furthermore, to indicate components that have been dropped, we introduce a constant $\varnothing$ which is outside of the domain of the latent variables, 
and is thus not altered by subsequent flow layers.
The forward and backward transforms are 
\begin{align}
    p_{\text{drop}}(x|z) &= \sum_{\mathcal{I}_{\downarrow}} p_{\mathcal{I}_{\downarrow}} \, \delta(x_{\mathcal{I}_{\uparrow}} - z_{\mathcal{I}_{\uparrow}}) \, \delta(x_{\mathcal{I}_{\downarrow}} - \varnothing), \nonumber \\
    q_{\text{drop}}(z|x) &= \sum_{\mathcal{I}_{\downarrow}} \delta_{\mathcal{I}_{\downarrow}, \text{argdrop}(x)} \, \delta(z_{\mathcal{I}_{\uparrow}} - x_{\mathcal{I}_{\uparrow}} ) \, q(z_{\mathcal{I}_{\downarrow}}).
\end{align}
That is, the forward transform picks a set of dropout indices $\mathcal{I}_{\downarrow}$ with probability $p_{\mathcal{I}_{\downarrow}}$ and drops the corresponding components. 
The inverse fills the dropped components with probability $q(z_{\mathcal{I}_{\downarrow}})$. 
Instead of computing the likelihood contribution and the bound looseness, we can write the likelihood as 
\begin{align} \label{eq:dropout-likelihood}
    p(x) &= \int_{\mathcal{Z}} dz \, p_{\text{drop}}(x|z) \, p(z) \nonumber \\
    &= \int_{\mathcal{Z}} dz_{\mathcal{I}_{\downarrow}} \, \delta(x_{\mathcal{I}_{\downarrow}} - \varnothing) \, p(x_{\mathcal{I}_{\uparrow}}, z_{\mathcal{I}_{\downarrow}}).
\end{align} 
In general, the integral in eq.~\eqref{eq:dropout-likelihood} is intractable. 
However, under the assumption that $x_{\mathcal{I}_{\uparrow}}$ and $z_{I_{\downarrow}}$ are independent, i.e. 
$p(x_{\mathcal{I}_{\uparrow}}, z_{I_{\downarrow}})$ = $p(x_{\mathcal{I}_{\uparrow}}) \, p(z_{I_{\downarrow}})$, 
eq.~\eqref{eq:dropout-likelihood} reduces to $p(x) = \delta(x_{\mathcal{I}_{\downarrow}} - \varnothing) \, p(x_{\mathcal{I}_{\uparrow}})$, which is easily evaluated.
To enforce this simplification for all $\mathcal{I}_{\downarrow}$, all components of the latent space must be independent, which is generally not the case after one or more flow transforms.
However, since the base distribution is usually chosen as either a uniform distribution or a multivariate Gaussian with diagonal covariance,
placing the dropout surjection directly after the base distribution leads to a model with a tractable likelihood. 
Note that tractability is a direct consequence of the choice of letting $p_{\mathcal{I}_{\downarrow}}$ to be independent of $z$.
We instead rely on the following flow layers to learn the relevant correlations in the individual dropout configurations.
SAY SOMETHING ABOUT Q(ZDOWN)?

\begin{figure}
    \centering
    \begin{tikzpicture}
        % Sort figure

        % Nodes
        \node at (-3.7,0.0)(pz0) {$\text{Unif(0,1)}^8 \sim $};
        \node[blob] at (-2.0,0.0)(z0) {$z_0$};
        \node[blob] at (0.0,0.0)(z0drop) {$z_{0,\mathcal{I}_{\uparrow}}$};
        \node[blob] at (2.0,0.0)(z1) {$z_1$};
        \node[blob] at (4.0,0.0)(z2) {$z_2$};
        \node[blob] at (6.0,0.0)(zn) {$z_n$};
        \node[blob] at (8.0,0.0)(x) {$x$};
        \node[blob] at (10.0,0.0)(xI) {$x_{\mathcal{I}_p}$};
            
        % Arrows between zs
        \draw[arrow] (z0) to [out=45,in=135] (z0drop);
        \draw[dashed,arrow] (z0drop) to [out=225,in=315] (z0);
        \draw[arrow] (z0drop) to [out=45,in=135] (z1);
        \draw[arrow] (z1) to [out=225,in=315] (z0drop);
        \draw[arrow] (z1) to [out=45,in=135] (z2);
        \draw[arrow] (z2) to [out=225,in=315] (z1);
        \draw[arrow] (z2) to [out=45,in=135] (zn);
        \draw[arrow] (zn) to [out=225,in=315] (z2);
        \draw[arrow] (zn) to [out=45,in=135] (x);
        \draw[arrow] (x) to [out=225,in=315] (zn);
        \draw[dashed,arrow] (x) to [out=45,in=135] (xI);
        \draw[arrow] (xI) to [out=225,in=315] (x);
                
        % Function transforms
        \node at (-1.0, 0.0) {drop};
        \node at (1.0, 0.0) {$f_1$};
        \node at (3.0, 0.0) {$f_2$};
        \node at (5.,0) {...};
        \node at (7.0, 0.17) {\footnotesize phase};
        \node at (7.0, -0.17) {\footnotesize space};
        \node at (9.0, 0.0) {sort};       
        
    \end{tikzpicture}
    \caption{
    The normalizing flow architecture of figure \ref{fig:perm} with a dropout surjection included after the base distribution.
    }
    \label{fig:dropout}
\end{figure}

Figure \ref{fig:dropout} shows the architecture for the example process with an included dropout surjection. 





\subsection{Discrete features}


%%% References
\bibliographystyle{JHEP}  
\bibliography{refs}

\end{document}
