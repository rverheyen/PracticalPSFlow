import torch
import torch.nn as nn
import torch.nn.functional as F

class MulticlassClassifier(nn.Module):
    def __init__(self, n_hidden, d_hidden, d_in, d_out): 
        super(MulticlassClassifier, self).__init__()
        self.first = nn.Linear(d_in, d_hidden)
        self.hidden = nn.ModuleList()
        for k in range(n_hidden-1):
            self.hidden.append(nn.Linear(d_hidden, d_hidden))
        self.last = nn.Linear(d_hidden, d_out)

    # Returns a set of log likelihoods for classes
    def forward(self, x):
        # First layer d_in -> d_hidden
        x = F.relu(self.first(x))

        # Hidden layers d_hidden -> d_hidden
        for layer in self.hidden:
            x = F.relu(layer(x))
        
        # Last layer d_hidden -> d_out
        x = self.last(x)

        # Scale to probabilities
        x = F.log_softmax(x, dim=1)
        return x

    # Returns the likelihood of a class
    def category_log_prob(self, context, categories):
        batch_size = context.shape[0]
        return self.forward(context)[torch.arange(batch_size), categories.long()]

    # Use Gumbel-max trick to sample
    def sample(self, context):
        log_probs = self.forward(context)
        return torch.argmax(log_probs - torch.log(-torch.log(torch.rand(log_probs.shape))), dim=1)

'''
num_categories:         [D]-dimensional array with the number of categories per dimension
starting_context_size:  Scalar size of the initial context
n_hidden:               Number of hidden layers in the classifiers
d_hidden:               Number of nodes in hidden layers in the classifiers
'''
class AutoregressiveClassifier(nn.Module):
    def __init__(self, num_categories, starting_context_size, n_hidden, d_hidden):
        super(AutoregressiveClassifier, self).__init__()

        self.classifiers_ = nn.ModuleList()
        self.num_categories_ = num_categories

        context_size = starting_context_size
        for num_index in num_categories:
            self.classifiers_.append(MulticlassClassifier(n_hidden, d_hidden, context_size, num_index))
            context_size += num_index
    
    def category_log_prob(self, starting_context, categories):
        batch_size = starting_context.shape[0]
        context = starting_context

        log_probs = torch.zeros(batch_size)
        for index, (classifier, category, num_category) in enumerate(zip(self.classifiers_, categories.transpose(0,1), self.num_categories_)):
            # Evaluate log probs
            log_probs += classifier.category_log_prob(context, category)

            # Prepare context for next iteration
            if index < self.num_categories_.shape[0]:
                one_hot = F.one_hot(category.long(), num_category)
                context = torch.cat((context, one_hot), dim=1)
        
        return log_probs

    def sample(self, starting_context):
        batch_size = starting_context.shape[0]
        context = starting_context

        samples = torch.empty((batch_size, self.num_categories_.shape[0]))
        for index, (classifier, num_category) in enumerate(zip(self.classifiers_, self.num_categories_)):        
            # Sample from classifier
            samples[:,index] = classifier.sample(context)

            # Prepare context for next iteration
            if index < self.num_categories_.shape[0]:
                one_hot = F.one_hot(samples[:,index].long(), num_category)
                context = torch.cat((context, one_hot), dim=1)

        return samples
