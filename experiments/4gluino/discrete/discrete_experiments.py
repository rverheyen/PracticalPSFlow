#!/usr/bin/python3

import torch
import torch.optim as optim
import wandb
import os
import argparse
import yaml
from ray import tune
import argparse

from ppflows.gluino.gluino_auxiliary import build_dataloader
from ppflows.gluino.gluino_models import GluinoModel, MixtureGluinoModel

from ppflows.utils import EarlyStopper

from ray.tune.integration.wandb import wandb_mixin


def compute_loss_over_dataloader(model, dataloader, config):
    loss = 0
    data_size = 0
    with torch.no_grad():
        for _, (batch_cont, batch_disc) in enumerate(dataloader):
            loss_now = -model.log_prob(batch_cont, batch_disc).mean()

            loss = loss*data_size + loss_now.item()*batch_cont.shape[0]
            data_size += batch_cont.shape[0]
            loss /= data_size

    return loss

@wandb_mixin
def train(config, checkpoint_dir=None):
    train_loader, val_loader, test_loader = build_dataloader(config)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    if config["discrete_mode"] == "mixture":
        model = MixtureGluinoModel(config)
    else:
        model = GluinoModel(config)
    model.to(device)

    optimizer = optim.Adam(model.parameters(), lr=config["learning_rate"])

    # Scheduling with reduction on plateau
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=config["lr_decay"], patience=config["lr_decay_patience"], verbose=True)

    batch_generator = iter(train_loader)
    best_validation_loss = float("inf")
    validation_counter = 0
    test_loss = 0
    for iteration in range(config["n_iterations"]):
        # Catch dataloader exceptions when hitting end of dataset
        try:
            train_batch_cont, train_batch_disc = next(batch_generator)
        except StopIteration:
            # Restart the generator
            batch_generator = iter(train_loader)
            train_batch_cont, train_batch_disc = next(batch_generator)

        # SGD
        optimizer.zero_grad()
        loss = -model.log_prob(train_batch_cont, train_batch_disc).mean()
        loss.backward()
        optimizer.step()

        # ---------------- Validation -----------------
        validation_counter += 1
        if validation_counter == config["validation_interval"]:
            validation_counter = 0

            # Compute validation loss
            validation_loss = compute_loss_over_dataloader(model, val_loader, config)
            wandb.log({"validation_loss": validation_loss})

            # Compute test loss and save best model
            if validation_loss < best_validation_loss:
                best_validation_loss = validation_loss
                test_loss = compute_loss_over_dataloader(model, test_loader, config)
                torch.save(model.state_dict(), os.path.join(wandb.run.dir, "model.pt"))
                
            # Update lr
            scheduler.step(validation_loss)
            
            # Update lr
            scheduler.step(validation_loss)
            wandb.log({ "batch_loss": loss,
                "num_bad_epochs": scheduler.num_bad_epochs,
                "learning_rate": optimizer.param_groups[0]['lr'],
                "validation_loss": validation_loss})

            # Break when lr has been reduced by 3 orders of magnitude
            if optimizer.param_groups[0]['lr'] < config["learning_rate"]*1e-3:
                break
            
    wandb.log({"test_loss": test_loss})

with open("hyperparams.yaml") as file:
    hyperparams = yaml.safe_load(file)
    
# Add data dir
hyperparams["data_dir"] = os.getcwd() + "/.."

parser = argparse.ArgumentParser(description='Discrete experiments')
parser.add_argument('--mode', required=True, choices=['uniform_dequantization', 'flow_dequantization', 'uniform_argmax', 'flow_argmax', 'mixture'], help='Discrete mode')
args = parser.parse_args()

# Add weights and biases info
hyperparams["discrete_mode"] = args.mode
hyperparams["wandb"] = {"project": args.mode + "_experiments","api_key": "4494a6b931b14488cee12d47fb9d7696822c00d7"}

hyperparams["permutation"] = tune.grid_search(["stochastic", "ordered"])

# analysis = tune.run(train, config=config, num_samples=1, resources_per_trial={"cpu": 1})
if "argmax" in args.mode:
    resources_per_trial = 1.
else:
    resources_per_trial = 0.5

analysis = tune.run(train, config=hyperparams, num_samples=1, resources_per_trial={"gpu": resources_per_trial})