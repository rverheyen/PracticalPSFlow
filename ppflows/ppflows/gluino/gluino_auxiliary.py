"""
Discrete encoder for gg->4gluino
Uses Lehmer codes for colour permutations
https://medium.com/@benjamin.botto/sequentially-indexing-permutations-a-linear-algorithm-for-computing-lexicographic-rank-a22220ffd6e3
"""
import torch
from torch import nn
import math
import pandas as pd
import os

from ppflows.file_lock import FileLock

class GluinoEncoder(nn.Module):
    def __init__(self, cont_size=16, hel_size=6, col_size=5):
        super().__init__()
        
        # sqrts gets inferred during the first call to encode_continuous
        self.sqrts = 3000
        self.cont_size = cont_size
        self.hel_size = hel_size
        self.col_size = col_size

        # Set up table of factors for helicity data
        hel_factors = torch.ones(hel_size, dtype=torch.int)
        for i in range(hel_size-2, -1, -1):
            hel_factors[i] = hel_factors[i+1]*2
        self.register_buffer("hel_factors_", hel_factors)

        # Set up table of factorials for colour data
        col_factors = torch.ones(col_size, dtype=torch.int)
        for i in range(col_size-2, -1, -1):
            col_factors[i] = col_factors[i+1]*(col_size - 1 - i)
        self.register_buffer("col_factors_", col_factors)

    '''
    Encoder functions
    '''
    # Encode discrete into 2 sets of numbers
    def encode_discrete(self, data_disc):
        # Split up into helicity data and colour data
        hel_data = data_disc[:,:self.hel_size]
        col_data = data_disc[:,self.hel_size:]

        # Clone helicity data and convert to binary ones and zeroes
        hel_binary = hel_data.clone()
        hel_binary[hel_binary == -1] = 0
        # Then find the number that corresponds with this binary sequence
        hel_code = torch.sum(hel_binary * self.hel_factors_, -1)

        # Clone the colour permutation because we're altering col_lehmer sequentially
        col_lehmer = col_data.clone() 
        for i in range(1,self.col_size-1):
            # Broadcasting such that lhs = (batch,size) and rhs = (batch,1)
            bools = col_data < col_data[:,i,None] 
            # Slice off such that we only have indices below the current one
            sliced_bools = bools[:,:i] 
            # Subtract one for every index < the current one that appeared earlier
            col_lehmer[:,i] -= torch.sum(sliced_bools, axis=1) 
        # Last entry is always zero
        col_lehmer[:,-1] = 0

        # Next, the lehmer sequence is converted to a number using a factorial number system
        col_code = torch.sum(col_lehmer * self.col_factors_, -1)

        return torch.stack((hel_code, col_code), dim=1)

    # Encode continuous - assumes data is in form (batch_size, n, 4)
    def encode_continuous(self, data_cont):
        batch_size = data_cont.size()[0]
        n = data_cont.size()[1]

        # Compute cumulative sum of momenta (p1+p2,p1+p2+p3,...)
        p_cumsum = torch.cumsum(data_cont, dim=1)[:,1:,:] # [batch_size, n-1, 4]

        # Set sqrts if it has not been set yet
        if self.sqrts == None:
            self.sqrts = p_cumsum[0][-1][0]

        # Now boost (p2,p3,..) to the restframes of (p1+p2,p1+p2+p3,..)

        # Compute the masses of the cumulative momentum sums
        m = torch.sqrt(p_cumsum[:,:,0]**2 - p_cumsum[:,:,1]**2 - p_cumsum[:,:,2]**2 - p_cumsum[:,:,3]**2)

        # Check for nans 
        assert(m.isnan().sum().item() == 0)

        # Skip intermediate variables here - immediately evaluate the energy
        p_boost = torch.empty(batch_size, n-1, 4, device=data_cont.device) # n-1 because it does not include p1
        # Compute the boosted energy
        p_boost[:,:,0] = (p_cumsum[:,:,0]*data_cont[:,1:,0] - p_cumsum[:,:,1]*data_cont[:,1:,1] - p_cumsum[:,:,2]*data_cont[:,1:,2] - p_cumsum[:,:,3]*data_cont[:,1:,3])/m
        # Compute the boosted momenta
        p_boost[:,:,1:4] = (data_cont[:,1:,1:4] - p_cumsum[:,:,1:4]*torch.unsqueeze((data_cont[:,1:,0] + p_boost[:,:,0])/(m + p_cumsum[:,:,0]), 2))

        # Output cube
        cube = torch.empty(batch_size, 3*n-4, device=data_cont.device)
        # Store the costhetas
        cube[:,torch.arange(start=0,end=3*(n-2)+1, step=3)] = ( p_boost[:,:,3]/(torch.sqrt(p_boost[:,:,1]**2 + p_boost[:,:,2]**2 + p_boost[:,:,3]**2)) + 1 ) / 2
        # Store the phis
        cube[:,torch.arange(start=1,end=3*(n-2)+2, step=3)] = (torch.atan2(p_boost[:,:,1], p_boost[:,:,2]) + math.pi)/2/math.pi
        # Get and store the scaled masses
        m_scaled = m[:,:-1]/m[:,1:]
        cube[:,torch.arange(start=2,end=3*(n-3)+3, step=3)] = m_scaled

        # Compute phase space weight
        pows = n - 3 - torch.arange(0, n-2, device=data_cont.device)
        log_jacobian = (n-1)*math.log(math.pi/2) + torch.sum(torch.log(m_scaled**pows * (1 - m_scaled)), dim=-1)
        
        return cube, -log_jacobian

    '''
    Decoder functions
    '''

    def decode_discrete(self, categories):
        batch_size = categories.shape[0]

        # First decode the helicity data
        hel_code_running = categories[:,0]
        hel_data = torch.zeros((batch_size, self.hel_size), dtype=torch.int, device=categories.device)
        # Find binary representation
        for i in range(self.hel_size):
            # hel_data[:,i] = hel_code_running.floor_divide(self.hel_factors_[i])
            hel_data[:,i] = torch.div(hel_code_running, self.hel_factors_[i], rounding_mode='floor')
            hel_code_running = hel_code_running.remainder(self.hel_factors_[i])
        # Change zeroes to -1's
        hel_data[hel_data == 0] = -1

        # Now decode the colour code to the lehmer sequence
        col_code_running = categories[:,1]
        col_lehmer = torch.zeros((batch_size, self.col_size), dtype=torch.int, device=categories.device)

        for i in range(self.col_size-1):
            # col_lehmer[:,i] = col_code_running.floor_divide(self.col_factors_[i])
            col_lehmer[:,i] = torch.div(col_code_running, self.col_factors_[i], rounding_mode='floor')
            col_code_running = col_code_running.remainder(self.col_factors_[i])

        # Clone because the first index is the same
        col_data = col_lehmer.clone()

        # Start from 1, because the 0th index is always identical
        for i in range (1, self.col_size):
            # Start from zeroes in the new column
            col_data_now = torch.empty(batch_size, dtype=torch.int, device=categories.device).fill_(0)
            col_lehmer_now = col_lehmer[:,i]
            # Now loop
            while True:
                # Find indices where the current new_perm already appears in the previous indices
                bools_increment = torch.sum(col_data_now[:,None] == col_data[:,:i], axis=1) != False
                # If there are any such indices, increment and continue
                if torch.sum(bools_increment) != False:
                    col_data_now[bools_increment] += 1
                else:
                    # Increment every index that has remaining lehmer values
                    bools_increment = col_lehmer_now != 0
                    # If there are any such indices, increment new_perms_now, decrement lehmer_now and continue
                    if torch.sum(bools_increment) != False:
                        col_data_now[bools_increment] += 1
                        col_lehmer_now[bools_increment] -= 1
                    # Otherwise, we are done
                    else:
                        break

            # Insert into perm
            col_data[:,i] = col_data_now

        return torch.cat((hel_data, col_data), axis=1)
    
    def decode_continuous(self, cube):
        # Make sure sqrts has been set
        if self.sqrts == None:
            raise ValueError("sqrts has not been set")

        batch_size = cube.size()[0]
        n = int((cube.size()[1] + 4)/3)

        costheta = cube[:,torch.arange(start=0,end=3*(n-2)+1, step=3)]*2 - 1
        sintheta = torch.sqrt(1 - costheta**2)
        phi = cube[:,torch.arange(start=1,end=3*(n-2)+2, step=3)]*2*math.pi - math.pi
        m_scaled = cube[:,torch.arange(start=2,end=3*(n-3)+3, step=3)]

        # Compute phase space weight - do not include the dimensionful constant
        pows = n - 3 - torch.arange(0, n-2, device=cube.device)
        log_jacobian = (n-1)*math.log(math.pi/2) + torch.sum(torch.log(m_scaled**pows * (1 - m_scaled)), dim=-1)

        # Unscale the masses
        m = torch.zeros(batch_size, n, device=cube.device)
        m[:,-1]  = self.sqrts
        for i in range(1, n-1):
            m[:,-i-1] = m[:,-i]*m_scaled[:,-i]
        
        # Pre-boost momenta
        p_preboost = torch.zeros(batch_size, n, 4, device=cube.device)

        E_preboost   = (m[:,1:]**2 - m[:,:-1]**2)/2/m[:,1:]
        p_preboost[:,1:,0] = E_preboost
        p_preboost[:,1:,1] = E_preboost*sintheta*torch.sin(phi)
        p_preboost[:,1:,2] = E_preboost*sintheta*torch.cos(phi)
        p_preboost[:,1:,3] = E_preboost*costheta

        p_out = torch.zeros(batch_size, n, 4, device=cube.device)
        
        # The last momentum does not get boosted
        p_out[:,-1,:] = p_preboost[:,-1,:]

        # Sequential unboosting
        p_cm = torch.zeros(batch_size, 4, device=cube.device)
        p_cm[:,0] = self.sqrts 

        for i in range(1, n-1):
            p_cm = p_cm - p_out[:,-i]
            p_out[:,-i-1,0] = (p_preboost[:,-i-1,0]*p_cm[:,0] + p_preboost[:,-i-1,1]*p_cm[:,1] + p_preboost[:,-i-1,2]*p_cm[:,2] + p_preboost[:,-i-1,3]*p_cm[:,3])/m[:,-i-1]
            p_out[:,-i-1,1:4] = p_preboost[:,-i-1,1:4] + p_cm[:,1:4]*torch.unsqueeze((p_preboost[:,-i-1,0] + p_out[:,-i-1,0]) / (m[:,-i-1] + p_cm[:,0]), 1)

        # Get the last momentum through mom conservation
        p_out[:,0,:] = -torch.sum(p_out, dim=1)
        p_out[:,0,0] += self.sqrts

        return p_out, log_jacobian

    '''
    Encoder for continuous data for mixed experiments
    We're not going to worry about logprobs in this context
    '''
    def encode_mixed_continuous(self, data_cont):
        batch_size = data_cont.shape[0]

        mask_2_gluino = data_cont[:,2,0] == 0.
        mask_4_gluino = ~mask_2_gluino

        # Output
        cube = torch.zeros(batch_size, 8, device=data_cont.device)

        # 4-gluino data
        if data_cont[mask_4_gluino].shape[0] != 0:
            cube[mask_4_gluino], _ = self.encode_continuous(data_cont[mask_4_gluino])

        # 2-gluino data
        if data_cont[mask_2_gluino].shape[0] != 0:
            costheta = (data_cont[mask_2_gluino][:,0,3]/data_cont[mask_2_gluino][:,0,0] + 1)/2
            phi = (torch.atan2(data_cont[mask_2_gluino][:,0,1], data_cont[mask_2_gluino][:,0,2]) + math.pi)/2/math.pi
            cube[mask_2_gluino] = torch.cat((costheta[...,None], phi[...,None], -1*torch.ones(data_cont[mask_2_gluino].shape[0], 6, device=data_cont.device)), dim=-1)

        return cube
        
    '''
    Decoder for continuous data for mixed experiments
    '''
    def decode_mixed_continuous(self, cube):
        # Make sure sqrts has been set
        if self.sqrts == None:
            raise ValueError("sqrts has not been set")

        batch_size = cube.shape[0]

        mask_2_gluino = cube[:,2] == -1.
        mask_4_gluino = ~mask_2_gluino

        # Output
        p_out = torch.zeros(batch_size, 4, 4, device=cube.device)

        # 4-gluino data
        if cube[mask_4_gluino].shape[0] != 0:
            p_out[mask_4_gluino], _ = self.decode_continuous(cube[mask_4_gluino])

        # 2-gluino data
        if cube[mask_2_gluino].shape[0] != 0:
            costheta = cube[mask_2_gluino][:,0]*2 - 1
            sintheta = torch.sqrt(1 - costheta**2)
            phi = cube[mask_2_gluino][:,1]*2*math.pi - math.pi

            # Bunch of index magic to make things work
            p1 = torch.cat( ( torch.ones(cube[mask_2_gluino].shape[0], 1, device=cube.device)*self.sqrts/2, \
                            ( sintheta*torch.sin(phi)*self.sqrts/2)[...,None], \
                            ( sintheta*torch.cos(phi)*self.sqrts/2)[...,None], \
                            ( costheta*self.sqrts/2)[...,None] ), dim=-1)
            p2 = torch.cat( ( torch.ones(cube[mask_2_gluino].shape[0], 1, device=cube.device)*self.sqrts/2, \
                            (-sintheta*torch.sin(phi)*self.sqrts/2)[...,None], \
                            (-sintheta*torch.cos(phi)*self.sqrts/2)[...,None], \
                            (-costheta*self.sqrts/2)[...,None] ), dim=-1)

            p_out[mask_2_gluino] = torch.cat((p1.unsqueeze(1), p2.unsqueeze(1), torch.zeros(cube[mask_2_gluino].shape[0], 2, 4, device=cube.device)), dim=1)

        return p_out
        


'''
Rambo rescaling procedure: massless to massive
Assumes all masses are identical
'''
def massless_to_massive(p, m):
    batch_size = p.size()[0]
    sqrts = torch.sum(p, dim=1)[0,0]

    assert(sqrts > m*p.size()[1])

    p_abs2 = p[:,:,1]**2 + p[:,:,2]**2 + p[:,:,3]**2
    x = torch.ones(batch_size, 1, device=p.device)
    f = torch.ones(batch_size, 1, device=p.device)
    while (torch.max(f) > 1e-3):
        f = torch.sum(torch.sqrt(p_abs2 * x**2 + m**2), dim=1) - sqrts
        f_prime = torch.sum(p_abs2*x/torch.sqrt(p_abs2 * x**2 + m**2), dim=1)
        x -= torch.unsqueeze(f/f_prime, 1)

    p[:,:,1:4] *= torch.unsqueeze(x, 2)
    p[:,:,0] = torch.sqrt(p[:,:,1]**2 + p[:,:,2]**2 + p[:,:,3]**2 + m**2)

    return p

'''
Rambo rescaling procedure: massive to massless
Assumes all masses are identical
'''
def massive_to_massless(p):
    sqrts = torch.sum(p, dim=1)[0,0]
    x = sqrts/torch.sum(torch.sqrt(p[:,:,1]**2 + p[:,:,2]**2 + p[:,:,3]**2), dim=1)

    p[:,:,1:4] *= x.unsqueeze(1).unsqueeze(1)
    p[:,:,0] = torch.sqrt(p[:,:,1]**2 + p[:,:,2]**2 + p[:,:,3]**2)
    
    return p

'''
Get the permutation that sorts the data by energy
'''
def energy_ordered_permutation(inputs_continuous, inputs_discrete):
    return torch.argsort(inputs_continuous[:,:,0], dim=1, descending=True)

'''
Data loader for gg->4gluino experiments
'''
def build_dataloader(config, pretrain=False):
    file_path = config["data_dir"] + "/data_4_gluino.csv"

    with FileLock(file_path):
        df = pd.read_csv(file_path, header=None)

    data_size = df.shape[0]

    if pretrain:
        # Preprocessing of continuous data
        data_cont = torch.Tensor(df.values[:,:16])
        # Reshape into 4-vectors
        data_cont = data_cont.reshape(data_cont.shape[0], 4, 4)

        # Map to massless
        data_cont = massive_to_massless(data_cont)

        # Discrete data
        data_disc = torch.IntTensor(df.values[:,16:27])

        training_combined = torch.utils.data.TensorDataset(data_cont, data_disc)
        training_loader   = torch.utils.data.DataLoader(dataset=training_combined, batch_size=config["batch_size"], shuffle=True)

        return training_loader
        
    else:
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        n_training = config["n_training"]
        n_validation = config["n_validation"]
        n_test = config["n_test"]

        assert(n_training + n_validation + n_test < data_size)
        
        # Only keep data we will use
        df = df[:(n_training + n_validation + n_test)]

        # Preprocessing of continuous data
        data_cont = torch.Tensor(df.values[:,:16]).to(device)
        # Reshape into 4-vectors
        data_cont = data_cont.reshape(data_cont.shape[0], 4, 4)

        # Map to massless
        data_cont = massive_to_massless(data_cont)

        # Discrete data
        data_disc = torch.IntTensor(df.values[:,16:27]).to(device)
            
        # Combine and build loader    
        training_combined = torch.utils.data.TensorDataset(data_cont[ : n_training ], data_disc[ : n_training ])
        training_loader   = torch.utils.data.DataLoader(dataset=training_combined  , batch_size=config["batch_size"], shuffle=True)

        validation_combined = torch.utils.data.TensorDataset(data_cont[ n_training : n_training + n_validation ], data_disc[ n_training : n_training + n_validation ])
        validation_loader   = torch.utils.data.DataLoader(dataset=validation_combined, batch_size=config["batch_size"], shuffle=True)
        
        test_combined = torch.utils.data.TensorDataset(data_cont[ n_training + n_validation : n_training + n_validation + n_test ], data_disc[ n_training + n_validation : n_training + n_validation + n_test ])
        test_loader   = torch.utils.data.DataLoader(dataset=test_combined, batch_size=config["batch_size"], shuffle=True)
        
        return training_loader, validation_loader, test_loader

'''
Data loader for mixed gg->2gluino and gg->2gluino experiments
'''
def build_dataloader_mixed(config):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    file_path_4_gluino = config["data_dir"] + "/data_4_gluino.csv"
    file_path_2_gluino = config["data_dir"] + "/data_2_gluino.csv"

    with FileLock(file_path_4_gluino):
        df_4_gluino = pd.read_csv(file_path_4_gluino, header=None)
    with FileLock(file_path_2_gluino):
        df_2_gluino = pd.read_csv(file_path_2_gluino, header=None)

    data_size_4_gluino = df_4_gluino.shape[0]
    data_size_2_gluino = df_2_gluino.shape[0]

    n_training_4_gluino = int(config["n_training"]*config["four_gluino_frac"])
    n_validation_4_gluino = int(config["n_validation"]*config["four_gluino_frac"])
    n_test_4_gluino = int(config["n_test"]*config["four_gluino_frac"])

    n_training_2_gluino = int(config["n_training"]*(1 - config["four_gluino_frac"]))
    n_validation_2_gluino = int(config["n_validation"]*(1 - config["four_gluino_frac"]))
    n_test_2_gluino = int(config["n_test"]*(1 - config["four_gluino_frac"]))

    assert(n_training_4_gluino + n_validation_4_gluino + n_test_4_gluino < data_size_4_gluino)
    assert(n_training_2_gluino + n_validation_2_gluino + n_test_2_gluino < data_size_2_gluino)

    n_training = n_training_2_gluino + n_training_4_gluino
    n_validation = n_validation_2_gluino + n_validation_4_gluino
    n_test = n_test_2_gluino + n_test_4_gluino

    n_4_gluino = n_training_4_gluino + n_validation_4_gluino + n_test_4_gluino
    n_2_gluino = n_training_2_gluino + n_validation_2_gluino + n_test_2_gluino
    
    # Only keep data we will use
    df_4_gluino = df_4_gluino[:n_4_gluino]
    df_2_gluino = df_2_gluino[:n_2_gluino]

    # We only use continuous data for the mixed experiments
    data_4_gluino = torch.Tensor(df_4_gluino.values[:,:16]).to(device)
    data_2_gluino = torch.Tensor(df_2_gluino.values[:,:8]).to(device)
    data_2_gluino = torch.cat((data_2_gluino, torch.zeros(data_2_gluino.shape[0], 8, device=device)), dim=-1)

    # Reshape 
    data_4_gluino = data_4_gluino.reshape(data_4_gluino.shape[0], 4, 4)
    data_2_gluino = data_2_gluino.reshape(data_2_gluino.shape[0], 4, 4)

    # Map to massless
    data_4_gluino = massive_to_massless(data_4_gluino)
    data_2_gluino = massive_to_massless(data_2_gluino)

    # Concatenate data
    data_combine = torch.cat((data_4_gluino, data_2_gluino))

    # Shuffle data
    data_combine = data_combine[torch.randperm(data_combine.shape[0])]

    # Set up data loader
    training_loader   = torch.utils.data.DataLoader(dataset=data_combine[ : n_training ], batch_size=config["batch_size"], shuffle=True)
    validation_loader = torch.utils.data.DataLoader(dataset=data_combine[ n_training : n_training + n_validation ], batch_size=config["batch_size"], shuffle=True)
    test_loader       = torch.utils.data.DataLoader(dataset=data_combine[ n_training + n_validation : n_training + n_validation + n_test ], batch_size=config["batch_size"], shuffle=True)

    return training_loader, validation_loader, test_loader